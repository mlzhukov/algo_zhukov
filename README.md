Здесь представлены решения заданий по курсу Java Algorithms and Data Structures  
С помощью средств ООП на java были реализованы такие структуры данных, как:

* Dynamic Array - lesson03  
* DoublyLinkedList, SinglyLinkedList - lesson04  
* Queue, Stack (linked) - lesson05  
* AssociativeArray - lesson06  
* BinaryTree (array/linked) BinarySearchTree - lesson07  
* Balanceable Tree - lesson 08  
* RedBlackTree - lesson 09  

Юнит-тесты написаны к:  
1) методам каждой структуры  
2) методам некоторых классов-потомков java.util.Collection, некоторым методам java.util.Collections (с целью изучения)  

Реализованы алгоритмы сортировки bubble, merge, insertion - lesson10  

**Жуков Матвей**