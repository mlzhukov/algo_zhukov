package com.gjj.training.algo1704.zhukovm.lesson01;

import java.util.Scanner;

public class Task06 {

    public static void main(final String[] args) {
        System.out.println("a) given n<31, calc 2^n\n" +
                "b) given n,m<31, calc 2^n+2^m\n" +
                "c) reset n lower bits (create mask with n lower bits reset)\n" +
                "d) set a's n-th bit with 1\n" +
                "e) invert n-th bit (use 2 bit ops)\n" +
                "f) set a's n-th bit with 0\n" +
                "g) return n lower bits\n" +
                "h) return n-th bit\n" +
                "i) given byte a. output bin representation using bit ops (don't use jdk api)");
        System.out.println("select point: a, b, c, d, e, f, g, h, i");
        Scanner in = new Scanner(System.in);
        char input = in.nextLine().charAt(0);
        switch (input) {
            case 'a':
                System.out.println("enter n < 31: ");
                int n = in.nextInt();
                System.out.println(calculatePowOfTwo(n));
                break;
            case 'b':
                System.out.println("enter n, m < 31: ");
                n = in.nextInt();
                int m = in.nextInt();
                System.out.println(calculateSumOfPowers(n, m));
                break;
            case 'c':
                System.out.println("enter n a: ");
                n = in.nextInt();
                int a = in.nextInt();
                System.out.println(resetBits(a, n));
                break;
            case 'd':
                System.out.println("enter n a:");
                n = in.nextInt();
                a = in.nextInt();
                System.out.println(setBitToOne(a, n));
                break;
            case 'e':
                System.out.println("enter n a:");
                n = in.nextInt();
                a = in.nextInt();
                System.out.println(invertBit(a, n));
                break;
            case 'f':
                System.out.println("enter n a:");
                n = in.nextInt();
                a = in.nextInt();
                System.out.println(setBitToZero(a, n));
                break;
            case 'g':
                System.out.println("enter n a:");
                n = in.nextInt();
                a = in.nextInt();
                System.out.println(returnLowestBits(a, n));
                break;
            case 'h':
                System.out.println("enter n a:");
                n = in.nextInt();
                a = in.nextInt();
                System.out.println(returnBit(a, n));
                break;
            case 'i':
                System.out.println("enter n (byte): ");
                byte b = in.nextByte();
                System.out.println(getBinary(b));
                break;
            default:
                break;
        }
    }

    /**
     * a) given n<31, calc 2^n
     */
    static int calculatePowOfTwo(final int n) {
        return 1 << n;
    }

    /**
     * b) given n,m<31, calc 2^n+2^m
     */
    static int calculateSumOfPowers(final int n, final int m) {
        return invertBit(1 << n, m);
    }

    /**
     * c) reset n lower bits (create mask with n lower bits reset)
     */
    static int resetBits(final int a, final int n) {
        return a & (~0 << n);
    }

    /**
     * d) set a's n-th bit with 1
     */
    static int setBitToOne(final int a, final int n) {
        return a | 1 << n;
    }

    /**
     * e) invert n-th bit (use 2 bit ops)
     */
    static int invertBit(final int a, final int n) {
        return a ^ 1 << n;
    }

    /**
     * f) set a's n-th bit with 0
     */
    static int setBitToZero(final int a, final int n) {
        return a & ~(1 << n);
    }

    /**
     * g) return n lower bits
     */
    static int returnLowestBits(final int a, final int n) {
        return a & ~(~0 << n);
    }

    /**
     * h) return n-th bit
     */
    static int returnBit(final int a, final int n) {
        return (a & 1 << n) >> n;
    }

    /**
     * i) given byte a. output bin representation using bit ops (don't use jdk api)
     */
    static String getBinary(byte a) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            byte tmp = (byte) (a >> i);
            int expect = tmp & 0b1;
            result.append(expect);
        }
        return result.reverse().toString();
    }

}
