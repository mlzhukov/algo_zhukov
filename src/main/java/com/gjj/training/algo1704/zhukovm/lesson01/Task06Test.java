package com.gjj.training.algo1704.zhukovm.lesson01;

import static com.gjj.training.algo1704.zhukovm.lesson01.Task06.*;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class Task06Test {

    public static void main(final String[] args) {
        testPowOfTwo();
        testSumOfPowers();
        testResetBits();
        testSetBitToOne();
        testInvertBit();
        testSetBitToZero();
        testReturnLowestBits();
        testReturnBit();
        testGetBinary();
    }

    private static void testPowOfTwo() {
        assertEquals("Task06Test.testPowOfTwo", 16, calculatePowOfTwo(4));
    }

    private static void testSumOfPowers() {
        assertEquals("Task06Test.testSumOfPowers", 20, calculateSumOfPowers(4, 2));
    }

    private static void testResetBits() {
        assertEquals("Task06Test.testResetBits", 0b111000, resetBits(0b111111, 3));
    }

    private static void testSetBitToOne() {
        assertEquals("Task06Test.testSetBitToOne", 0b111001, setBitToOne(0b110001, 3));
    }

    private static void testInvertBit() {
        assertEquals("Task06Test.testInvertBit", 0b110011, invertBit(0b110111, 2));
    }

    private static void testSetBitToZero() {
        assertEquals("Task06Test.testSetBitToZero", 0b1110001, setBitToZero(0b1110101, 2));
    }

    private static void testReturnLowestBits() {
        assertEquals("Task06Test.testReturnLowestBits", 0b101, returnLowestBits(0b10101101, 3));
    }

    private static void testReturnBit() {
        assertEquals("Task06Test.testReturnBit", 0b1, returnBit(0b0101101, 3));
    }

    private static void testGetBinary() {
        assertEquals("Task06Test.testGetBinary", "00011101", getBinary((byte) 0b00011101));
    }

}
