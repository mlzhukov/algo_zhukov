package com.gjj.training.algo1704.zhukovm.lesson01;

import java.util.Scanner;

public class Task07 {

    static int x;
    static int y;

    public static void main(String[] args) {
        System.out.println("enter x y: ");
        Scanner in = new Scanner(System.in);
        x = in.nextInt();
        y = in.nextInt();
        System.out.println("select solution:\n1 (arithmetic + -)\n" +
                "2 (xor)\n" +
                "3 (arithmetic * /)\n" +
                "4 (& |) ");
        int point = in.nextInt();
        switch (point) {
            case 1:
                swapFirst();
                break;
            case 2:
                swapSecond();
                break;
            case 3:
                swapThird();
                break;
            case 4:
                swapFourth();
                break;
            default:
                break;
        }
        System.out.println("x = " + x + " y = " + y);
    }

    static void swapFirst() {
        x += y;
        y = x - y;
        x -= y;
    }

    static void swapSecond() {
        x ^= y;
        y ^= x;
        x ^= y;
    }

    static void swapThird() {
        x *= y;
        y = x / y;
        x /= y;
    }

    static void swapFourth() {
        x = x & ~y | ~x & y;
        y = x & ~y | ~x & y;
        x = x & ~y | ~x & y;
    }

}

