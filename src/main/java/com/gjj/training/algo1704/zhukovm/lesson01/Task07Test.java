package com.gjj.training.algo1704.zhukovm.lesson01;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class Task07Test {

    public static void main(String[] args) {
        testSwapFirst();
        testSwapSecond();
        testSwapThird();
        testSwapFourth();
    }

    private static void testSwapFirst() {
        Task07.x = 4;
        Task07.y = 5;
        Task07.swapFirst();
        assertEquals("Task07Test.testSwapFirst", 5, Task07.x);
        assertEquals("Task07Test.testSwapFirst", 4, Task07.y);
    }

    private static void testSwapSecond() {
        Task07.x = 1;
        Task07.y = 2;
        Task07.swapFirst();
        assertEquals("Task07Test.testSwapSecond", 2, Task07.x);
        assertEquals("Task07Test.testSwapSecond", 1, Task07.y);
    }

    private static void testSwapThird() {
        Task07.x = 3;
        Task07.y = 4;
        Task07.swapFirst();
        assertEquals("Task07Test.testSwapThird", 4, Task07.x);
        assertEquals("Task07Test.testSwapThird", 3, Task07.y);
    }

    private static void testSwapFourth() {
        Task07.x = 1;
        Task07.y = 2;
        Task07.swapFirst();
        assertEquals("Task07Test.testSwapFourth", 2, Task07.x);
        assertEquals("Task07Test.testSwapFourth", 1, Task07.y);
    }

}
