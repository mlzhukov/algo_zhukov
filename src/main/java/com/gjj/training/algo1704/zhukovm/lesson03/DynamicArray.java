package com.gjj.training.algo1704.zhukovm.lesson03;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

import static java.util.Arrays.copyOf;

public class DynamicArray {

    private static final int DEFAULT_CAPACITY = 10;

    private int size;
    private Object[] elements;
    private int modCount;

    public DynamicArray() {
        elements = new Object[DEFAULT_CAPACITY];
    }

    public DynamicArray(final int capacity) {
        elements = new Object[capacity];
    }

    static DynamicArray makeDefaultArray() {
        DynamicArray array = new DynamicArray(5);
        array.elements[0] = 3.14;
        array.elements[1] = 1.1;
        array.elements[2] = 1.2;
        array.elements[3] = 1.3;
        array.elements[4] = 1.4;
        array.size = 5;
        return array;
    }

    boolean add(final Object e) {
        ensureCapacity();
        elements[size++] = e;
        modCount++;
        return true;
    }

    void add(final int i, final Object e) {
        rangeCheckAdd(i);
        ensureCapacity();
        System.arraycopy(elements, i, elements, i + 1, size - i);
        elements[i] = e;
        size++;
        modCount++;
    }

    Object set(final int i, final Object e) {
        rangeCheck(i);
        Object tmp = elements[i];
        elements[i] = e;
        return tmp;
    }

    Object get(final int i) {
        rangeCheck(i);
        return elements[i];
    }

    Object remove(final int i) {
        rangeCheck(i);
        Object tmp = elements[i];
        System.arraycopy(elements, i + 1, elements, i, size - i - 1);
        elements[--size] = null;
        modCount++;
        return tmp;
    }

    boolean remove(final Object e) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(e)) {
                System.arraycopy(elements, i + 1, elements, i, size - i - 1);
                elements[--size] = null;
                modCount++;
                return true;
            }
        }
        return false;
    }

    int size() {
        return size;
    }

    int indexOf(final Object e) {
        for (int i = 0; i < size; i++) {
            if (e.equals(elements[i])) {
                return i;
            }
        }
        return -1;
    }

    boolean contains(final Object e) {
        for (int i = 0; i < size; i++) {
            if (e.equals(elements[i])) {
                return true;
            }
        }
        return false;
    }

    Object[] toArray() {
        return copyOf(elements, size);
    }

    private void rangeCheck(final int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private void rangeCheckAdd(final int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private int capacity() {
        return elements.length;
    }

    private void ensureCapacity() {
        if (elements.length == size) {
            elements = copyOf(elements, (int) (size * 1.5));
        }
    }

    DynamicArray.ListIterator listIterator() {
        return new DynamicArray.ListIterator();
    }

    class ListIterator {

        private int cursor;
        private int expectedModCount = modCount;
        private int lastReturned = -1;

        boolean hasNext() {
            return cursor != size;
        }

        Object next() {
            if (cursor == size) {
                throw new NoSuchElementException();
            }
            checkForModification();
            lastReturned = cursor;
            return elements[cursor++];
        }

        boolean hasPrevious() {
            return cursor != 0;
        }

        Object previous() {
            if (cursor == 0) {
                throw new NoSuchElementException();
            }
            checkForModification();
            lastReturned = --cursor;
            return elements[cursor];
        }

        int nextIndex() {
            return cursor;
        }

        int previousIndex() {
            return cursor - 1;
        }

        void remove() {
            checkForModification();
            checkForIllegalState();
            DynamicArray.this.remove(lastReturned);
            cursor = lastReturned;
            lastReturned = -1;
            expectedModCount = ++modCount;
        }

        void set(final Object e) {
            checkForModification();
            checkForIllegalState();
            DynamicArray.this.set(lastReturned, e);
        }

        void add(final Object e) {
            checkForModification();
            DynamicArray.this.add(cursor++, e);
            lastReturned = -1;
            expectedModCount = ++modCount;
        }

        private void checkForModification() {
            if (expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }
        }

        private void checkForIllegalState() {
            if (lastReturned == -1) {
                throw new IllegalStateException();
            }
        }

    }

}

