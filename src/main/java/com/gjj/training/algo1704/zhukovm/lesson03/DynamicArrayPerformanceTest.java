package com.gjj.training.algo1704.zhukovm.lesson03;

import com.gjj.training.algo1704.zhukovm.util.StopWatch;

import java.util.ArrayList;

public class DynamicArrayPerformanceTest {

    public static void main(String[] args) {
        // testAddByIndexToBeg();
        // testAddByIndexToMid();
        // testAddByIndexToEnd();
        // testRegularAddToEnd();
        // testRemoveByIndexFromBeg();
        // testRemoveByIndexFromMid();
        testRemoveByObjectFromBeg();

    }

    private static void testAddByIndexToBeg() {
        System.out.println("-------- Addition to the begin --------");
        DynamicArray dynamicArray = new DynamicArray(300_000);
        ArrayList arrayList = new ArrayList(300_000);
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            dynamicArray.add(0, i);
        }
        System.out.println("DynamicArray.add(e): " + timer.getElapsedTime() + " ms");
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            arrayList.add(0, i);
        }
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddByIndexToMid() {
        System.out.println("-------- Addition to the middle --------");
        DynamicArray dynamicArray = new DynamicArray(300_000);
        ArrayList arrayList = new ArrayList(300_000);
        StopWatch timer = new StopWatch();
        timer.start();
        dynamicArray.add(1);
        for (int i = 0; i < 300_000; i++) {
            dynamicArray.add(1, i);
        }
        System.out.println("DynamicArray.add(e): " + timer.getElapsedTime() + " ms");
        timer.start();
        arrayList.add(1);
        for (int i = 0; i < 300_000; i++) {
            arrayList.add(1, i);
        }
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddByIndexToEnd() {
        System.out.println("-------- Addition by index to the end --------");
        DynamicArray dynamicArray = new DynamicArray(900_000_000);
        ArrayList arrayList = new ArrayList(900_000_000);
        StopWatch timer = new StopWatch();
        byte value = 127;
        timer.start();
        for (int i = 0; i < 900_000_000; i++) {
            dynamicArray.add(i, value);
        }
        System.out.println("DynamicArray.add(e): " + timer.getElapsedTime() + " ms");
        timer.start();
        for (int i = 0; i < 900_000_000; i++) {
            arrayList.add(i, value);
        }
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime() + " ms");

    }

    private static void testRegularAddToEnd() {
        System.out.println("-------- Addition to the end --------");
        DynamicArray dynamicArray = new DynamicArray(1200_000_000);
        ArrayList arrayList = new ArrayList(1200_000_000);
        byte value = 127;
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 1200_000_000; i++) {
            dynamicArray.add(value);
        }
        System.out.println("DynamicArray.add(e): " + timer.getElapsedTime() + " ms");
        timer.start();
        for (int i = 0; i < 1200_000_000; i++) {
            arrayList.add(value);
        }
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromBeg() {
        System.out.println("-------- Removing by index from the begin --------");
        DynamicArray dynamicArray = new DynamicArray(300_000);
        ArrayList arrayList = new ArrayList(300_000);
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 300_000; i++) {
            dynamicArray.add(0, i);
        }
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            dynamicArray.remove(0);
        }
        System.out.println("DynamicArray.remove(e): " + timer.getElapsedTime() + " ms");
        for (int i = 0; i < 300_000; i++) {
            arrayList.add(0, i);
        }
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            arrayList.remove(0);
        }
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromMid() {
        System.out.println("-------- Removing by index from the middle --------");
        DynamicArray dynamicArray = new DynamicArray(300_000);
        ArrayList arrayList = new ArrayList(300_000);
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 300_000; i++) {
            dynamicArray.add(i);
        }
        timer.start();
        for (int i = 0; i < 295_000; i++) {
            dynamicArray.remove(6);
        }
        System.out.println("DynamicArray.remove(e): " + timer.getElapsedTime() + " ms");
        for (int i = 0; i < 300_000; i++) {
            arrayList.add(i);
        }
        timer.start();
        for (int i = 0; i < 295_000; i++) {
            arrayList.remove(6);
        }
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromEnd() {
        System.out.println("-------- Removing by index from the end --------");
        DynamicArray dynamicArray = new DynamicArray(1400_000_001);
        ArrayList arrayList = new ArrayList(1400_000_001);
        StopWatch timer = new StopWatch();
        byte value = 127;
        for (int i = 0; i < 1400_000_001; i++) {
            dynamicArray.add(value);
        }
        timer.start();
        for (int i = 1400_000_000; i > 0; i--) {
            dynamicArray.remove(i);
        }
        System.out.println("DynamicArray.remove(e): " + timer.getElapsedTime() + " ms");
        for (int i = 0; i < 1400_000_001; i++) {
            arrayList.add(value);
        }
        timer.start();
        for (int i = 1400_000_000; i > 0; i--) {
            arrayList.remove(i);
        }
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByObjectFromBeg() {
        System.out.println("-------- Removing by object from the begin --------");
        DynamicArray dynamicArray = new DynamicArray(300_001);
        ArrayList arrayList = new ArrayList(300_001);
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 300001; i++) {
            dynamicArray.add(value);
        }
        timer.start();
        for (int i = 300_000; i > 0; i--) {
            dynamicArray.remove(value);
        }
        System.out.println("DynamicArray.remove(e): " + timer.getElapsedTime() + " ms");
        for (int i = 0; i < 300_001; i++) {
            arrayList.add(value);
        }
        timer.start();
        for (int i = 300_000; i > 0; i--) {
            arrayList.remove(value);
        }
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime() + " ms");
    }

}
