package com.gjj.training.algo1704.zhukovm.lesson03;

import static com.gjj.training.algo1704.zhukovm.lesson03.DynamicArray.makeDefaultArray;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;

public class DynamicArrayTest {

    public static void main(String[] args) {
        testToArray();

        testGetSuccess();
        testGetException();

        testSetSuccess();
        testSetException();

        testAddByIndexToBeg();
        testAddByIndexToMid();
        testAddByIndexToEnd();
        testAddByIndexException();
        testRegularAddToBeg();
        testRegularAddToEnd();

        testRemoveByIndexFromBeg();
        testRemoveByIndexFromMid();
        testRemoveByIndexFromEnd();
        testRemoveByIndexException();
        testRemoveObjFromBeg();
        testRemoveObjFromMid();
        testRemoveObjFromEnd();

        testSize();

        testContains();
        testNotContains();

        testIndexCorrect();
        testIndexWrong();
    }

    private static void testToArray() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {3.14, 1.1, 1.2, 1.3, 1.4};
        assertEquals("DynamicArrayTest.testToArray", testArray, defaultArray.toArray());
    }

    private static void testGetSuccess() {
        DynamicArray defaultArray = makeDefaultArray();
        assertEquals("DynamicArrayTest.testGetSuccess", 3.14, defaultArray.get(0));
    }

    private static void testGetException() {
        DynamicArray defaultArray = makeDefaultArray();
        try {
            defaultArray.get(5);
            fail("AssertionError");
        } catch (ArrayIndexOutOfBoundsException e) {
            assertEquals("DynamicArrayTest.testGetException", true, e.getMessage() == null);
        }
    }

    private static void testSetSuccess() {
        DynamicArray defaultArray = makeDefaultArray();
        defaultArray.set(1, 1.0);
        Object[] testArray = {3.14, 1.0, 1.2, 1.3, 1.4};
        assertEquals("DynamicArrayTest.testSetSuccess", testArray, defaultArray.toArray());
    }

    private static void testSetException() {
        DynamicArray defaultArray = makeDefaultArray();
        try {
            defaultArray.set(5, 4.9);
            fail("AssertionError");
        } catch (ArrayIndexOutOfBoundsException e) {
            assertEquals("DynamicArrayTest.testSetException", true, e.getMessage() == null);
        }
    }

    private static void testAddByIndexToBeg() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {5.0, 3.14, 1.1, 1.2, 1.3, 1.4};
        defaultArray.add(0, 5.0);
        assertEquals("DynamicArrayTest.testAddByIndexToBeg", testArray, defaultArray.toArray());
    }

    private static void testAddByIndexToMid() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {3.14, 1.1, 1.2, 5.0, 1.3, 1.4};
        defaultArray.add(3, 5.0);
        assertEquals("DynamicArrayTest.testAddByIndexToMid", testArray, defaultArray.toArray());
    }

    private static void testAddByIndexToEnd() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {3.14, 1.1, 1.2, 1.3, 1.4, 5.0};
        defaultArray.add(5, 5.0);
        assertEquals("DynamicArrayTest.testAddByIndexToEnd", testArray, defaultArray.toArray());
    }

    private static void testAddByIndexException() {
        try {
            DynamicArray defaultArray = makeDefaultArray();
            defaultArray.add(6, 5.0);
            fail("AssertionError");
        } catch (ArrayIndexOutOfBoundsException e) {
            assertEquals("DynamicArrayTest.testAddByIndexException", true, e.getMessage() == null);
        }
    }

    private static void testRegularAddToBeg() {
        DynamicArray addArray = new DynamicArray();
        addArray.add(3.15);
        Object[] testArray = {3.15};
        assertEquals("DynamicArrayTest.testRegularAddToBeg", testArray, addArray.toArray());
    }

    private static void testRegularAddToEnd() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {3.14, 1.1, 1.2, 1.3, 1.4, 6.0};
        defaultArray.add(6.0);
        assertEquals("DynamicArrayTest.testRegularAddToEnd", testArray, defaultArray.toArray());
    }

    private static void testRemoveByIndexFromBeg() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {1.1, 1.2, 1.3, 1.4};
        defaultArray.remove(0);
        assertEquals("DynamicArrayTest.testRemoveByIndexFromBeg", testArray, defaultArray.toArray());
    }

    private static void testRemoveByIndexFromMid() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {3.14, 1.1, 1.3, 1.4};
        defaultArray.remove(2);
        assertEquals("DynamicArrayTest.testRemoveByIndexFromMid", testArray, defaultArray.toArray());
    }

    private static void testRemoveByIndexFromEnd() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {3.14, 1.1, 1.2, 1.3};
        defaultArray.remove(4);
        assertEquals("DynamicArrayTest.testRemoveByIndexFromEnd", testArray, defaultArray.toArray());
    }

    private static void testRemoveByIndexException() {
        try {
            DynamicArray defaultArray = makeDefaultArray();
            defaultArray.remove(6);
            fail("AssertionError");
        } catch (ArrayIndexOutOfBoundsException e) {
            assertEquals("DynamicArrayTest.testRemoveByIndexException", true, e.getMessage() == null);
        }
    }

    private static void testRemoveObjFromBeg() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {1.1, 1.2, 1.3, 1.4};
        defaultArray.remove(3.14);
        assertEquals("DynamicArrayTest.testRemoveObjFromBeg", testArray, defaultArray.toArray());
    }

    private static void testRemoveObjFromMid() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {3.14, 1.1, 1.3, 1.4};
        defaultArray.remove(1.2);
        assertEquals("DynamicArrayTest.testRemoveObjFromMid", testArray, defaultArray.toArray());
    }

    private static void testRemoveObjFromEnd() {
        DynamicArray defaultArray = makeDefaultArray();
        Object[] testArray = {3.14, 1.1, 1.2, 1.3};
        defaultArray.remove(1.4);
        assertEquals("DynamicArrayTest.testRemoveObjFromEnd", testArray, defaultArray.toArray());
    }

    private static void testSize() {
        DynamicArray defaultArray = makeDefaultArray();
        assertEquals("DynamicArrayTest.testSize", 5, defaultArray.size());
    }

    private static void testContains() {
        DynamicArray defaultArray = makeDefaultArray();
        assertEquals("DynamicArrayTest.testContains", true, defaultArray.contains(1.2));
    }

    private static void testNotContains() {
        DynamicArray defaultArray = makeDefaultArray();
        assertEquals("DynamicArrayTest.testNotContains", false, defaultArray.contains(1.23));
    }

    private static void testIndexCorrect() {
        DynamicArray defaultArray = makeDefaultArray();
        assertEquals("DynamicArrayTest.testIndexCorrect", 3, defaultArray.indexOf(1.3));
    }

    private static void testIndexWrong() {
        DynamicArray defaultArray = makeDefaultArray();
        assertEquals("DynamicArrayTest.testIndexWrong", -1, defaultArray.indexOf(32));
    }

}
