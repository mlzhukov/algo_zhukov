package com.gjj.training.algo1704.zhukovm.lesson04;

import java.util.ArrayList;
import java.util.Collection;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.Arrays.asList;

public class CollectionTest {

    public static void main(String[] args) {
        testSize();
        testIsEmpty();
        testIsNotEmpty();
        testContains();
        testNotContains();
        testToArray();
        testRegularAddToEnd();
        testRegularAddToBeg();
        testRemoveObjFromBeg();
        testRemoveObjFromMid();
        testRemoveObjFromEnd();
        testContainsAll();
        testNotContainsAll();
        testAddAll();
        testRemoveAll();
        testRetainAll();
        testClear();
        testEquals();
        testNotEquals();
        testSameHashCode();
        testNotSameHashCode();
    }

    private static void testSize() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        assertEquals("CollectionTest.testAdd", 3, collection.size());
    }

    private static void testIsEmpty() {
        Collection<String> collection = new ArrayList<>();
        assertEquals("CollectionTest.testIsEmpty", true, collection.isEmpty());
    }

    private static void testIsNotEmpty() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        assertEquals("CollectionTest.testIsNotEmpty", false, collection.isEmpty());
    }

    private static void testContains() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        assertEquals("CollectionTest.testContains", true, collection.contains("second"));
    }

    private static void testNotContains() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        assertEquals("CollectionTest.testNotContains", false, collection.contains("zzz"));
    }

    private static void testToArray() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Object[] expectedArray = {"first", "second", "third"};
        assertEquals("CollectionTest.testToArray", expectedArray, collection.toArray());
    }

    private static void testRegularAddToBeg() {
        Collection<String> collection = new ArrayList<>();
        collection.add("fourth");
        Object[] expectedArray = {"fourth"};
        assertEquals("CollectionTest.testRegularAddToBeg", expectedArray, collection.toArray());
    }

    private static void testRegularAddToEnd() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        collection.add("fourth");
        Object[] expectedArray = {"first", "second", "third", "fourth"};
        assertEquals("CollectionTest.testRegularAddToEnd", expectedArray, collection.toArray());
    }

    private static void testRemoveObjFromBeg() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        collection.remove("first");
        Object[] expectedArray = {"second", "third"};
        assertEquals("CollectionTest.testRemoveObjFromBeg", expectedArray, collection.toArray());
    }

    private static void testRemoveObjFromMid() {
        Collection<String> collection = new ArrayList<>(asList("first", "third"));
        collection.remove("second");
        Object[] expectedArray = {"first", "third"};
        assertEquals("CollectionTest.testRemoveObjFromMid", expectedArray, collection.toArray());
    }

    private static void testRemoveObjFromEnd() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        collection.remove("third");
        Object[] expectedArray = {"first", "second"};
        assertEquals("CollectionTest.testRemoveObjFromEnd", expectedArray, collection.toArray());
    }

    private static void testContainsAll() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("second", "third"));
        assertEquals("CollectionTest.testContainsAll", true, collection.containsAll(testCollection));
    }

    private static void testNotContainsAll() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("seeeecond", "third"));
        assertEquals("CollectionTest.testNotContainsAll", false, collection.containsAll(testCollection));
    }

    private static void testAddAll() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("fourth", "fifth"));
        Object[] expectedArray = {"first", "second", "third", "fourth", "fifth"};
        collection.addAll(testCollection);
        assertEquals("CollectionTest.testAddAll", expectedArray, collection.toArray());
    }

    private static void testRemoveAll() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("first", "third"));
        Object[] expectedArray = {"second"};
        collection.removeAll(testCollection);
        assertEquals("CollectionTest.testRemoveAll", expectedArray, collection.toArray());
    }

    private static void testRetainAll() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("first", "second"));
        Object[] expectedArray = {"first", "second"};
        collection.retainAll(testCollection);
        assertEquals("CollectionTest.testRetainAll", expectedArray, collection.toArray());
    }

    private static void testClear() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Object[] expectedArray = {};
        collection.clear();
        assertEquals("CollectionTest.testClear", expectedArray, collection.toArray());
    }

    private static void testEquals() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("first", "second", "third"));
        assertEquals("CollectionTest.testEquals", true, collection.equals(testCollection));
    }

    private static void testNotEquals() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("first", "third"));
        assertEquals("CollectionTest.testNotEquals", false, collection.equals(testCollection));
    }

    private static void testSameHashCode() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("first", "second", "third"));
        assertEquals("CollectionTest.testSameHashCode", true, collection.hashCode() == testCollection.hashCode());
    }

    private static void testNotSameHashCode() {
        Collection<String> collection = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testCollection = new ArrayList<>(asList("first", "third"));
        assertEquals("CollectionTest.testNotSameHashCode", false, collection.hashCode() == testCollection.hashCode());
    }

}

