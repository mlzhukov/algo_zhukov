package com.gjj.training.algo1704.zhukovm.lesson04;

import com.gjj.training.algo1704.zhukovm.util.StopWatch;

import java.util.LinkedList;

public class DLLPerformanceTest {

    public static void main(String[] args) {
//        testAddToBegDLL();
//        testAddToBegLL();
//        testAddToMidDLL();
//        testAddToMidLL();
//        testAddToEndDLL();
//        testAddToEndLL();
//        testRemoveByIndexFromBegDLL();
//        testRemoveByIndexFromBegLL();
//        testRemoveByIndexFromMidDLL();
//        testRemoveByIndexFromMidLL();
//        testRemoveByIndexFromEndDLL();
//        testRemoveByIndexFromEndLL();
//        testRemoveByObjectFromBegDLL();
        testRemoveByObjectFromBegLL();

    }

    private static void testAddToBegDLL() {
        System.out.println("-------- Addition to the begin --------");
        DoublyLinkedList<Byte> dll = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        timer.start();
        for (int i = 0; i < 90_000_000; i++) {
            dll.add(0, value);
        }
        System.out.println("DoublyLinkedList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddToBegLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        timer.start();
        for (int i = 0; i < 90_000_000; i++) {
            ll.add(0, value);
        }
        System.out.println("LinkedList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddToMidDLL() {
        System.out.println("-------- Addition to the middle --------");
        DoublyLinkedList<Byte> dll = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        dll.add(value);
        timer.start();
        for (int i = 0; i < 90_000_000; i++) {
            dll.add(1, value);
        }
        System.out.println("DoublyLinkedList.add(e): " + timer.getElapsedTime() + " ms");

    }

    private static void testAddToMidLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        ll.add(value);
        timer.start();
        for (int i = 0; i < 90_000_000; i++) {
            ll.add(1, value);
        }
        System.out.println("LinkedList.add(e): " + timer.getElapsedTime() + " ms");
    }


    private static void testAddToEndDLL() {
        System.out.println("-------- Addition to the end --------");
        DoublyLinkedList<Byte> dll = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        timer.start();
        for (int i = 0; i < 90_000_000; i++) {
            dll.add(value);
        }
        System.out.println("DoublyLinkedList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddToEndLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        timer.start();
        for (int i = 0; i < 90_000_000; i++) {
            ll.add(value);
        }
        System.out.println("LinkedList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromBegDLL() {
        System.out.println("-------- Removing by index from the begin --------");
        DoublyLinkedList<Byte> dll = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 120_000_000; i++) {
            dll.add(value);
        }
        timer.start();
        for (int i = 0; i < 120_000_000; i++) {
            dll.remove(0);
        }
        System.out.println("DoublyLinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromBegLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 120_000_000; i++) {
            ll.add(value);
        }
        timer.start();
        for (int i = 0; i < 120_000_000; i++) {
            ll.remove(0);
        }
        System.out.println("LinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromMidDLL() {
        System.out.println("-------- Removing by index from the middle --------");
        DoublyLinkedList<Byte> dll = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 120_000_000; i++) {
            dll.add(value);
        }
        timer.start();
        for (int i = 0; i < 110_000_000; i++) {
            dll.remove(10);
        }
        System.out.println("DoublyLinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromMidLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 120_000_000; i++) {
            ll.add(value);
        }
        timer.start();
        for (int i = 0; i < 110_000_000; i++) {
            ll.remove(10);
        }
        System.out.println("LinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromEndDLL() {
        System.out.println("-------- Removing by index from the end --------");
        DoublyLinkedList<Byte> dll = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 120_000_001; i++) {
            dll.add(value);
        }
        System.out.println("added");
        timer.start();
        for (int i = 120_000_000; i > 0; i--) {
            dll.remove(i);
        }
        System.out.println("DoublyLinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromEndLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 120_000_001; i++) {
            ll.add(value);
        }
        System.out.println("added");
        timer.start();
        for (int i = 120_000_000; i > 0; i--) {
            ll.remove(i);
        }
        System.out.println("LinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByObjectFromBegDLL() {
        System.out.println("-------- Removing by object from the begin --------");
        DoublyLinkedList<Byte> dll = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 120_000_000; i++) {
            dll.add(value);
        }
        System.out.println("added");
        timer.start();
        for (int i = 0; i < 120_000_000; i++) {
            dll.remove(value);
        }
        System.out.println("DoublyLinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByObjectFromBegLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 120_000_000; i++) {
            ll.add(value);
        }
        System.out.println("added");
        timer.start();
        for (int i = 0; i < 120_000_000; i++) {
            ll.remove(value);
        }
        System.out.println("LinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

}
