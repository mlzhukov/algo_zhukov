package com.gjj.training.algo1704.zhukovm.lesson04;


import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<E> extends AbstractList<E> implements List<E> {

    private int size;
    private int modCount;
    private Element<E> first;
    private Element<E> last;

    static DoublyLinkedList<Character> makeDefaultArray() {
        DoublyLinkedList<Character> dll = new DoublyLinkedList<>();
        Element<Character> node = new Element<>(null, null, 'b');
        dll.first = new Element<>(null, null, 'a');
        dll.last = new Element<>(null, null, 'c');
        dll.first.next = node;
        dll.last.prev = node;
        node.prev = dll.first;
        node.next = dll.last;
        dll.size = 3;
        return dll;
    }

    @Override
    public boolean add(final E e) {
        Element<E> l = last;
        Element<E> newNode = new Element<>(l, null, e);
        last = newNode;
        if (size == 0) {
            first = newNode;
        } else {
            l.next = newNode;
        }
        size++;
        modCount++;
        return true;
    }

    @Override
    public void add(final int index, final E element) {
        rangeCheckAdd(index);
        if (index == size) {
            add(element);
        } else {
            Element<E> tmp = find(index);
            if (tmp.prev == null) {
                Element<E> newNode = new Element<>(null, tmp, element);
                tmp.prev = newNode;
                first = newNode;
            } else {
                Element<E> newNode = new Element<>(tmp.prev, tmp, element);
                tmp.prev.next = newNode;
                tmp.prev = newNode;
            }
            size++;
            modCount++;
        }
    }

    @Override
    public boolean remove(final Object o) {
        if (o == null) {
            for (Element<E> x = first; x != null; x = x.next) {
                if (x.val == null) {
                    relink(x);
                    return true;
                }
            }
        } else {
            for (Element<E> x = first; x != null; x = x.next) {
                if (x.val.equals(o)) {
                    relink(x);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public E remove(final int index) {
        rangeCheck(index);
        return relink(find(index));
    }

    @Override
    public E get(final int index) {
        rangeCheck(index);
        return find(index).val;
    }

    private E relink(Element<E> node) {
        E tmp = node.val;
        if (size == 1) {
            node = null;
            first = last = null;
        } else if (node.prev == null) {
            node.next.prev = null;
            first = node.next;
            node.val = null;
        } else if (node.next == null) {
            node.prev.next = null;
            last = node.prev;
            node.val = null;
        } else {
            node.prev.next = node.next;
            node.next.prev = node.prev;
            node.val = null;
        }
        size--;
        modCount++;
        return tmp;
    }

    private Element<E> find(final int index) {
        if (index <= (size >> 1)) {
            Element<E> x = first;
            for (int i = 0; i < index; i++) {
                x = x.next;
            }
            return x;
        } else {
            Element<E> x = last;
            for (int i = size - 1; i > index; i--) {
                x = x.prev;
            }
            return x;
        }
    }

    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Element<E> x = first; x != null; x = x.next)
            result[i++] = x.val;
        return result;
    }

    public Object[] toArrayBack() {
        Object[] result = new Object[size];
        int i = 0;
        for (Element<E> x = last; x != null; x = x.prev)
            result[i++] = x.val;
        return result;
    }

    private void rangeCheck(final int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void rangeCheckAdd(final int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public ListIterator<E> listIterator() {
        return new ListIteratorImpl(0);
    }

    @Override
    public ListIterator<E> listIterator(final int index) {
        rangeCheckAdd(index);
        return new ListIteratorImpl(index);
    }

    private static class Element<E> {

        Element<E> prev;
        Element<E> next;
        E val;

        public Element(Element<E> prev, Element<E> next, E val) {
            this.val = val;
            this.next = next;
            this.prev = prev;
        }

    }

    class ListIteratorImpl implements ListIterator<E> {

        private Element<E> lastReturned;
        private Element<E> cursor;
        private int expectedModCount = modCount;
        private int nextIndex;

        public ListIteratorImpl(final int index) {
            cursor = (index == size) ? null : find(index);
            nextIndex = index;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size;
        }

        @Override
        public E next() {
            checkForModification();
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastReturned = cursor;
            cursor = cursor.next;
            nextIndex++;
            return lastReturned.val;
        }

        @Override
        public boolean hasPrevious() {
            return nextIndex != 0;
        }

        @Override
        public E previous() {
            checkForModification();
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            if (cursor == null) {
                lastReturned = last;
                cursor = last;
            } else {
                cursor = cursor.prev;
                lastReturned = cursor;
            }
            nextIndex--;
            return lastReturned.val;
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() {
            checkForModification();
            checkForIllegalState();
            Element<E> lastNext = lastReturned.next;
            relink(lastReturned);
            if (cursor == lastReturned) {
                cursor = lastNext;
            } else {
                nextIndex--;
            }
            lastReturned = null;
            expectedModCount++;
        }

        @Override
        public void set(E e) {
            checkForIllegalState();
            checkForModification();
            lastReturned.val = e;
        }

        @Override
        public void add(E e) {
            checkForModification();
            lastReturned = null;
            if (cursor == null)
                DoublyLinkedList.this.add(e);
            else
                DoublyLinkedList.this.add(nextIndex, e);
            nextIndex++;
            expectedModCount++;
        }

        private void checkForModification() {
            if (expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }
        }

        private void checkForIllegalState() {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
        }

    }

}
