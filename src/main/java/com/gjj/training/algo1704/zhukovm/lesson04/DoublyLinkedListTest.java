package com.gjj.training.algo1704.zhukovm.lesson04;

import static com.gjj.training.algo1704.zhukovm.lesson04.DoublyLinkedList.makeDefaultArray;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;

public class DoublyLinkedListTest {

    public static void main(String[] args) {
        testGetSuccess();
        testGetException();

        testAddByIndexToBeg();
        testAddByIndexToMid();
        testAddByIndexToEnd();
        testAddByIndexException();

        testRegularAddToBeg();
        testRegularAddToEnd();

        testRemoveByIndexFromBeg();
        testRemoveByIndexFromMid();
        testRemoveByIndexFromEnd();
        testRemoveByIndexException();

        testRemoveObjFromBeg();
        testRemoveObjFromMid();
        testRemoveObjFromEnd();
    }

    private static void testGetSuccess() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        assertEquals("DoublyLinkedListTest.testGetSuccess", (Character) 'a', dll.get(0));
    }

    private static void testGetException() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        try {
            dll.get(3);
            fail("AssertionError");
        } catch (IndexOutOfBoundsException e) {
            assertEquals("DoublyLinkedListTest.testGetException", true, e.getMessage() == null);
        }
    }

    private static void testAddByIndexToBeg() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'z', 'a', 'b', 'c'};
        dll.add(0, 'z');
        assertEquals("DoublyLinkedListTest.testAddByIndexToBeg", testArray, dll.toArray());
    }

    private static void testAddByIndexToMid() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'a', 'b', 'z', 'c'};
        dll.add(2, 'z');
        assertEquals("DoublyLinkedListTest.testAddByIndexToMid", testArray, dll.toArray());
    }

    private static void testAddByIndexToEnd() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'a', 'b', 'c', 'z'};
        dll.add(3, 'z');
        assertEquals("DoublyLinkedListTest.testAddByIndexToEnd", testArray, dll.toArray());
    }

    private static void testAddByIndexException() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        try {
            dll.add(5, 'z');
            fail("AssertionError");
        } catch (IndexOutOfBoundsException e) {
            assertEquals("DoublyLinkedListTest.testAddByIndexException", true, e.getMessage() == null);
        }
    }

    private static void testRegularAddToBeg() {
        DoublyLinkedList<Character> dll = new DoublyLinkedList<>();
        Object[] testArray = {'a'};
        dll.add('a');
        assertEquals("DoublyLinkedListTest.testRegularAddToBeg", testArray, dll.toArray());
    }

    private static void testRegularAddToEnd() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'a', 'b', 'c', 'z'};
        dll.add('z');
        assertEquals("DoublyLinkedListTest.testRegularAddToEnd", testArray, dll.toArray());
    }

    private static void testRemoveByIndexFromBeg() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'b', 'c'};
        dll.remove(0);
        assertEquals("DoublyLinkedListTest.testRemoveByIndexFromBeg", testArray, dll.toArray());
    }

    private static void testRemoveByIndexFromMid() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'a', 'c'};
        dll.remove(1);
        assertEquals("DoublyLinkedListTest.testRemoveByIndexFromMid", testArray, dll.toArray());
    }

    private static void testRemoveByIndexFromEnd() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'a', 'b'};
        dll.remove(2);
        assertEquals("DoublyLinkedListTest.testRemoveByIndexFromEnd", testArray, dll.toArray());
    }

    private static void testRemoveByIndexException() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        try {
            dll.remove(3);
            fail("AssertionError");
        } catch (IndexOutOfBoundsException e) {
            assertEquals("DoublyLinkedListTest.testRemoveByIndexException", true, e.getMessage() == null);
        }
    }

    private static void testRemoveObjFromBeg() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'b', 'c'};
        dll.remove((Character) 'a');
        assertEquals("DoublyLinkedListTest.testRemoveObjFromBeg", testArray, dll.toArray());
    }

    private static void testRemoveObjFromMid() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'a', 'c'};
        dll.remove((Character) 'b');
        assertEquals("DoublyLinkedListTest.testRemoveObjFromMid", testArray, dll.toArray());
    }

    private static void testRemoveObjFromEnd() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        Object[] testArray = {'a', 'b'};
        dll.remove((Character) 'c');
        assertEquals("DoublyLinkedListTest.testRemoveObjFromEnd", testArray, dll.toArray());
    }

}