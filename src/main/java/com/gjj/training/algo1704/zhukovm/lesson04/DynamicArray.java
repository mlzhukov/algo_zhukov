package com.gjj.training.algo1704.zhukovm.lesson04;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static java.util.Arrays.copyOf;

public class DynamicArray<E> extends AbstractList<E> implements List<E> {

    private static final int DEFAULT_CAPACITY = 10;
    private int size;
    private E[] elements;
    private int modCount;

    public DynamicArray() {
        elements = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public DynamicArray(final int capacity) {
        elements = (E[]) new Object[capacity];
    }

    public static void main(String[] args) {
        DynamicArray da = new DynamicArray();
        da.add(1);
        da.add(2);
        System.out.println(da.indexOf(2));
    }

    static DynamicArray makeDefaultArray() {
        DynamicArray array = new DynamicArray(5);
        array.elements[0] = 3.14;
        array.elements[1] = 1.1;
        array.elements[2] = 1.2;
        array.elements[3] = 1.3;
        array.elements[4] = 1.4;
        array.size = 5;
        return array;
    }

    public boolean add(final E e) {
        ensureCapacity();
        elements[size++] = e;
        modCount++;
        return true;
    }

    public void add(final int i, final E e) {
        rangeCheckAdd(i);
        ensureCapacity();
        System.arraycopy(elements, i, elements, i + 1, size - i);
        elements[i] = e;
        size++;
        modCount++;
    }

    @Override
    public E set(final int i, final E e) {
        rangeCheck(i);
        E tmp = elements[i];
        elements[i] = e;
        return (E) tmp;
    }

    public E get(final int i) {
        rangeCheck(i);
        return elements[i];
    }

    public E remove(final int i) {
        rangeCheck(i);
        E tmp = elements[i];
        System.arraycopy(elements, i + 1, elements, i, size - i - 1);
        elements[--size] = null;
        modCount++;
        return tmp;
    }

    public boolean remove(final Object e) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(e)) {
                System.arraycopy(elements, i + 1, elements, i, size - i - 1);
                elements[--size] = null;
                modCount++;
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int indexOf(final Object e) {
        for (int i = 0; i < size; i++) {
            if (e.equals(elements[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(final Object e) {
        for (int i = 0; i < size; i++) {
            if (e.equals(elements[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object[] toArray() {
        return copyOf(elements, size);
    }

    private void rangeCheck(final int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("ArrayIndexOutOfBoundsException");
        }
    }

    private void rangeCheckAdd(final int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException("ArrayIndexOutOfBoundsException");
        }
    }

    private int capacity() {
        return elements.length;
    }

    private void ensureCapacity() {
        if (elements.length == size) {
            elements = copyOf(elements, (int) (size * 1.5));
        }
    }

    public ListIterator<E> listIterator() {
        return new ListIteratorImpl(0);
    }

    public ListIterator<E> listIterator(int index) {
        return new ListIteratorImpl(index);
    }

    class ListIteratorImpl implements ListIterator<E> {

        private int cursor;
        private int expectedModCount = modCount;
        private int lastReturned = -1;

        public ListIteratorImpl(int index) {
            cursor = index;
        }

        public boolean hasNext() {
            return cursor != size;
        }

        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            checkForModification();
            lastReturned = cursor;
            return elements[cursor++];
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public E previous() {
            if (cursor == 0) {
                throw new NoSuchElementException();
            }
            checkForModification();
            lastReturned = --cursor;
            return elements[cursor];
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public void remove() {
            checkForModification();
            checkForIllegalState();
            DynamicArray.this.remove(lastReturned);
            cursor = lastReturned;
            lastReturned = -1;
            expectedModCount = ++modCount;
        }

        public void set(final E e) {
            checkForModification();
            checkForIllegalState();
            DynamicArray.this.set(lastReturned, e);
        }

        public void add(final E e) {
            checkForModification();
            DynamicArray.this.add(cursor++, e);
            lastReturned = -1;
            expectedModCount = ++modCount;
        }

        private void checkForModification() {
            if (expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }
        }

        private void checkForIllegalState() {
            if (lastReturned == -1) {
                throw new IllegalStateException();
            }
        }

    }

}