package com.gjj.training.algo1704.zhukovm.lesson04;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static com.gjj.training.algo1704.zhukovm.lesson04.DoublyLinkedList.makeDefaultArray;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;

public class IteratorTest {

    public static void main(String[] args) {
        testNext();
        testNextNoSuchElement();
        testNextConcurrentModification();
        testHasNext();
        testHasNoNext();
        testPrevious();
        testPreviousNoSuchElement();
        testPreviousConcurrentModification();
        testHasPrevious();
        testHasNoPrevious();
        testNextIndex();
        testPreviousIndex();
        testRemove();
        testRemoveIllegalState();
        testRemoveConcurrentModification();
        testSet();
        testSetIllegalState();
        testSetConcurrentModification();
        testAdd();
        testAddConcurrentModification();
    }

    private static void testNext() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        Object[] expectedArray = {'a', 'b', 'c'};
        Object[] testArray = new Object[3];
        for (int i = 0; i < 3; i++) {
            testArray[i] = li.next();
        }
        assertEquals("IteratorTest.testNext", expectedArray, testArray);
    }

    private static void testNextNoSuchElement() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(3);
        try {
            li.next();
            fail("AssertionError");
        } catch (NoSuchElementException e) {
            assertEquals("IteratorTest.testNextNoSuchElement", true, e.getMessage() == null);
        }
    }

    private static void testNextConcurrentModification() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        dll.add('e');
        try {
            li.next();
            fail("AssertionError");
        } catch (ConcurrentModificationException e) {
            assertEquals("IteratorTest.testNextConcurrentModification", true, e.getMessage() == null);
        }
    }

    private static void testHasNext() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(2);
        assertEquals("IteratorTest.testHasNext", true, li.hasNext());
    }

    private static void testHasNoNext() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(3);
        assertEquals("IteratorTest.testHasNext", false, li.hasNext());
    }

    private static void testPrevious() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(3);
        Object[] expectedArray = {'a', 'b', 'c'};
        Object[] testArray = new Object[3];
        for (int i = 2; i >= 0; i--) {
            testArray[i] = li.previous();
        }
        assertEquals("IteratorTest.testPrevious", expectedArray, testArray);
    }

    private static void testPreviousNoSuchElement() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(0);
        try {
            li.previous();
            fail("AssertionError");
        } catch (NoSuchElementException e) {
            assertEquals("IteratorTest.testPreviousNoSuchElement", true, e.getMessage() == null);
        }
    }

    private static void testPreviousConcurrentModification() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        dll.remove(2);
        try {
            li.previous();
            fail("AssertionError");
        } catch (ConcurrentModificationException e) {
            assertEquals("IteratorTest.testPreviousConcurrentModification", true, e.getMessage() == null);
        }
    }

    private static void testHasPrevious() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(3);
        assertEquals("IteratorTest.testHasPrevious", true, li.hasPrevious());
    }

    private static void testHasNoPrevious() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        assertEquals("IteratorTest.testHasNoPrevious", false, li.hasPrevious());
    }

    private static void testNextIndex() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(1);
        assertEquals("IteratorTest.testNextIndex", 1, li.nextIndex());
    }

    private static void testPreviousIndex() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(1);
        assertEquals("IteratorTest.testPreviousIndex", 0, li.previousIndex());
    }

    private static void testRemove() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(1);
        li.next();
        li.remove();
        Object[] expectedArray = {'a', 'c'};
        assertEquals("IteratorTest.testRemove", expectedArray, dll.toArray());
    }

    private static void testRemoveIllegalState() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        li.next();
        li.remove();
        try {
            li.remove();
            fail("AssertionError");
        } catch (IllegalStateException e) {
            assertEquals("IteratorTest.testPreviousNoSuchElement", true, e.getMessage() == null);
        }
    }

    private static void testRemoveConcurrentModification() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        li.next();
        dll.remove(2);
        try {
            li.remove();
            fail("AssertionError");
        } catch (ConcurrentModificationException e) {
            assertEquals("IteratorTest.testPreviousConcurrentModification", true, e.getMessage() == null);
        }
    }

    private static void testSet() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator(1);
        li.next();
        Object[] expectedArray = {'a', 's', 'c'};
        li.set('s');
        assertEquals("IteratorTest.testSet", expectedArray, dll.toArray());
    }

    private static void testSetIllegalState() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        li.next();
        li.add('c');
        try {
            li.set('e');
            fail("AssertionError");
        } catch (IllegalStateException e) {
            assertEquals("IteratorTest.testSetIllegalState", true, e.getMessage() == null);
        }
    }

    private static void testSetConcurrentModification() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        li.next();
        ListIterator<Character> li2 = dll.listIterator();
        li2.next();
        li2.remove();
        try {
            li.set('a');
            fail("AssertionError");
        } catch (ConcurrentModificationException e) {
            assertEquals("IteratorTest.testSetConcurrentModification", true, e.getMessage() == null);
        }
    }

    private static void testAdd() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        li.add('z');
        Object[] expectedArray = {'z', 'a', 'b', 'c'};
        assertEquals("IteratorTest.testAdd", expectedArray, dll.toArray());
    }

    private static void testAddConcurrentModification() {
        DoublyLinkedList<Character> dll = makeDefaultArray();
        ListIterator<Character> li = dll.listIterator();
        dll.remove(0);
        try {
            li.add('a');
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("IteratorTest.testAddConcurrentModification", true, e.getMessage() == null);
        }
    }

}
