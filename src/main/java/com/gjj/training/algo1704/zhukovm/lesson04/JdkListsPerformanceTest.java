package com.gjj.training.algo1704.zhukovm.lesson04;

import com.gjj.training.algo1704.zhukovm.util.StopWatch;

import java.util.ArrayList;
import java.util.LinkedList;

public class JdkListsPerformanceTest {

    public static void main(String[] args) {
//        testAddToBegAL();
//        testAddToBegLL();
//        testAddToMidAL();
//        testAddToMidLL();
//        testAddToEndAL();
//        testAddToEndLL();
//        testRemoveByIndexFromBegAL();
//        testRemoveByIndexFromBegLL();
//        testRemoveByIndexFromMidAL();
//        testRemoveByIndexFromMidLL();
//        testRemoveByIndexFromEndAL();
//        testRemoveByIndexFromEndLL();
//        testRemoveByObjectFromBegAL();
        testRemoveByObjectFromBegLL();
    }

    private static void testAddToBegAL() {
        System.out.println("-------- Addition to the begin --------");
        ArrayList<Byte> al = new ArrayList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            al.add(0, value);
        }
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddToBegLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            ll.add(0, value);
        }
        System.out.println("LinkedList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddToMidAL() {
        System.out.println("-------- Addition to the middle --------");
        ArrayList<Byte> al = new ArrayList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        al.add(value);
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            al.add(1, value);
        }
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddToMidLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        ll.add(value);
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            ll.add(1, value);
        }
        System.out.println("LinkedList.add(e): " + timer.getElapsedTime() + " ms");
    }


    private static void testAddToEndAL() {
        System.out.println("-------- Addition to the end --------");
        ArrayList<Byte> al = new ArrayList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        timer.start();
        for (int i = 0; i < 90_000_000; i++) {
            al.add(value);
        }
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddToEndLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        timer.start();
        for (int i = 0; i < 90_000_000; i++) {
            ll.add(value);
        }
        System.out.println("LinkedList.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromBegAL() {
        System.out.println("-------- Removing by index from the begin --------");
        ArrayList<Byte> al = new ArrayList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 300_000; i++) {
            al.add(value);
        }
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            al.remove(0);
        }
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromBegLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 300_000; i++) {
            ll.add(value);
        }
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            ll.remove(0);
        }
        System.out.println("LinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromMidAL() {
        System.out.println("-------- Removing by index from the middle --------");
        ArrayList<Byte> al = new ArrayList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 300_000; i++) {
            al.add(value);
        }
        timer.start();
        for (int i = 0; i < 295_000; i++) {
            al.remove(10);
        }
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromMidLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 300_000; i++) {
            ll.add(value);
        }
        timer.start();
        for (int i = 0; i < 295_000; i++) {
            ll.remove(10);
        }
        System.out.println("LinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromEndAL() {
        System.out.println("-------- Removing by index from the end --------");
        ArrayList al = new ArrayList();
        StopWatch timer = new StopWatch();
        byte value = 127;
        for (int i = 0; i < 400_000_001; i++) {
            al.add(value);
        }
        System.out.println("added");
        timer.start();
        for (int i = 400_000_000; i > 0; i--) {
            al.remove(i);
        }
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByIndexFromEndLL() {
        LinkedList ll = new LinkedList();
        StopWatch timer = new StopWatch();
        byte value = 127;
        for (int i = 0; i < 400_000_001; i++) {
            ll.add(value);
        }
        System.out.println("added");
        timer.start();
        for (int i = 400_000_000; i > 0; i--) {
            ll.remove(i);
        }
        System.out.println("LinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByObjectFromBegAL() {
        System.out.println("-------- Removing by object from the begin --------");
        ArrayList<Byte> al = new ArrayList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 300_000; i++) {
            al.add(value);
        }
        System.out.println("added");
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            al.remove(value);
        }
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveByObjectFromBegLL() {
        LinkedList<Byte> ll = new LinkedList<>();
        StopWatch timer = new StopWatch();
        Byte value = 127;
        for (int i = 0; i < 300_000; i++) {
            ll.add(value);
        }
        System.out.println("added");
        timer.start();
        for (int i = 0; i < 300_000; i++) {
            ll.remove(value);
        }
        System.out.println("LinkedList.remove(e): " + timer.getElapsedTime() + " ms");
    }

}
