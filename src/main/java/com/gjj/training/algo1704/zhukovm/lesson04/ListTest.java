package com.gjj.training.algo1704.zhukovm.lesson04;

import com.gjj.training.algo1704.zhukovm.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.Arrays.asList;

public class ListTest {

    public static void main(String[] args) {
        testAddAllByIndex();
        testAddByIndexToBeg();
        testAddByIndexToMid();
        testAddByIndexToEnd();
        testGet();
        testGetIndexOutOfBound();
        testIndexOf();
        testRemoveByIndexFromBeg();
        testRemoveByIndexFromMid();
        testRemoveByIndexFromEnd();
        testRemoveByIndexException();
        testSet();
        testSetIndexOutOfBound();
        testSubList();
    }

    private static void testAddAllByIndex() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        Collection<String> testList = new ArrayList<>(asList("fourth", "fifth"));
        Object[] expectedArray = {"first", "second", "fourth", "fifth", "third"};
        list.addAll(2, testList);
        Assert.assertEquals("CollectionTest.testAddAll", expectedArray, list.toArray());
    }

    private static void testAddByIndexToBeg() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        list.add(0, "zero");
        Object[] expectedArray = {"zero", "first", "second", "third"};
        Assert.assertEquals("ListTest.testAddByIndexToBeg", expectedArray, list.toArray());
    }

    private static void testAddByIndexToMid() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        list.add(2, "zero");
        Object[] expectedArray = {"first", "second", "zero", "third"};
        Assert.assertEquals("ListTest.testAddByIndexToMid", expectedArray, list.toArray());
    }

    private static void testAddByIndexToEnd() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        list.add(3, "zero");
        Object[] expectedArray = {"first", "second", "third", "zero"};
        Assert.assertEquals("ListTest.testAddByIndexToEnd", expectedArray, list.toArray());
    }

    private static void testGet() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        Assert.assertEquals("ListTest.testGet", "second", list.get(1));
    }

    private static void testGetIndexOutOfBound() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        try {
            list.get(4);
            Assert.fail("AssertionError");
        } catch (IndexOutOfBoundsException e) {
            Assert.assertEquals("ListTest.testGetIndexOutOfBound", "Index: 4, Size: 3", e.getMessage());
        }
    }

    private static void testIndexOf() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        Assert.assertEquals("ListTest.testIndexOf", 0, list.indexOf("first"));
    }

    private static void testRemoveByIndexFromBeg() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        list.remove(0);
        Object[] expectedArray = {"second", "third"};
        Assert.assertEquals("ListTest.testRemoveByIndexFromBeg", expectedArray, list.toArray());
    }

    private static void testRemoveByIndexFromMid() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        list.remove(1);
        Object[] expectedArray = {"first", "third"};
        Assert.assertEquals("ListTest.testRemoveByIndexFromMid", expectedArray, list.toArray());
    }

    private static void testRemoveByIndexFromEnd() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        list.remove(2);
        Object[] expectedArray = {"first", "second"};
        Assert.assertEquals("ListTest.testRemoveByIndexFromEnd", expectedArray, list.toArray());
    }

    private static void testRemoveByIndexException() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        try {
            list.remove(4);
            Assert.fail("AssertionError");
        } catch (IndexOutOfBoundsException e) {
            Assert.assertEquals("ListTest.testSetIndexOutOfBound", "Index: 4, Size: 3", e.getMessage());
        }
    }

    private static void testSet() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        list.set(1, "two");
        Object[] expectedArray = {"first", "two", "third"};
        Assert.assertEquals("ListTest.testSet", expectedArray, list.toArray());
    }

    private static void testSetIndexOutOfBound() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        try {
            list.set(4, "two");
            Assert.fail("AssertionError");
        } catch (IndexOutOfBoundsException e) {
            Assert.assertEquals("ListTest.testSetIndexOutOfBound", "Index: 4, Size: 3", e.getMessage());
        }
    }

    private static void testSubList() {
        List<String> list = new ArrayList<>(asList("first", "second", "third"));
        List<String> expectedList = new ArrayList<>(asList("second"));
        Assert.assertEquals("ListTest.testSublist", expectedList, list.subList(1, 2));
    }

}
