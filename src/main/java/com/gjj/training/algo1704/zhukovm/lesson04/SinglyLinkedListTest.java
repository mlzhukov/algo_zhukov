package com.gjj.training.algo1704.zhukovm.lesson04;

import java.util.ArrayList;
import java.util.List;

import static com.gjj.training.algo1704.zhukovm.lesson04.SinglyLinkedList.makeDefaultArray;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.Arrays.asList;

public class SinglyLinkedListTest {

    public static void main(String[] args) {
        testAsList();
        testAddToEmpty();
        testAdd();
        testGet();
        testSize();
        testReverse();
    }

    private static void testAsList() {
        SinglyLinkedList<Character> list = makeDefaultArray();
        List<Character> expectedList = new ArrayList<>(asList('a', 'b', 'c'));
        assertEquals("SinglyLinkedListTest.testAsList", expectedList, list.asList());
    }

    private static void testAddToEmpty() {
        SinglyLinkedList<Character> list = new SinglyLinkedList<>();
        list.add('a');
        List<Character> expectedList = new ArrayList<>(asList('a'));
        assertEquals("SinglyLinkedListTest.testAddToEmpty", expectedList, list.asList());
    }

    private static void testAdd() {
        SinglyLinkedList<Character> list = makeDefaultArray();
        list.add('z');
        List<Character> expectedList = new ArrayList<>(asList('a', 'b', 'c', 'z'));
        assertEquals("SinglyLinkedListTest.testAdd", expectedList, list.asList());
    }

    private static void testGet() {
        SinglyLinkedList<Character> list = makeDefaultArray();
        assertEquals("SinglyLinkedListTest.testGet", (Character) 'c', list.get(2));
    }

    private static void testSize() {
        SinglyLinkedList<Character> list = makeDefaultArray();
        assertEquals("SinglyLinkedListTest.testSize", 3, list.size());
    }

    private static void testReverse() {
        SinglyLinkedList<Character> list = makeDefaultArray();
        list.reverse();
        List<Character> expectedList = new ArrayList<>(asList('c', 'b', 'a'));
        assertEquals("SinglyLinkedListTest.testReverse", expectedList, list.asList());
    }

}
