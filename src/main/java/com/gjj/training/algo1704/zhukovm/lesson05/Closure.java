package com.gjj.training.algo1704.zhukovm.lesson05;

public interface Closure<E> {

    public void execute(E element);

}
