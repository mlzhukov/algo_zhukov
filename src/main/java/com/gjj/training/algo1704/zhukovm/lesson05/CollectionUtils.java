package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;

public class CollectionUtils {

    public static <E> boolean filter(Iterable<E> collection, Predicate<E> predicate) {
        boolean result = false;
        if (collection != null && predicate != null) {
            for (Iterator<E> iterator = collection.iterator(); iterator.hasNext(); ) {
                if (predicate.evaluate(iterator.next())) {
                    iterator.remove();
                    result = true;
                }
            }
        }
        return result;
    }

    public static <I, O> Collection<O> returnTransformed(Iterable<I> collection, Transformer<I, O> transformer) {
        Collection<O> outputCollection = new ArrayList<>();
        if (collection != null && transformer != null) {
            for (Iterator<I> iterator = collection.iterator(); iterator.hasNext(); ) {
                I element = iterator.next();
                O newElement = transformer.transform(element);
                outputCollection.add(newElement);
            }
        }
        return outputCollection;
    }

    public static <I, O> void transformCurrent(Collection collection, Transformer<I, O> transformer) {
        Collection<O> outputCollection = new ArrayList<>();
        if (collection != null && transformer != null) {
            for (Iterator iterator = collection.iterator(); iterator.hasNext(); ) {
                I element = (I) iterator.next();
                O newElement = transformer.transform(element);
                outputCollection.add(newElement);
            }
            collection.clear();
            collection.addAll(outputCollection);
        }
    }

    public static <E> void forAllDo(Iterable<E> collection, Closure<E> closure) {
        if (collection != null && closure != null) {
            for (Iterator<E> iterator = collection.iterator(); iterator.hasNext(); ) {
                E element = iterator.next();
                closure.execute(element);
            }
        }
    }

    public static <E> Collection<E> unmodifiableCollection(Collection<E> c) {
        return new UnmodifiableCollection<>(c);
    }

    static class UnmodifiableCollection<E> implements Collection<E> {

        private Collection<E> col;

        public UnmodifiableCollection(Collection<E> c) {
            this.col = c;
        }

        @Override
        public int size() {
            return col.size();
        }

        @Override
        public boolean isEmpty() {
            return col.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return col.contains(o);
        }

        @Override
        public Iterator<E> iterator() {
            return new Iterator<E>() {
                private final Iterator<? extends E> i = col.iterator();

                public boolean hasNext() {
                    return i.hasNext();
                }

                public E next() {
                    return i.next();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }

                @Override
                public void forEachRemaining(Consumer<? super E> action) {
                    // Use backing collection version
                    i.forEachRemaining(action);
                }
            };
        }

        @Override
        public Object[] toArray() {
            return col.toArray();
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return col.toArray(a);
        }

        @Override
        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return col.containsAll(c);
        }

        @Override
        public boolean addAll(Collection<? extends E> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }

    }

}
