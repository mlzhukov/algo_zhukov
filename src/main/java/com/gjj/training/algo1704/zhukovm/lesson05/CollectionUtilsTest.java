package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static com.gjj.training.algo1704.zhukovm.lesson05.CollectionUtils.*;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;
import static java.lang.Character.getNumericValue;
import static java.util.Arrays.asList;

public class CollectionUtilsTest {

    public static void main(String[] args) {
        testFilter();
        testReturnTransformed();
        testTransformCurrent();
        testForAllDo();

        testUnColSize();
        testUnColIsEmpty();
        testUnColContains();
        testUnColToArray();
        testUnColAdd();
        testUnColRemove();
        testUnColContainsAll();
        testUnColAddAll();
        testUnColRemoveAll();
        testUnColRetainAll();
        testUnColClear();
        testUnColIteratorRemove();
    }

    private static void testFilter() {
        Collection<Character> col = new ArrayList<>(asList('a', 'b', 'b', 'c'));
        filter(col, new Predicate<Character>() {
            @Override
            public boolean evaluate(Character object) {
                return object.equals('b');
            }
        });
        Character[] expectedArray = {'a', 'c'};
        assertEquals("CollectionUtilsTest.testFilter", expectedArray, col.toArray());
    }

    private static void testReturnTransformed() {
        Collection<Character> col = new ArrayList<>(asList('1', '2', '3', '4'));
        Collection<Integer> newCol =
                returnTransformed(col, new Transformer<Character, Integer>() {
                    @Override
                    public Integer transform(Character element) {
                        return getNumericValue(element);
                    }
                });
        assertEquals("CollectionUtilsTest.testReturnTransformedInstanceOf", true, newCol.toArray()[2] instanceof Integer);
        Iterator li = newCol.iterator();
        int expectedSum = 0;
        while (li.hasNext()) {
            expectedSum += (int) li.next();
        }
        assertEquals("CollectionUtilsTest.testReturnTransformedSum", 10, expectedSum);
    }

    private static void testTransformCurrent() {
        Collection<Character> col = new ArrayList<>(asList('1', '2', '3', '4'));
        transformCurrent(col, new Transformer<Character, Integer>() {
            @Override
            public Integer transform(Character element) {
                return getNumericValue(element);
            }
        });
        assertEquals("CollectionUtilsTest.testTransformCurrent", true, col.toArray()[2] instanceof Integer);
        Iterator li = col.iterator();
        int expectedSum = 0;
        while (li.hasNext()) {
            expectedSum += (int) li.next();
        }
        assertEquals("CollectionUtilsTest.testReturnTransformedSum", 10, expectedSum);
    }

    private static void testForAllDo() {
        class Name {
            private String name;

            public Name(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void modifyName() {
                name = name + "!!!";
            }
        }
        Collection<Name> list = new ArrayList<>();
        Name first = new Name("Ivan");
        Name second = new Name("John");
        list.add(first);
        list.add(second);
        forAllDo(list, new Closure<Name>() {
            @Override
            public void execute(Name element) {
                element.modifyName();
            }
        });
        Iterator<Name> iterator = list.iterator();
        String[] expectedArray = {"Ivan!!!", "John!!!"};
        String[] testArray = new String[2];
        for (int i = 0; i < 2; i++) {
            first = iterator.next();
            testArray[i] = first.getName();
        }
        assertEquals("CollectionUtilsTest.testForAllDo", expectedArray, testArray);
    }

    private static void testUnColSize() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        assertEquals("CollectionUtilsTest.testUnColSize", 3, unmodCollection.size());
    }

    private static void testUnColIsEmpty() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        assertEquals("CollectionUtilsTest.testUnColIsEmpty", false, unmodCollection.isEmpty());
    }

    private static void testUnColContains() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        assertEquals("CollectionUtilsTest.testUnColContains", true, unmodCollection.contains('b'));
    }

    private static void testUnColToArray() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        Character[] expectedArray = {'a', 'b', 'c'};
        assertEquals("CollectionUtilsTest.testUnColToArray", expectedArray, unmodCollection.toArray());
    }

    private static void testUnColAdd() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        try {
            unmodCollection.add('c');
            fail("AssertionError");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnColAdd", true, e.getMessage() == null);
        }
    }

    private static void testUnColRemove() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        try {
            unmodCollection.remove('c');
            fail("AssertionError");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnColRemove", true, e.getMessage() == null);
        }
    }

    private static void testUnColContainsAll() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        assertEquals("CollectionUtilsTest.testUnColContainsAll", true, unmodCollection.containsAll(collection));
    }

    private static void testUnColAddAll() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        try {
            unmodCollection.addAll(collection);
            fail("AssertionError");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnColAddAll", true, e.getMessage() == null);
        }
    }

    private static void testUnColRemoveAll() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        try {
            unmodCollection.removeAll(collection);
            fail("AssertionError");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnColRemoveAll", true, e.getMessage() == null);
        }
    }

    private static void testUnColRetainAll() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        try {
            unmodCollection.retainAll(collection);
            fail("AssertionError");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnColRetainAll", true, e.getMessage() == null);
        }
    }

    private static void testUnColClear() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        try {
            unmodCollection.clear();
            fail("AssertionError");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnColClear", true, e.getMessage() == null);
        }
    }

    private static void testUnColIteratorRemove() {
        Collection<Character> collection = new ArrayList<>(asList('a', 'b', 'c'));
        Collection<Character> unmodCollection = unmodifiableCollection(collection);
        Iterator<Character> iterator = unmodCollection.iterator();
        iterator.next();
        try {
            iterator.remove();
            fail("AssertionError");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnColIteratorRemove", true, e.getMessage() == null);
        }
    }

}
