package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;
import static java.util.Arrays.asList;

public class DequeTest {

    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testRemoveFirst();
        testRemoveLast();
        testRemoveFirstException();
        testRemoveLastException();
        testPollFirst();
        testPollLast();
        testPollFirstEmpty();
        testPollLastEmpty();
        testGetFirst();
        testGetLast();
        testGetFirstException();
        testGetLastException();
        testPeekFirst();
        testPeekLast();
        testPeekFirstException();
        testPeekLastException();
        testRemoveFirstOccurrence();
        testRemoveLastOccurrence();
        testPush();
        testPop();
        testRemove();
        testContains();
        testNotContains();
        testSize();
    }

    private static void testAddFirst() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.addFirst('0');
        Character[] expectedArray = {'0', 'a', 'b', 'c'};
        assertEquals("DequeTest.testAddFirst", expectedArray, deque.toArray());
    }

    private static void testAddLast() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.addLast('0');
        Character[] expectedArray = {'a', 'b', 'c', '0'};
        assertEquals("DequeTest.testAddFirst", expectedArray, deque.toArray());
    }

    private static void testOfferFirst() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.offerFirst('0');
        Character[] expectedArray = {'0', 'a', 'b', 'c'};
        assertEquals("DequeTest.testOfferFirst", expectedArray, deque.toArray());
    }

    private static void testOfferLast() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.offerLast('0');
        Character[] expectedArray = {'a', 'b', 'c', '0'};
        assertEquals("DequeTest.testOfferLast", expectedArray, deque.toArray());
    }

    private static void testRemoveFirst() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.removeFirst();
        Character[] expectedArray = {'b', 'c'};
        assertEquals("DequeTest.testRemoveFirst", expectedArray, deque.toArray());
    }

    private static void testRemoveLast() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.removeLast();
        Character[] expectedArray = {'a', 'b'};
        assertEquals("DequeTest.testRemoveLast", expectedArray, deque.toArray());
    }

    private static void testRemoveFirstException() {
        Deque<Character> deque = new ArrayDeque<>();
        try {
            deque.removeFirst();
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("DequeTest.testRemoveFirstException", true, e.getMessage() == null);
        }
    }

    private static void testRemoveLastException() {
        Deque<Character> deque = new ArrayDeque<>();
        try {
            deque.removeLast();
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("DequeTest.testRemoveFirstException", true, e.getMessage() == null);
        }
    }

    private static void testPollFirst() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.pollFirst();
        Character[] expectedArray = {'b', 'c'};
        assertEquals("DequeTest.testPollFirst", expectedArray, deque.toArray());
    }

    private static void testPollLast() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.pollLast();
        Character[] expectedArray = {'a', 'b'};
        assertEquals("DequeTest.testPollLast", expectedArray, deque.toArray());
    }

    private static void testPollFirstEmpty() {
        Deque<Character> deque = new ArrayDeque<>();
        assertEquals("DequeTest.testPollFirstEmpty", true, deque.pollFirst() == null);
    }

    private static void testPollLastEmpty() {
        Deque<Character> deque = new ArrayDeque<>();
        assertEquals("DequeTest.testPollLastEmpty", true, deque.pollLast() == null);
    }

    private static void testGetFirst() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("DequeTest.testGetFirst", (Character) 'a', deque.getFirst());
    }

    private static void testGetLast() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("DequeTest.testGetLast", (Character) 'c', deque.getLast());
    }

    private static void testGetFirstException() {
        Deque<Character> deque = new ArrayDeque<>();
        try {
            deque.getFirst();
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("DequeTest.testGetFirstException", true, e.getMessage() == null);
        }
    }

    private static void testGetLastException() {
        Deque<Character> deque = new ArrayDeque<>();
        try {
            deque.getLast();
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("DequeTest.testGetLastException", true, e.getMessage() == null);
        }
    }

    private static void testPeekFirst() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("DequeTest.testPeekFirst", (Character) 'a', deque.peekFirst());
    }

    private static void testPeekLast() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("DequeTest.testPeekLast", (Character) 'c', deque.peekLast());
    }

    private static void testPeekFirstException() {
        Deque<Character> deque = new ArrayDeque<>();
        assertEquals("DequeTest.testPeekFirstException", true, deque.peekFirst() == null);
    }

    private static void testPeekLastException() {
        Deque<Character> deque = new ArrayDeque<>();
        assertEquals("DequeTest.testPeekLastException", true, deque.peekLast() == null);
    }

    private static void testRemoveFirstOccurrence() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c', 'b'));
        deque.removeFirstOccurrence('b');
        Character[] expectedArray = {'a', 'c', 'b'};
        assertEquals("DequeTest.testRemoveFirstOccurrence", expectedArray, deque.toArray());
    }

    private static void testRemoveLastOccurrence() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c', 'b'));
        deque.removeLastOccurrence('b');
        Character[] expectedArray = {'a', 'b', 'c'};
        assertEquals("DequeTest.testRemoveFirstOccurrence", expectedArray, deque.toArray());
    }

    private static void testPush() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.push('z');
        Character[] expectedArray = {'z', 'a', 'b', 'c'};
        assertEquals("DequeTest.testPush", expectedArray, deque.toArray());
    }

    private static void testPop() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.pop();
        Character[] expectedArray = {'b', 'c'};
        assertEquals("DequeTest.testPop", expectedArray, deque.toArray());
    }

    private static void testRemove() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        deque.remove('b');
        Character[] expectedArray = {'a', 'c'};
        assertEquals("DequeTest.testRemove", expectedArray, deque.toArray());
    }

    private static void testContains() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("DequeTest.testContains", true, deque.contains('b'));
    }

    private static void testNotContains() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("DequeTest.testNotContains", false, deque.contains('z'));
    }

    private static void testSize() {
        Deque<Character> deque = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("DequeTest.testSize", 3, deque.size());
    }

}
