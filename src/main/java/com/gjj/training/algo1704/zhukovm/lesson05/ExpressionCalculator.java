package com.gjj.training.algo1704.zhukovm.lesson05;

import static java.lang.Character.getNumericValue;

public class ExpressionCalculator {

    private static String expression = "";
    private static LinkedListStack<Character> operatorStack = new LinkedListStack<>();

    public static String transformToPostfix(final String input) {
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            switch (c) {
                case '(':
                    operatorStack.push(c);
                    break;
                case ')':
                    ifParenthesis();
                    break;
                case '+': //fall through
                case '-':
                    ifOperation(c, 1);
                    break;
                case '/': //fall through
                case '*':
                    ifOperation(c, 2);
                    break;
                default:
                    expression = expression + c;
                    break;
            }
        }
        while (!operatorStack.asList().isEmpty()) {
            expression = expression + operatorStack.pop();
        }
        String result = expression;
        expression = "";
        return result;
    }

    public static int calculatePostfix(String expression) {
        Stack<Integer> operandStack = new LinkedListStack<>();
        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            if (c >= '0' && c <= '9') {
                operandStack.push(getNumericValue(c));
            } else {
                int op2 = operandStack.pop();
                int op1 = operandStack.pop();
                switch (c) {
                    case '+':
                        operandStack.push(op1 + op2);
                        break;
                    case '-':
                        operandStack.push(op1 - op2);
                        break;
                    case '*':
                        operandStack.push(op1 * op2);
                        break;
                    case '/':
                        operandStack.push(op1 * op2);
                        break;
                    default:
                        break;
                }
            }
        }
        return operandStack.pop();
    }

    private static void ifParenthesis() {
        while (!operatorStack.asList().isEmpty()) {
            char c = operatorStack.pop();
            if (c != '(') {
                expression = expression + c;
            } else {
                break;
            }
        }
    }

    private static void ifOperation(final char c, final int precedence) {
        while (!operatorStack.asList().isEmpty()) {
            char head = operatorStack.pop();
            if (head == '(') {
                operatorStack.push(head);
                break;
            } else {
                int headPrecedence;
                if (head == '+' || head == '-') {
                    headPrecedence = 1;
                } else {
                    headPrecedence = 2;
                }
                if (headPrecedence < precedence) {
                    operatorStack.push(head);
                    break;
                } else {
                    expression = expression + head;
                }
            }
        }
        operatorStack.push(c);
    }

}
