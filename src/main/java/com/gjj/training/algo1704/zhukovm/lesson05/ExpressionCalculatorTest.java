package com.gjj.training.algo1704.zhukovm.lesson05;

import static com.gjj.training.algo1704.zhukovm.lesson05.ExpressionCalculator.calculatePostfix;
import static com.gjj.training.algo1704.zhukovm.lesson05.ExpressionCalculator.transformToPostfix;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class ExpressionCalculatorTest {

    public static void main(String[] args) {
        testTransform();
        testCalculate();
    }

    private static void testTransform() {
        assertEquals("ExpressionCalculatorTest.testTransform", "12+4*3+", transformToPostfix("(1+2)*4+3"));
    }

    private static void testCalculate() {
        String expression = "12+4*3+";
        assertEquals("ExpressionCalculatorTest.testCalculate", 15, calculatePostfix(expression));
    }

}
