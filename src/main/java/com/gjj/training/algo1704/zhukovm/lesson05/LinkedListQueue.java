package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

public class LinkedListQueue<E> extends AbstractLLQueue<E> implements Queue<E> {

    private int size;

    private Node<E> head;
    private Node<E> tail;

    @Override
    public boolean add(final E e) {
        Node<E> current = tail;
        tail = new Node<E>(e, null);
        if (size == 0) {
            head = tail;
        } else {
            current.next = tail;
        }
        size++;
        return true;
    }

    @Override
    public E remove() {
        if (size == 0) {
            throw new NoSuchElementException();
        } else if (size == 1) {
            E tmp = head.val;
            head = tail = null;
            size--;
            return tmp;
        } else {
            E tmp = head.val;
            head = head.next;
            size--;
            return tmp;
        }

    }

    List<E> asList() {
        List<E> tmp = new ArrayList<>();
        for (Node<E> cursor = head; cursor != null; cursor = cursor.next) {
            tmp.add(cursor.val);
        }
        return tmp;
    }

}
