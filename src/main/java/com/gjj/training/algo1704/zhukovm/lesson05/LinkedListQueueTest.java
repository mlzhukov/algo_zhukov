package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;
import static java.util.Arrays.asList;

public class LinkedListQueueTest {

    public static void main(String[] args) {
        testAdd();
        testRemove();
        testRemoveException();
    }

    private static void testAdd() {
        LinkedListQueue<Character> queue = new LinkedListQueue<>();
        queue.add('a');
        queue.add('b');
        queue.add('c');
        List<Character> expectedList = new ArrayList<>(asList('a', 'b', 'c'));
        assertEquals("LinkedListQueueTest.testAdd", expectedList, queue.asList());
    }

    private static void testRemove() {
        LinkedListQueue<Character> queue = new LinkedListQueue<>();
        queue.add('a');
        queue.add('b');
        queue.add('c');
        queue.remove();
        List<Character> expectedList = new ArrayList<>(asList('b', 'c'));
        assertEquals("LinkedListQueueTest.testRemove", expectedList, queue.asList());
    }

    private static void testRemoveException() {
        LinkedListQueue<Character> queue = new LinkedListQueue<>();
        try {
            queue.remove();
            fail("AssertionError");
        } catch (NoSuchElementException e) {
            assertEquals("LinkedListQueueTest.testRemoveException", true, e.getMessage() == null);
        }
    }

}
