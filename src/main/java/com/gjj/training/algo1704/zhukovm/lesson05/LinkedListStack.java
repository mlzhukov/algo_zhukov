package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.List;

public class LinkedListStack<E> implements Stack<E> {

    private SinglyLinkedList<E> list;

    public LinkedListStack() {
        list = new SinglyLinkedList<>();
    }

    @Override
    public void push(final E e) {
        list.addFirst(e);
    }

    @Override
    public E pop() {
        return list.removeFirst();
    }

    List<E> asList() {
        return list.asList();
    }

}
