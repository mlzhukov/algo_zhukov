package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;
import static java.util.Arrays.asList;

public class LinkedListStackTest {

    public static void main(String[] args) {
        testPush();
        testPop();
        testPopException();
    }

    private static void testPush() {
        LinkedListStack<Character> stack = new LinkedListStack<>();
        stack.push('a');
        stack.push('b');
        List<Character> expectedList = new ArrayList<>(asList('b', 'a'));
        assertEquals("LinkedListStackTest.testPush", expectedList, stack.asList());
    }

    private static void testPop() {
        LinkedListStack<Character> stack = new LinkedListStack<>();
        stack.push('a');
        stack.push('b');
        stack.push('c');
        stack.pop();
        List<Character> expectedList = new ArrayList<>(asList('b', 'a'));
        assertEquals("LinkedListStackTest.testPop", expectedList, stack.asList());
    }

    private static void testPopException() {
        LinkedListStack<Character> stack = new LinkedListStack<>();
        try {
            stack.pop();
            fail("AssertionError");
        } catch (NoSuchElementException e) {
            assertEquals("LinkedListStackTest.testPopException", true, e.getMessage() == null);
        }
    }

}
