package com.gjj.training.algo1704.zhukovm.lesson05;

public class Node<E> {

    Node<E> next;
    E val;

    public Node() {
    }

    public Node(E val, Node<E> next) {
        this.val = val;
        this.next = next;
    }

}
