package com.gjj.training.algo1704.zhukovm.lesson05;

public interface Predicate<E> {

    boolean evaluate(E object);

}
