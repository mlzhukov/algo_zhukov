package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.ArrayDeque;
import java.util.Queue;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;
import static java.util.Arrays.asList;

public class QueueTest {

    public static void main(String[] args) {
        testAdd();
        testOffer();
        testRemove();
        testRemoveException();
        testPoll();
        testPollEmpty();
        testElement();
        testElementException();
        testPeek();
        testPeekEmpty();
    }

    private static void testAdd() {
        Queue<Character> queue = new ArrayDeque<>(asList('a', 'b', 'c'));
        queue.add('d');
        Character[] expectedArray = {'a', 'b', 'c', 'd'};
        assertEquals("QueueTest.testAdd", expectedArray, queue.toArray());
    }

    private static void testOffer() {
        Queue<Character> queue = new ArrayDeque<>(asList('a', 'b', 'c'));
        queue.offer('d');
        Character[] expectedArray = {'a', 'b', 'c', 'd'};
        assertEquals("QueueTest.testOffer", expectedArray, queue.toArray());
    }

    private static void testRemove() {
        Queue<Character> queue = new ArrayDeque<>(asList('a', 'b', 'c'));
        queue.remove();
        Character[] expectedArray = {'b', 'c'};
        assertEquals("QueueTest.testRemove", expectedArray, queue.toArray());
    }

    private static void testRemoveException() {
        Queue<Character> queue = new ArrayDeque<>();
        try {
            queue.remove();
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("QueueTest.testRemoveException", true, e.getMessage() == null);
        }
    }

    private static void testPoll() {
        Queue<Character> queue = new ArrayDeque<>(asList('a', 'b', 'c'));
        queue.poll();
        Character[] expectedArray = {'b', 'c'};
        assertEquals("QueueTest.testPoll", expectedArray, queue.toArray());
    }

    private static void testPollEmpty() {
        Queue<Character> queue = new ArrayDeque<>();
        assertEquals("QueueTest.testPollEmpty", true, queue.poll() == null);
    }

    private static void testElement() {
        Queue<Character> queue = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("QueueTest.testElement", (Character) 'a', queue.element());
    }

    private static void testElementException() {
        Queue<Character> queue = new ArrayDeque<>();
        try {
            queue.element();
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("QueueTest.testElementException", true, e.getMessage() == null);
        }
    }

    private static void testPeek() {
        Queue<Character> queue = new ArrayDeque<>(asList('a', 'b', 'c'));
        assertEquals("QueueTest.testPeek", (Character) 'a', queue.peek());
    }

    private static void testPeekEmpty() {
        Queue<Character> queue = new ArrayDeque<>();
        assertEquals("QueueTest.testPeek", true, queue.peek() == null);
    }

}
