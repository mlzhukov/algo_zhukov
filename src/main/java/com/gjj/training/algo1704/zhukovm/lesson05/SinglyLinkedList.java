package com.gjj.training.algo1704.zhukovm.lesson05;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class SinglyLinkedList<E> {

    private Node<E> head;

    static SinglyLinkedList<Character> makeDefaultArray() {
        Node<Character> first = new Node<>('a', null);
        Node<Character> second = new Node<>('b', null);
        Node<Character> third = new Node<>('c', null);
        SinglyLinkedList<Character> tmp = new SinglyLinkedList<>();
        first.next = second;
        second.next = third;
        tmp.head = first;
        return tmp;
    }

    public void add(final E e) {
        Node<E> newNode = new Node<E>(e, null);
        if (head == null) {
            head = newNode;
        } else {
            Node<E> cursor = head;
            while (cursor.next != null) {
                cursor = cursor.next;
            }
            cursor.next = newNode;
        }
    }

    public void addFirst(final E e) {
        Node<E> newNode = new Node<>(e, head);
        head = newNode;
    }

    public E removeFirst() {
        if (head == null) {
            throw new NoSuchElementException();
        }
        Node<E> tmp = head;
        head = head.next;
        return tmp.val;
    }

    public E get(final int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> cursor = head;
        for (int i = 0; i < index; i++) {
            cursor = cursor.next;
        }
        return cursor.val;
    }

    public int size() {
        if (head == null) {
            return 0;
        } else {
            int i = 0;
            for (Node<E> cursor = head; cursor != null; cursor = cursor.next) {
                i++;
            }
            return i;
        }
    }

    public void reverse() {
        Node<E> cursor = head;
        Node<E> nextNode = null;
        Node<E> prevNode = null;
        while (cursor != null) {
            nextNode = cursor.next;
            relink(cursor, prevNode);
            prevNode = cursor;
            cursor = nextNode;
        }
        head = prevNode;
    }

    public void relink(Node<E> cursor, Node<E> prev) {
        cursor.next = prev;
    }

    public List<E> asList() {
        List<E> tmp = new ArrayList<>();
        for (Node<E> cursor = head; cursor != null; cursor = cursor.next) {
            tmp.add(cursor.val);
        }
        return tmp;
    }

}
