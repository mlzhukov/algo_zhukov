package com.gjj.training.algo1704.zhukovm.lesson05;

interface Stack<E> {

    void push(E e); // add element to the top

    E pop(); // removes element from the top

}

