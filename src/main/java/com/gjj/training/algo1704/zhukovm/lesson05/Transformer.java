package com.gjj.training.algo1704.zhukovm.lesson05;

public interface Transformer<I, O> {

    O transform(I element);

}
