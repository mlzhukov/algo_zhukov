package com.gjj.training.algo1704.zhukovm.lesson06;

import com.gjj.training.algo1704.zhukovm.util.StopWatch;

import java.util.HashMap;

public class AAPerformanceTest {

    public static void main(String[] args) {
//        testAddAA();
//        testAddHashMap();
//        testRemoveHashMap();
//        testRemoveAA();
//        testGetHashMap();
        testGetAA();
    }

    private static void testAddHashMap() {
        StopWatch timer = new StopWatch();
        HashMap<Integer, Object> hashMap = new HashMap<>(400_000);
        timer.start();
        for (int i = 0; i < 24_000_000; i++) {
            hashMap.put(i, new Object());
        }
        System.out.println("HashMap.put(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testAddAA() {
        StopWatch timer = new StopWatch();
        AssociativeArray<Integer, Object> aa = new AssociativeArray<>(400_000);
        timer.start();
        for (int i = 0; i < 24_000_000; i++) {
            aa.add(i, new Object());
        }
        System.out.println("AssociativeArray.add(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveHashMap() {
        StopWatch timer = new StopWatch();
        HashMap<Integer, Object> hashMap = new HashMap<>(400_000);
        for (int i = 0; i < 60_000_000; i++) {
            hashMap.put(i, new Object());
        }
        System.out.println("added");
        timer.start();
        for (int i = 0; i < 60_000_000; i++) {
            hashMap.remove(i);
        }
        System.out.println("HashMap.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testRemoveAA() {
        StopWatch timer = new StopWatch();
        AssociativeArray<Integer, Object> aa = new AssociativeArray<>(400_000);
        for (int i = 0; i < 60_000_000; i++) {
            aa.add(i, new Object());
        }
        System.out.println("added");
        timer.start();
        for (int i = 0; i < 60_000_000; i++) {
            aa.remove(i);
        }
        System.out.println("AssociativeArray.remove(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testGetHashMap() {
        StopWatch timer = new StopWatch();
        HashMap<Integer, Object> hashMap = new HashMap<>(400_000);
        for (int i = 0; i < 60_000_000; i++) {
            hashMap.put(i, new Object());
        }
        System.out.println("added");
        timer.start();
        for (int i = 0; i < 60_000_000; i++) {
            hashMap.get(i);
        }
        System.out.println("HashMap.get(e): " + timer.getElapsedTime() + " ms");
    }

    private static void testGetAA() {
        StopWatch timer = new StopWatch();
        AssociativeArray<Integer, Object> aa = new AssociativeArray<>(400_000);
        for (int i = 0; i < 60_000_000; i++) {
            aa.add(i, new Object());
        }
        System.out.println("added");
        timer.start();
        for (int i = 0; i < 60_000_000; i++) {
            aa.get(i);
        }
        System.out.println("AssociativeArray.get(e): " + timer.getElapsedTime() + " ms");
    }

}
