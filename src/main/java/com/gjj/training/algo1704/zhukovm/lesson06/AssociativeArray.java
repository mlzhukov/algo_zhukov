package com.gjj.training.algo1704.zhukovm.lesson06;

import static java.lang.Math.abs;

public class AssociativeArray<K, V> {

    private static final double CONGESTION = 0.75;

    private Node<K, V>[] table;
    private int size;

    public AssociativeArray(int capacity) {
        table = new Node[capacity];
    }

    static AssociativeArray<Integer, String> makeDefaultArray(int capacity) {
        AssociativeArray<Integer, String> array = new AssociativeArray<>(capacity);
        Node<Integer, String> first = new Node<Integer, String>(1, "first", null);
        array.table[1] = new Node<Integer, String>(11, "eleven", first);
        array.table[2] = new Node<Integer, String>(2, "second", null);
        array.table[3] = new Node<Integer, String>(3, "third", null);
        return array;
    }

    V add(K key, V value) {
        if (size >= CONGESTION * table.length) {
            resize();
        }
        int position = hash(key);
        Node<K, V> newElement = new Node<>(key, value, null);
        if (table[position] == null) {
            table[position] = newElement;
        } else {
            for (Node<K, V> cursor = table[position]; cursor != null; cursor = cursor.next) {
                if (key == null && cursor.key == null || key != null && key.equals(cursor.key)) {
                    V oldValue = cursor.value;
                    cursor.value = value;
                    return oldValue;
                }
            }
            newElement.next = table[position];
            table[position] = newElement;
        }
        size++;
        return null;
    }

    V get(K key) {
        int position = hash(key);
        if (table[position] == null) {
            return null;
        } else {
            for (Node<K, V> cursor = table[position]; cursor != null; cursor = cursor.next) {
                if (key == null && cursor.key == null || key != null && key.equals(cursor.key)) {
                    return cursor.value;
                }
            }
        }
        return null;
    }

    void remove(K key) {
        int position = hash(key);
        if (table[position] != null) {
            if (key == null && table[position].key == null || key != null && key.equals(table[position].key)) {
                if (table[position].next == null) {
                    table[position] = null;
                } else {
                    table[position].key = null;
                    table[position].value = null;
                    table[position] = table[position].next;
                }
                size--;
            } else {
                for (Node<K, V> cursor = table[position]; cursor.next != null; cursor = cursor.next) {
                    if (key == null && cursor.next.key == null || key != null && key.equals(cursor.next.key)) {
                        cursor.next.value = null;
                        cursor.next.key = null;
                        cursor.next = cursor.next.next;
                        size--;
                        break;
                    }
                }
            }
        }
    }

    private int hash(Object key) {
        if (key == null) {
            return 0;
        } else {
            return abs(key.hashCode() % table.length);
        }
    }

    private void resize() {
        Node<K, V>[] oldTable = table;
        table = new Node[oldTable.length * 2];
        for (int i = 0; i < oldTable.length; i++) {
            Node<K, V> newNode;
            if ((newNode = oldTable[i]) != null) {
                oldTable[i] = null;
                if (newNode.next == null) {
                    table[hash(newNode.key)] = newNode;
                } else {
                    for (Node<K, V> cursor = newNode; cursor != null; cursor = cursor.next) {
                        if (table[hash(cursor.key)] == null) {
                            table[hash(cursor.key)] = cursor;
                        } else {
                            Node<K, V> tmp = new Node<>(cursor.key, cursor.value, table[hash(cursor.key)]);
                            table[hash(cursor.key)] = tmp.next;
                        }
                    }
                }
            }
        }
    }

    static class Node<K, V> {
        Node<K, V> next;
        private K key;
        private V value;

        public Node(K key, V value, Node<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }

}
