package com.gjj.training.algo1704.zhukovm.lesson06;

import static com.gjj.training.algo1704.zhukovm.lesson06.AssociativeArray.makeDefaultArray;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class AssociativeArrayTest {

    public static void main(String[] args) {
        testGetUniqueIndex();
        testGetSameIndex();
        testAddUniqueIndex();
        testAddSameIndex();
        testAddSameKey();
        testAddNullUniqueIndex();
        testAddNullSameIndex();
        testRemoveSameIndex();
        testRemoveUniqueIndex();
        testRemoveNullSameIndex();
        testRemoveNullUniqueIndex();
        testHash();
        testResizedArray();
    }

    private static void testGetUniqueIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        assertEquals("AssociativeArrayTest.testGetUniqueIndex", "first", aa.get(1));
    }

    private static void testGetSameIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        assertEquals("AssociativeArrayTest.testGetSameIndex", "eleven", aa.get(11));
    }

    private static void testAddUniqueIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.add(10, "ten");
        assertEquals("AssociativeArrayTest.testAddUniqueIndex", "ten", aa.get(10));
    }

    private static void testAddSameIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.add(10, "ten");
        aa.add(0, "zero");
        assertEquals("AssociativeArrayTest.testAddSameIndex", "ten", aa.get(10));
    }

    private static void testAddSameKey() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.add(10, "ten");
        aa.add(10, "tenten");
        assertEquals("AssociativeArrayTest.testAddSameKey", "tenten", aa.get(10));
    }

    private static void testAddNullUniqueIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.add(null, "null");
        assertEquals("AssociativeArrayTest.testAddNullUniqueIndex", "null", aa.get(null));
    }

    private static void testAddNullSameIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.add(null, "null");
        aa.add(10, "ten");
        assertEquals("AssociativeArrayTest.testAddNullSameIndex", "null", aa.get(null));
    }

    private static void testRemoveUniqueIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.remove(10);
        assertEquals("AssociativeArrayTest.testRemoveUniqueIndex", true, aa.get(10) == null);
    }

    private static void testRemoveSameIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.add(10, "ten");
        aa.add(0, "zero");
        aa.remove(10);
        assertEquals("AssociativeArrayTest.testRemoveSameIndex", true, aa.get(10) == null);
    }

    private static void testRemoveNullUniqueIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.add(null, "null");
        aa.remove(null);
        assertEquals("AssociativeArrayTest.testRemoveNullUniqueIndex", true, aa.get(null) == null);
    }

    private static void testRemoveNullSameIndex() {
        AssociativeArray<Integer, String> aa = makeDefaultArray(10);
        aa.add(null, "null");
        aa.add(10, "ten");
        aa.remove(null);
        assertEquals("AssociativeArrayTest.testRemoveNullSameIndex", true, aa.get(null) == null);
    }

    private static void testHash() {
        AssociativeArray<String, Integer> aa = new AssociativeArray<>(10);
        aa.add("polygenelubricants", 143);
        aa.add("random", 256);
        assertEquals("AssociativeArrayTest.testHash", (Integer) 143, aa.get("polygenelubricants"));

    }

    private static void testResizedArray() {
        AssociativeArray<Integer, Integer> ma = new AssociativeArray<>(5);
        ma.add(1, 1);
        ma.add(2, 2);
        ma.add(72, 72);
        ma.add(56, 56);
        ma.add(30, 30);
        ma.add(40, 40);
        ma.add(50, 50);
        ma.add(null, 1000);
        ma.add(80, 80);
        ma.add(8, 8);
        Integer[] expectedArray = {1, 2, 72, 56, 30, 40, 50, 1000, 80, 8};
        Integer[] testArray = {ma.get(1), ma.get(2), ma.get(72), ma.get(56),
                ma.get(30), ma.get(40), ma.get(50), ma.get(null),
                ma.get(80), ma.get(8)
        };
        assertEquals("AssociativeArrayTest.testResizedArray", expectedArray, testArray);
    }

}
