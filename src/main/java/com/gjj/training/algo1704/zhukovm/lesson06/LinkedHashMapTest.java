package com.gjj.training.algo1704.zhukovm.lesson06;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.AbstractMap.SimpleEntry;
import static java.util.Map.Entry;

public class LinkedHashMapTest {
    public static void main(String[] args) {
        testAddAndIterate();
    }

    private static void testAddAndIterate() {
        LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(3, "third");
        linkedHashMap.put(2, "second");
        linkedHashMap.put(1, "first");
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(3, "third");
        hashMap.put(2, "second");
        hashMap.put(1, "first");
        Entry[] expectedHMArray = {new SimpleEntry<>(1, "first"), new SimpleEntry<>(2, "second"), new SimpleEntry<>(3, "third")};
        Entry[] expectedLinkedHMArray = {new SimpleEntry<>(3, "third"), new SimpleEntry<>(2, "second"), new SimpleEntry<>(1, "first")};
        Entry[] testHMArray = new Entry[3];
        Entry[] testLinkedHMArray = new Entry[3];
        int i = 0;
        for (Entry entry : linkedHashMap.entrySet()) {
            testLinkedHMArray[i] = entry;
            i++;
        }
        i = 0;
        for (Entry entry : hashMap.entrySet()) {
            testHMArray[i] = entry;
            i++;
        }
        boolean isEqual = Arrays.equals(testHMArray, expectedHMArray) && Arrays.equals(testLinkedHMArray, expectedLinkedHMArray);
        assertEquals("LinkedHashMapTest.testAddAndIterate", true, isEqual);
    }

}
