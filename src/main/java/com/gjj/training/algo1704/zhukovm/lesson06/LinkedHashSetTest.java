package com.gjj.training.algo1704.zhukovm.lesson06;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.Arrays.asList;

public class LinkedHashSetTest {

    public static void main(String[] args) {
        testAddAndIterate();
    }

    private static void testAddAndIterate() {
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.addAll(asList(3, 2, 1));
        HashSet<Integer> hashSet = new HashSet<>();
        hashSet.addAll(asList(3, 2, 1));
        Object[] expectedLinkedHSArray = {3, 2, 1};
        Object[] expectedHSArray = {1, 2, 3};
        Object[] testHSArray = hashSet.toArray();
        Object[] testLinkedHSArray = linkedHashSet.toArray();
        boolean isEquals = Arrays.equals(expectedHSArray, testHSArray) && Arrays.equals(expectedLinkedHSArray, testLinkedHSArray);
        assertEquals("LinkedHashSetTest.testAddAndIterate", true, isEquals);
    }

}
