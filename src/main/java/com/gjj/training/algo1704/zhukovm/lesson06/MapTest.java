package com.gjj.training.algo1704.zhukovm.lesson06;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.AbstractMap.SimpleEntry;
import static java.util.Arrays.asList;
import static java.util.Map.Entry;


public class MapTest {

    public static void main(String[] args) {
        testSize();
        testIsEmpty();
        testIsNotEmpty();
        testHashCode();
        testContainsKey();
        testContainsValue();
        testNotContainsKey();
        testNotContainsValue();
        testGet();
        testNotGet();
        testGetByNull();
        testPutReplace();
        testRemove();
        testPutAll();
        testClear();
        testKeySet();
        testValues();
        testEntrySet();
        testEquals();
        testNotEquals();
    }

    private static void testSize() {
        Map<Integer, String> map = new HashMap<>();
        assertEquals("MapTest.testSize", 0, map.size());
    }

    private static void testIsEmpty() {
        Map<Integer, String> map = new HashMap<>();
        assertEquals("MapTest.testIsEmpty", true, map.isEmpty());
    }

    private static void testIsNotEmpty() {
        Map<Integer, String> map = new HashMap<>();
        map.put(4, "four");
        assertEquals("MapTest.testIsNotEmpty", false, map.isEmpty());
    }

    private static void testHashCode() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "polygenelubricants");
        assertEquals("MapTest.testHashCode", -2147483647, map.hashCode());

    }

    private static void testContainsKey() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "polygenelubricants");
        assertEquals("MapTest.testContainsKey", true, map.containsKey(1));
    }

    private static void testNotContainsKey() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "polygenelubricants");
        assertEquals("MapTest.testNotContainsKey", false, map.containsKey(2));
    }

    private static void testContainsValue() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "polygenelubricants");
        assertEquals("MapTest.testContainsValue", true, map.containsValue("polygenelubricants"));
    }

    private static void testNotContainsValue() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "polygenelubricants");
        assertEquals("MapTest.testNotContainsValue", false, map.containsValue("poly"));
    }

    private static void testGet() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "polygenelubricants");
        assertEquals("MapTest.testGet", "polygenelubricants", map.get(1));
    }

    private static void testNotGet() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "polygenelubricants");
        assertEquals("MapTest.testNotGet", true, map.get(2) == null);
    }

    private static void testGetByNull() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        assertEquals("MapTest.testGetByNull", "polygenelubricants", map.get(null));
    }

    private static void testPutReplace() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.put(null, "random");
        assertEquals("MapTest.testPutReplace", "random", map.get(null));
    }

    private static void testRemove() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.remove(null);
        assertEquals("MapTest.testRemove", true, map.get(null) == null);
    }

    private static void testPutAll() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.put(1, "random");
        Map<Integer, String> secondMap = new HashMap<>();
        secondMap.putAll(map);
        assertEquals("MapTest.testPutAll", true, secondMap.containsKey(1) && secondMap.containsKey(null));
    }

    private static void testClear() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.put(1, "random");
        map.clear();
        assertEquals("MapTest.testClear", true, map.isEmpty());
    }

    private static void testKeySet() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.put(1, "random");
        Set<Integer> expectedSet = new HashSet<>(asList(null, 1));
        assertEquals("MapTest.testKeySet", expectedSet, map.keySet());
    }

    private static void testValues() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.put(1, "random");
        String[] expectedCol = {"polygenelubricants", "random"};
        assertEquals("MapTest.testValues", expectedCol, map.values().toArray());
    }

    private static void testEntrySet() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.put(1, "random");
        SimpleEntry<Integer, String> first = new SimpleEntry<>(null, "polygenelubricants");
        SimpleEntry<Integer, String> second = new SimpleEntry<>(1, "random");
        Set<Entry<Integer, String>> expectedSet = new HashSet<>(asList(first, second));
        Set<Entry<Integer, String>> testSet = map.entrySet();
        testSet.add(new SimpleEntry<Integer, String>(3, "three"));
        assertEquals("MapTest.testEntrySet", expectedSet, map.entrySet());
    }

    private static void testEquals() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.put(1, "random");
        Map<Integer, String> secondMap = new HashMap<>();
        secondMap.put(1, "random");
        secondMap.put(null, "polygenelubricants");
        assertEquals("MapTest.testEquals", true, map.equals(secondMap));
    }

    private static void testNotEquals() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "polygenelubricants");
        map.put(1, "random");
        Map<Integer, String> secondMap = new HashMap<>();
        secondMap.put(1, "random");
        assertEquals("MapTest.testNotEquals", false, map.equals(secondMap));
    }

}
