package com.gjj.training.algo1704.zhukovm.lesson06;

public interface Matrix<V> {

    V get(int i, int j);

    void set(int i, int j, V value);

}
