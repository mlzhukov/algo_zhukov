package com.gjj.training.algo1704.zhukovm.lesson06;

import java.util.HashMap;
import java.util.Map;

public class MatrixImpl<V> implements Matrix<V> {

    private Map<MatrixKey, V> matrix = new HashMap<>();

    @Override
    public V get(int i, int j) {
        MatrixKey key = new MatrixKey(i, j);
        return matrix.get(key);
    }

    @Override
    public void set(int i, int j, V value) {
        MatrixKey key = new MatrixKey(i, j);
        matrix.put(key, value);
    }

    public int size() {
        return matrix.size();
    }

    static class MatrixKey {
        private int x;
        private int y;
        private int hashValue;

        public MatrixKey(final int x, final int y) {
            this.x = x;
            this.y = y;
            hashValue = ((Integer) (x + y)).hashCode();
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof MatrixKey) {
                MatrixKey tmp = (MatrixKey) obj;
                return (tmp.x == x) && (tmp.y == y);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return hashValue;
        }
    }

}
