package com.gjj.training.algo1704.zhukovm.lesson06;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class MatrixImplTest {

    public static void main(String[] args) {
        testMatrix();
    }

    private static void testMatrix() {
        MatrixImpl<Integer> matrix = new MatrixImpl<>();
        for (int i = 0; i < 1_000_000; i++) {
            matrix.set(i, 999_999, i);
        }
        boolean size = matrix.size() == 1_000_000;
        boolean elements = true;
        for (int i = 0; i < 1_000_000; i++) {
            elements &= i == matrix.get(i, 999_999);
        }
        assertEquals("MatrixImplTest.testMatrix", true, size && elements);
    }

}
