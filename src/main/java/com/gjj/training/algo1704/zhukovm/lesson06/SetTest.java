package com.gjj.training.algo1704.zhukovm.lesson06;

import java.util.HashSet;
import java.util.Set;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.Arrays.asList;

public class SetTest {

    public static void main(String[] args) {
        testAdd();
        testAddDuplicate();
        testAddAllUnique();
        testAddAllNotUnique();
    }

    private static void testAdd() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        Integer[] expectedArray = {1};
        assertEquals("SetTest.testAdd", expectedArray, set.toArray());
    }

    private static void testAddDuplicate() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(1);
        Integer[] expectedArray = {1};
        assertEquals("SetTest.testAddDuplicate", expectedArray, set.toArray());
    }

    private static void testAddAllUnique() {
        Set<Integer> set = new HashSet<>(asList(4, 5, 6, 7));
        Integer[] expectedArray = {4, 5, 6, 7};
        assertEquals("SetTest.testAddAllUnique", expectedArray, set.toArray());
    }

    private static void testAddAllNotUnique() {
        Set<Integer> set = new HashSet<>(asList(4, 4, 4, 5, 5, 5, 6, 6, 7));
        Integer[] expectedArray = {4, 5, 6, 7};
        assertEquals("SetTest.testAddAllNotUnique", expectedArray, set.toArray());
    }

}
