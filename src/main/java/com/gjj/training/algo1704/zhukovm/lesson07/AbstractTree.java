package com.gjj.training.algo1704.zhukovm.lesson07;

import java.util.*;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return root() == n;
    }

    @Override
    public boolean isEmpty() {
        return root() != null;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */
    public Collection<Node<E>> preOrder() {
        Collection<Node<E>> nodes = new ArrayList<>();
        preOrderImpl(root(), nodes);
        return nodes;
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Collection<Node<E>> postOrder() {
        Collection<Node<E>> nodes = new ArrayList<>();
        postOrderImpl(root(), nodes);
        return nodes;
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Collection<Node<E>> breadthFirst() {
        Collection<Node<E>> nodes = new ArrayList<>();
        breadthFirstImpl(root(), nodes);
        return nodes;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */

    private void preOrderImpl(Node<E> cursorNode, Collection<Node<E>> nodes) {
        if (cursorNode == null) {
            return;
        } else {
            nodes.add(cursorNode);
            Collection<Node<E>> children = children(cursorNode);
            Iterator<Node<E>> iterator = children.iterator();
            while (iterator.hasNext()) {
                preOrderImpl(iterator.next(), nodes);
            }
        }
    }

    private void postOrderImpl(Node<E> cursorNode, Collection<Node<E>> nodes) {
        if (cursorNode == null) {
            return;
        } else {
            Collection<Node<E>> children = children(cursorNode);
            Iterator<Node<E>> iterator = children.iterator();
            while (iterator.hasNext()) {
                postOrderImpl(iterator.next(), nodes);
            }
            nodes.add(cursorNode);
        }
    }

    private void breadthFirstImpl(Node<E> root, Collection<Node<E>> nodes) {
        Queue<Node<E>> queue = new ArrayDeque<>();
        if (root == null) {
            return;
        }
        queue.add(root);
        while (!queue.isEmpty()) {
            Node<E> node = queue.remove();
            nodes.add(node);
            queue.addAll(children(node));
        }
    }

    private class ElementIterator implements Iterator<E> {
        Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
