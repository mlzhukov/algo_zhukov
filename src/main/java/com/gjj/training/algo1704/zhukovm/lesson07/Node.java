package com.gjj.training.algo1704.zhukovm.lesson07;

/**
 * An abstraction for a node of a tree
 *
 * @param <E> element
 */
public interface Node<E> {

    /**
     * @return the element stored at this node
     */
    E getElement();

}
