package com.gjj.training.algo1704.zhukovm.lesson07.binary;

import com.gjj.training.algo1704.zhukovm.lesson07.AbstractTree;
import com.gjj.training.algo1704.zhukovm.lesson07.Node;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        Node<E> parent = parent(n);
        Node<E> parentLeft = left(parent);
        Node<E> parentRight = right(parent);
        if (parentLeft == n) {
            return parentRight;
        } else {
            return parentLeft;
        }
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        Node<E> left = left(n);
        Node<E> right = right(n);
        Collection<Node<E>> nodes = new ArrayList<>();
        if (left != null) {
            nodes.add(left);
        }
        if (right != null) {
            nodes.add(right);
        }
        return nodes;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        Node<E> left = left(n);
        Node<E> right = right(n);
        if (left == null && right == null) {
            return 0;
        } else if (left != null && right != null) {
            return 2;
        } else {
            return 1;
        }
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        Collection<Node<E>> nodes = new ArrayList<>();
        inOrderImpl(root(), nodes);
        return nodes;
    }

    private void inOrderImpl(Node<E> cursorNode, Collection<Node<E>> nodes) {
        if (cursorNode == null) {
            return;
        } else {
            inOrderImpl(left(cursorNode), nodes);
            nodes.add(cursorNode);
            inOrderImpl(right(cursorNode), nodes);
        }
    }

}
