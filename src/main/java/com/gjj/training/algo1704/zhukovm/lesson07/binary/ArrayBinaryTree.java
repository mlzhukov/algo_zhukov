package com.gjj.training.algo1704.zhukovm.lesson07.binary;

import com.gjj.training.algo1704.zhukovm.lesson07.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static java.util.Arrays.copyOf;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {

    private NodeImpl<E>[] array = new NodeImpl[10];
    private int size;

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        int index = indexOf(p);
        if (index == -1) {
            throw new IllegalArgumentException();
        }
        index = 2 * index + 1;
        ensureCapacity(index);
        return array[index];

    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        int index = indexOf(p);
        if (index == -1) {
            throw new IllegalArgumentException();
        }
        index = 2 * index + 2;
        ensureCapacity(index);
        return array[index];
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        Node<E> left = left(n);
        if (left == null) {
            NodeImpl<E> newNode = new NodeImpl<>(e);
            int index = 2 * indexOf(n) + 1;
            ensureCapacity(index);
            array[index] = newNode;
            size++;
            return newNode;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        Node<E> right = right(n);
        if (right == null) {
            NodeImpl<E> newNode = new NodeImpl<>(e);
            int index = 2 * indexOf(n) + 2;
            ensureCapacity(index);
            array[index] = newNode;
            size++;
            return newNode;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> root() {
        return array[0];
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        int index = indexOf(n);
        if (index == -1) {
            throw new IllegalArgumentException();
        } else if (index == 0) {
            return null;
        } else if (index % 2 == 0) {
            index = (index - 2) / 2;
            return array[index];
        } else {
            index = (index - 1) / 2;
            return array[index];
        }

    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (array[0] != null) {
            throw new IllegalStateException();
        }
        NodeImpl<E> newNode = new NodeImpl<>(e);
        array[0] = newNode;
        size++;
        return newNode;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        int childrenNum = childrenNumber(n);
        if (childrenNum == 0) {
            return addLeft(n, e);
        } else if (childrenNum == 1) {
            try {
                return addLeft(n, e);
            } catch (Exception ex) {
                return addRight(n, e);
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        int index = indexOf(n);
        if (index == -1) {
            throw new IllegalArgumentException();
        } else {
            E oldValue = array[index].getElement();
            NodeImpl<E> newNode = new NodeImpl<>(e);
            array[index] = newNode;
            return oldValue;
        }
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int index = indexOf(n);
        if (index == -1) {
            throw new IllegalArgumentException();
        }
        int childrenNum = childrenNumber(n);
        E oldValue = array[index].getElement();
        if (childrenNum > 1) {
            throw new IllegalArgumentException();
        } else if (childrenNum == 0) {
            array[index] = null;
        } else {
            Node<E> left = left(n);
            Node<E> right = right(n);
            Collection<Integer> oldIndexes = makeListOfIndexes(index);
            if (left != null && right == null) {
                int newIndex = index * 2 + 1;
                Collection<Integer> newIndexes = makeListOfIndexes(newIndex);
                replaceIndexes(oldIndexes, newIndexes);
            } else if (left == null && right != null) {
                int newIndex = index * 2 + 2;
                Collection<Integer> newIndexes = makeListOfIndexes(newIndex);
                replaceIndexes(oldIndexes, newIndexes);
            }
        }
        return oldValue;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Collection<Node<E>> nodes() {
        return breadthFirst();
    }

    private int indexOf(Node<E> p) {
        if (p == null) {
            for (int i = 0; i < array.length; i++)
                if (array[i] == null)
                    return i;
        } else {
            for (int i = 0; i < array.length; i++)
                if (p.equals(array[i]))
                    return i;
        }
        return -1;
    }

    private void ensureCapacity(int index) {
        if (index >= array.length) {
            array = copyOf(array, (int) (array.length * 1.5));
        }
    }

    private void replaceIndexes(Collection<Integer> oldIndexes, Collection<Integer> newIndexes) {
        Iterator<Integer> oldIt = oldIndexes.iterator();
        Iterator<Integer> newIt = newIndexes.iterator();
        while (newIt.hasNext()) {
            int oldIndex = oldIt.next();
            int newIndex = newIt.next();
            array[oldIndex] = array[newIndex];
            array[newIndex] = null;
        }
    }

    private ArrayList<Integer> makeListOfIndexes(int index) {
        ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<Integer> result = new ArrayList<>();
        int currentIndex = index;
        temp.add(currentIndex);
        while (!temp.isEmpty()) {
            currentIndex = temp.remove(0);
            result.add(currentIndex);
            if ((currentIndex * 2 + 2) <= array.length) {
                temp.add(currentIndex * 2 + 1);
                temp.add(currentIndex * 2 + 2);
            }
        }
        return result;
    }

    protected static class NodeImpl<E> implements Node<E> {

        E value;

        public NodeImpl(E value) {
            this.value = value;
        }

        @Override
        public E getElement() {
            return this.value;
        }

        @Override
        public boolean equals(Object object) {
            NodeImpl obj = (NodeImpl) object;
            if (obj == null) {
                return false;
            } else {
                return this.value.equals(obj.value);
            }
        }
    }

}
