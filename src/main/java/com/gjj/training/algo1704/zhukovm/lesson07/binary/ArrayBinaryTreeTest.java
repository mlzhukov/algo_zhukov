package com.gjj.training.algo1704.zhukovm.lesson07.binary;

import com.gjj.training.algo1704.zhukovm.lesson07.AbstractTree;
import com.gjj.training.algo1704.zhukovm.lesson07.Node;
import com.gjj.training.algo1704.zhukovm.lesson07.Tree;

import java.util.Collection;
import java.util.Iterator;

import static com.gjj.training.algo1704.zhukovm.lesson07.binary.ArrayBinaryTree.NodeImpl;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;

public class ArrayBinaryTreeTest {

    public static void main(String[] args) {
        testAddRoot();
        testAddRootException();
        testAddToRoot();
        testAddException();
        testAddToLeaf();
        testSize();
        testAddRightAndLeft();
        testAddRightException();
        testAddLeftException();
        testSet();
        testParent();
        testRemoveException();
        testRemoveLeftLeaf();
        testRemoveRightLeaf();
        testRemoveMid();
        testPreOrder();
        testPostOrder();
        testBreadthFirst();
        testInOrder();
        testAnotherRemove();
    }

    private static void testAddRoot() {
        Tree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> expectedRoot = new NodeImpl<>(1);
        boolean testEqual = expectedRoot.equals(tree.addRoot(1)) && expectedRoot.equals(tree.root());
        assertEquals("ArrayBinaryTreeTest.testAddRoot", true, testEqual);
    }

    private static void testAddRootException() {
        Tree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        try {
            tree.addRoot(2);
            fail("AssertionError");
        } catch (IllegalStateException e) {
            assertEquals("ArrayBinaryTreeTest.testAddRootException", true, e.getMessage() == null);
        }
    }

    private static void testAddToRoot() {
        Tree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[3];
        Integer[] expectedArray = {1, 2, 3};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("ArrayBinaryTreeTest.testAddToRoot", expectedArray, testArray);
    }

    private static void testAddException() {
        Tree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        try {
            tree.add(root, 5);
            fail("AssertionError");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testAddException", true, e.getMessage() == null);
        }
    }

    private static void testSize() {
        Tree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        assertEquals("ArrayBinaryTreeTest.testSize", 2, tree.size());
    }

    private static void testAddToLeaf() {
        Tree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {1, 2, 3, 4, 5};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("ArrayBinaryTreeTest.testAddToLeaf", expectedArray, testArray);
    }

    private static void testAddRightAndLeft() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addRight(root, 2);
        Node<Integer> third = tree.addLeft(root, 3);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[3];
        Integer[] expectedArray = {1, 3, 2};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("ArrayBinaryTreeTest.testAddRightAndLeft", expectedArray, testArray);
    }

    private static void testAddRightException() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addRight(root, 2);
        try {
            tree.addRight(root, 3);
            fail("AssertionError");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testAddRightException", true, e.getMessage() == null);
        }
    }

    private static void testAddLeftException() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addLeft(root, 2);
        try {
            tree.addLeft(root, 3);
            fail("AssertionError");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testAddLeftException", true, e.getMessage() == null);
        }
    }

    private static void testSet() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addLeft(root, 2);
        tree.set(second, 10);
        assertEquals("ArrayBinaryTreeTest.testSet", (Integer) 10, tree.left(tree.root()).getElement());
    }

    private static void testParent() {
        Tree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(second, 3);
        Node<Integer> fourth = tree.add(third, 4);
        assertEquals("ArrayBinaryTreeTest.testParent", third.getElement(), tree.parent(fourth).getElement());
    }

    private static void testPreOrder() {
        AbstractTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Collection<Node<Integer>> preOrder = tree.preOrder();
        Iterator<Node<Integer>> it = preOrder.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {1, 2, 4, 5, 3};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next().getElement();
        }
        assertEquals("ArrayBinaryTreeTest.testPreOrder", expectedArray, testArray);
    }

    private static void testPostOrder() {
        AbstractTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Collection<Node<Integer>> postOrder = tree.postOrder();
        Iterator<Node<Integer>> it = postOrder.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {4, 5, 2, 3, 1};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next().getElement();
        }
        assertEquals("ArrayBinaryTreeTest.testPostOrder", expectedArray, testArray);
    }

    private static void testBreadthFirst() {
        AbstractTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Collection<Node<Integer>> breadthFirst = tree.breadthFirst();
        Iterator<Node<Integer>> it = breadthFirst.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {1, 2, 3, 4, 5};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next().getElement();
        }
        assertEquals("ArrayBinaryTreeTest.testBreadthFirst", expectedArray, testArray);
    }

    private static void testInOrder() {
        AbstractBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Collection<Node<Integer>> inOrder = tree.inOrder();
        Iterator<Node<Integer>> it = inOrder.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {4, 2, 5, 1, 3};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next().getElement();
        }
        assertEquals("ArrayBinaryTreeTest.testInOrder", expectedArray, testArray);
    }

    private static void testRemoveException() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addLeft(root, 2);
        Node<Integer> third = tree.add(second, 3);
        Node<Integer> fourth = tree.add(second, 4);
        try {
            tree.remove(second);
            fail("AssertionError");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testRemoveException", true, e.getMessage() == null);
        }
    }

    private static void testRemoveLeftLeaf() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addLeft(root, 2);
        tree.remove(second);
        assertEquals("ArrayBinaryTreeTest.testRemoveLeftLeaf", true, tree.left(tree.root()) == null);
    }

    private static void testRemoveRightLeaf() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addRight(root, 2);
        tree.remove(second);
        assertEquals("ArrayBinaryTreeTest.testRemoveRightLeaf", true, tree.right(tree.root()) == null);
    }

    private static void testRemoveMid() {
        Tree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(second, 3);
        Node<Integer> fourth = tree.add(third, 4);
        tree.remove(third);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[3];
        Integer[] expectedArray = {1, 2, 4};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("ArrayBinaryTreeTest.testRemoveMid", expectedArray, testArray);
    }

    private static void testAnotherRemove() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> fifth = tree.addRight(second, 5);
        Node<Integer> sixth = tree.add(fifth, 6);
        Node<Integer> seventh = tree.add(fifth, 7);
        Node<Integer> third = tree.add(root, 3);
        tree.remove(second);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {1, 5, 3, 6, 7};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("ArrayBinaryTreeTest.testAnotherRemove", expectedArray, testArray);
    }

}
