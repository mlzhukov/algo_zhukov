package com.gjj.training.algo1704.zhukovm.lesson07.binary;

import com.gjj.training.algo1704.zhukovm.lesson07.Node;

import java.util.Collection;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {

    protected Node<E> root;
    protected int size;
    // nonpublic utility

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root == null) {
            root = new NodeImpl<>(e);
            size++;
            return root;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        int childrenNum = childrenNumber(node);
        if (childrenNum == 0) {
            return addLeft(node, e);
        } else if (childrenNum == 1) {
            try {
                return addLeft(node, e);
            } catch (Exception ex) {
                return addRight(node, e);
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.left == null) {
            NodeImpl<E> newNode = new NodeImpl<>(e);
            node.left = newNode;
            size++;
            return newNode;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.right == null) {
            NodeImpl<E> newNode = new NodeImpl<E>(e);
            node.right = newNode;
            size++;
            return newNode;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E oldValue = node.value;
        node.value = e;
        return oldValue;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        int childrenNum = childrenNumber(node);
        E oldValue = node.value;
        NodeImpl<E> parent = (NodeImpl<E>) parent(n);
        if (childrenNum > 1) {
            throw new IllegalArgumentException();
        } else if (childrenNum == 0) {
            if (parent.left == node) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } else if (childrenNum == 1) {
            if (parent == null) {
                if (node.left != null) {
                    root = node.left;
                    node = null;
                } else {
                    root = node.right;
                    node = null;
                }
            } else if (parent.left == node) {
                if (node.left != null) {
                    parent.left = node.left;
                } else {
                    parent.left = node.right;
                }
            } else {
                if (node.left != null) {
                    parent.right = node.left;
                } else {
                    parent.right = node.right;
                }
            }
        }
        node = null;
        size--;
        return oldValue;
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        return validate(p).left;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        return validate(p).right;
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        return findParent(root, n);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Collection<Node<E>> nodes() {
        return breadthFirst();
    }

    private Node<E> findParent(Node<E> cursorNode, Node<E> child) {
        if (child == root || cursorNode == null) {
            return null;
        } else {
            NodeImpl<E> node = validate(cursorNode);
            if (node.left == child || node.right == child) {
                return cursorNode;
            } else {
                Node<E> found = findParent(node.left, child);
                if (found == null) {
                    found = findParent(node.right, child);
                }
                return found;
            }
        }
    }

    protected static class NodeImpl<E> implements Node<E> {

        public E value;
        public Node<E> left;
        public Node<E> right;

        public NodeImpl(E value) {
            this(value, null, null);
        }

        public NodeImpl(E value, Node<E> left, Node<E> right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        @Override
        public E getElement() {
            return value;
        }

        @Override
        public boolean equals(Object object) {
            NodeImpl obj = (NodeImpl) object;
            return this.value.equals(obj.value) && this.left == obj.left && this.right == obj.right;
        }

    }

}