package com.gjj.training.algo1704.zhukovm.lesson07.binary;

import com.gjj.training.algo1704.zhukovm.lesson07.AbstractTree;
import com.gjj.training.algo1704.zhukovm.lesson07.Node;
import com.gjj.training.algo1704.zhukovm.lesson07.Tree;

import java.util.Collection;
import java.util.Iterator;

import static com.gjj.training.algo1704.zhukovm.lesson07.binary.LinkedBinaryTree.NodeImpl;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;

public class LinkedBinaryTreeTest {

    public static void main(String[] args) {
        testAddRoot();
        testAddRootException();
        testAddToRoot();
        testAddException();
        testAddToLeaf();
        testSize();
        testAddRightAndLeft();
        testAddRightException();
        testAddLeftException();
        testSet();
        testParent();
        testRemoveException();
        testRemoveLeftLeaf();
        testRemoveRightLeaf();
        testRemoveMid();
        testPreOrder();
        testPostOrder();
        testBreadthFirst();
        testInOrder();
    }

    private static void testAddRoot() {
        Tree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> expectedRoot = new NodeImpl<>(1);
        boolean testEqual = expectedRoot.equals(tree.addRoot(1)) && expectedRoot.equals(tree.root());
        assertEquals("LinkedBinaryTreeTest.testAddRoot", true, testEqual);
    }

    private static void testAddRootException() {
        Tree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        try {
            tree.addRoot(2);
            fail("AssertionError");
        } catch (IllegalStateException e) {
            assertEquals("LinkedBinaryTreeTest.testAddRootException", true, e.getMessage() == null);
        }
    }

    private static void testAddToRoot() {
        Tree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[3];
        Integer[] expectedArray = {1, 2, 3};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("LinkedBinaryTreeTest.testAddToRoot", expectedArray, testArray);
    }

    private static void testAddException() {
        Tree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        try {
            tree.add(root, 5);
            fail("AssertionError");
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testAddException", true, e.getMessage() == null);
        }
    }

    private static void testSize() {
        Tree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        assertEquals("LinkedBinaryTreeTest.testSize", 2, tree.size());
    }

    private static void testAddToLeaf() {
        Tree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {1, 2, 3, 4, 5};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("LinkedBinaryTreeTest.testAddToLeaf", expectedArray, testArray);
    }

    private static void testAddRightAndLeft() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addRight(root, 2);
        Node<Integer> third = tree.addLeft(root, 3);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[3];
        Integer[] expectedArray = {1, 3, 2};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("LinkedBinaryTreeTest.testAddRightAndLeft", expectedArray, testArray);
    }

    private static void testAddRightException() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addRight(root, 2);
        try {
            tree.addRight(root, 3);
            fail("AssertionError");
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testAddRightException", true, e.getMessage() == null);
        }
    }

    private static void testAddLeftException() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addLeft(root, 2);
        try {
            tree.addLeft(root, 3);
            fail("AssertionError");
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testAddLeftException", true, e.getMessage() == null);
        }
    }

    private static void testSet() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addLeft(root, 2);
        tree.set(second, 10);
        assertEquals("LinkedBinaryTreeTest.testSet", (Integer) 10, second.getElement());
    }

    private static void testParent() {
        Tree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(second, 3);
        Node<Integer> fourth = tree.add(third, 4);
        assertEquals("LinkedBinaryTreeTest.testParent", second, tree.parent(third));
    }

    private static void testPreOrder() {
        AbstractTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Collection<Node<Integer>> preOrder = tree.preOrder();
        Iterator<Node<Integer>> it = preOrder.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {1, 2, 4, 5, 3};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next().getElement();
        }
        assertEquals("LinkedBinaryTreeTest.testPreOrder", expectedArray, testArray);
    }

    private static void testPostOrder() {
        AbstractTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Collection<Node<Integer>> postOrder = tree.postOrder();
        Iterator<Node<Integer>> it = postOrder.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {4, 5, 2, 3, 1};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next().getElement();
        }
        assertEquals("LinkedBinaryTreeTest.testPostOrder", expectedArray, testArray);
    }

    private static void testBreadthFirst() {
        AbstractTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Collection<Node<Integer>> breadthFirst = tree.breadthFirst();
        Iterator<Node<Integer>> it = breadthFirst.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {1, 2, 3, 4, 5};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next().getElement();
        }
        assertEquals("LinkedBinaryTreeTest.testBreadthFirst", expectedArray, testArray);
    }

    private static void testInOrder() {
        AbstractBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(root, 3);
        tree.add(second, 4);
        tree.add(second, 5);
        Collection<Node<Integer>> inOrder = tree.inOrder();
        Iterator<Node<Integer>> it = inOrder.iterator();
        Integer[] testArray = new Integer[5];
        Integer[] expectedArray = {4, 2, 5, 1, 3};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next().getElement();
        }
        assertEquals("LinkedBinaryTreeTest.testInOrder", expectedArray, testArray);
    }

    private static void testRemoveException() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addLeft(root, 2);
        Node<Integer> third = tree.add(second, 3);
        Node<Integer> fourth = tree.add(second, 4);
        try {
            tree.remove(second);
            fail("AssertionError");
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testRemoveException", true, e.getMessage() == null);
        }
    }

    private static void testRemoveLeftLeaf() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addLeft(root, 2);
        tree.remove(second);
        assertEquals("LinkedBinaryTreeTest.testRemoveLeftLeaf", true, tree.left(tree.root()) == null);
    }

    private static void testRemoveRightLeaf() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.addRight(root, 2);
        tree.remove(second);
        assertEquals("LinkedBinaryTreeTest.testRemoveRightLeaf", true, tree.right(tree.root()) == null);
    }

    private static void testRemoveMid() {
        Tree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> second = tree.add(root, 2);
        Node<Integer> third = tree.add(second, 3);
        Node<Integer> fourth = tree.add(third, 4);
        tree.remove(third);
        Iterator<Integer> it = tree.iterator();
        Integer[] testArray = new Integer[3];
        Integer[] expectedArray = {1, 2, 4};
        for (int i = 0; it.hasNext(); i++) {
            testArray[i] = it.next();
        }
        assertEquals("LinkedBinaryTreeTest.testRemoveMid", expectedArray, testArray);
    }

}
