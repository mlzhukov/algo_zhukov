package com.gjj.training.algo1704.zhukovm.lesson08;

import com.gjj.training.algo1704.zhukovm.lesson07.Node;
import com.gjj.training.algo1704.zhukovm.lesson07.binary.LinkedBinaryTree;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
        comparator = new Comparator<E>() {
            @Override
            public int compare(E o1, E o2) {
                if (o1 instanceof Comparable && o2 instanceof Comparable) {
                    return ((Comparable<E>) o1).compareTo(o2);
                } else {
                    throw new IllegalArgumentException();
                }
            }
        };
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        return comparator.compare(val2, val1);
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        if (n == null) {
            return null;
        } else {
            NodeImpl<E> node = validate(n);
            int comp = compare(node.getElement(), val);
            if (comp == 0) {
                return node;
            } else if (comp < 0) {
                return treeSearch(node.left, val);
            } else {
                return treeSearch(node.right, val);
            }
        }
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        int childrenNum = childrenNumber(node);
        int comp = compare(n.getElement(), e);
        if (childrenNum == 2 || comp == 0) {
            throw new IllegalArgumentException();
        } else if (comp < 0) {
            return addLeft(node, e);
        } else {
            return addRight(node, e);
        }
    }

    public Node<E> add(E e) throws IllegalArgumentException {
        if (root == null) {
            return addRoot(e);
        }
        Node<E> cursorNode = root;
        Node<E> parent;
        while (true) {
            parent = cursorNode;
            int comp = compare(cursorNode.getElement(), e);
            if (comp < 0) {
                cursorNode = validate(cursorNode).left;
                if (cursorNode == null) {
                    return addLeft(parent, e);
                }
            } else {
                cursorNode = validate(cursorNode).right;
                if (cursorNode == null) {
                    return addRight(parent, e);
                }
            }
        }
    }

    public Node<E> remove(E e) {
        NodeImpl<E> node = (NodeImpl<E>) treeSearch(root, e);
        Node<E> toReturn;
        if (childrenNumber(node) == 0) {
            super.remove(node);
            toReturn = null;
        } else if (childrenNumber(node) == 1) {
            if (node.left != null) {
                toReturn = node.left;
            } else {
                toReturn = node.right;
            }
            super.remove(node);
        } else {
            return toReturn = null;
        }
        return toReturn;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("(");
        toStringImpl(root, sb);
        sb.append(")");
        return sb.toString();
    }


    private void toStringImpl(Node<E> cursorNode, StringBuilder sb) {
        if (cursorNode == null) {
            return;
        } else {
            sb.append(cursorNode.getElement());
            int childrenNum = childrenNumber(cursorNode);
            if (childrenNum != 0) {
                sb.append("(");
            }
            Collection<Node<E>> children = children(cursorNode);
            Iterator<Node<E>> iterator = children.iterator();
            for (int i = childrenNum; iterator.hasNext(); i--) {
                if (i != childrenNum) {
                    sb.append(", ");
                }
                toStringImpl(iterator.next(), sb);
            }
            if (childrenNum != 0) {
                sb.append(")");
            }
        }
    }

    protected void afterElementRemoved(Node<E> n) {

    }

    protected void afterElementAdded(Node<E> n) {

    }

}
