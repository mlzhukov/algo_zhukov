package com.gjj.training.algo1704.zhukovm.lesson08;

import com.gjj.training.algo1704.zhukovm.lesson07.Node;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class BinarySearchTreeTest {

    public static void main(String[] args) {
        testToString();
        testAdd();
        testTreeSearch();
    }

    private static void testAdd() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> root = tree.add(10);
        Node<Integer> second = tree.add(7);
        tree.add(5);
        boolean isEqual = tree.left(root).getElement() == 7 && tree.left(second).getElement() == 5;
        assertEquals("BinarySearchTreeTest.testAdd", true, isEqual);
    }

    private static void testToString() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> root = tree.add(10);
        tree.add(7);
        tree.add(13);
        tree.add(8);
        tree.add(5);
        tree.add(14);
        tree.add(12);
        assertEquals("BinarySearchTreeTest.testToString", "(10(7(5, 8), 13(12, 14)))", tree.toString());
    }

    private static void testTreeSearch() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> root = tree.add(10);
        tree.add(7);
        Node<Integer> thirteen = tree.add(13);
        tree.add(8);
        tree.add(5);
        tree.add(14);
        tree.add(12);
        Node<Integer> testNode = tree.treeSearch(root, 13);
        assertEquals("BinarySearchTreeTest.testTreeSearch", thirteen, testNode);
    }

}
