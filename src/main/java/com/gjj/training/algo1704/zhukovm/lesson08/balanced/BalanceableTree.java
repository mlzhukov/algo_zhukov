package com.gjj.training.algo1704.zhukovm.lesson08.balanced;

import com.gjj.training.algo1704.zhukovm.lesson07.Node;
import com.gjj.training.algo1704.zhukovm.lesson08.BinarySearchTree;
import com.gjj.training.algo1704.zhukovm.lesson07.binary.LinkedBinaryTree;

public abstract class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.init.severynv.algo.tree.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(Node<E> parent, Node<E> child, boolean makeLeftChild) {
        // todo
        if (child == root) {
            root = parent;
        }
        if (makeLeftChild) {
            validate(parent).left = child;
            validate(child).right = null;
        } else {
            validate(parent).right = child;
            validate(child).left = null;
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        LinkedBinaryTree.NodeImpl<E> node = validate(n);
        if (node == root) {
            if (node.left != null) {
                rotate(node.left);
            } else {
                rotate(node.right);
            }
            return;
        }
        LinkedBinaryTree.NodeImpl<E> parent = validate(parent(n));
        LinkedBinaryTree.NodeImpl<E> grandParent = null;
        if (parent != root) {
            grandParent = validate(parent(parent));
        }
        if (parent.left == node) {
            Node<E> child = node.right;
            relink(node, parent, false);
            parent.left = child;
        } else {
            Node<E> child = node.left;
            relink(node, parent, true);
            parent.right = child;
        }
        if (grandParent != null) {
            if (grandParent.right == parent) {
                grandParent.right = node;
            } else {
                grandParent.left = node;
            }
        }
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.init.severynv.algo.tree.Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        LinkedBinaryTree.NodeImpl<E> node = validate(n);
        LinkedBinaryTree.NodeImpl<E> parent = (LinkedBinaryTree.NodeImpl<E>) parent(n);
        LinkedBinaryTree.NodeImpl<E> grandParent = (LinkedBinaryTree.NodeImpl<E>) parent(parent);
        if (grandParent.left == parent && parent.left == node
                || grandParent.right == parent && parent.right == node) {
            rotate(parent);
            return root;
        } else {
            rotate(node);
            rotate(node);
            return root;
        }
    }

}
