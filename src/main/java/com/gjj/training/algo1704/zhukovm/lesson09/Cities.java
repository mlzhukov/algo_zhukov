package com.gjj.training.algo1704.zhukovm.lesson09;

import java.util.NavigableSet;

public class Cities {

    public static NavigableSet<String> findCities(NavigableSet<String> cities, String string) {
        String toKey = string + Character.MAX_VALUE;
        while (cities.higher(toKey) != null && cities.higher(toKey).contains(string)) {
            toKey += Character.MAX_VALUE;
        }
        return cities.subSet(string, true, toKey, true);
    }

}
