package com.gjj.training.algo1704.zhukovm.lesson09;

import java.util.TreeSet;

import static com.gjj.training.algo1704.zhukovm.lesson09.Cities.findCities;
import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class CitiesTest {

    public static void main(String[] args) {
        testFindCities();
    }

    private static void testFindCities() {
        TreeSet<String> cities = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        cities.add("Moscow");
        cities.add("mogilev");
        cities.add("PARIS");
        cities.add("ab" + Character.MAX_VALUE);
        cities.add("ab" + Character.MAX_VALUE + "cd");
        cities.add("ab" + Character.MAX_VALUE + Character.MAX_VALUE + "zz");
        cities.add("aB");
        cities.add("ABBA");
        String[] expectedArray = {"ABBA", "aB",
                "ab" + Character.MAX_VALUE,
                "ab" + Character.MAX_VALUE + "cd",
                "ab" + Character.MAX_VALUE + Character.MAX_VALUE + "zz"};
        assertEquals("CitiesTest.testFindCities", expectedArray, findCities(cities, "ab").toArray());
    }

}
