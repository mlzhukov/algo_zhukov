package com.gjj.training.algo1704.zhukovm.lesson09;

import com.gjj.training.algo1704.zhukovm.util.Assert;

import java.util.*;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.AbstractMap.SimpleEntry;
import static java.util.Arrays.asList;

public class NavigableMapTest {

    public static void main(String[] args) {
        testLowerEntry();
        testLowerEntryNPE();
        testLowerEntryNull();
        testLowerKey();
        testLowerKeyNull();
        testLowerKeyNPE();
        testFloorEntry();
        testFloorEntryNull();
        testFloorEntryNPE();
        testFloorKey();
        testFloorKeyNull();
        testFloorKeyNPE();
        testCeilingEntry();
        testCeilingEntryNull();
        testCeilingEntryNPE();
        testCeilingKey();
        testCeilingKeyNull();
        testCeilingKeyNPE();
        testHigherEntry();
        testHigherEntryNull();
        testHigherEntryNPE();
        testHigherKey();
        testHigherKeyNull();
        testHigherKeyNPE();
        testFirstEntry();
        testFirstEntryNull();
        testLastEntry();
        testLastEntryNull();
        testPollFirstEntry();
        testPollFirstEntryNull();
        testPollLastEntry();
        testPollLastEntryNull();
        testDescendingMap();
        testNavigableKeySet();
        testDescendingKeySet();
    }

    private static void testLowerEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        SimpleEntry<Integer, String> testEntry = new SimpleEntry<>(4, "second");
        Assert.assertEquals("NavigableMapTest.testLowerEntry", testEntry, map.lowerEntry(6));
    }

    private static void testLowerEntryNPE() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        try {
            map.lowerEntry(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableMapTest.testLowerEntryNPE", true, e.getMessage() == null);
        }
    }

    private static void testLowerEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testLowerEntryNull", true, map.lowerEntry(0) == null);
    }

    private static void testLowerKey() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        Assert.assertEquals("NavigableMapTest.testLowerKey", (Integer) 1, map.lowerKey(4));
    }

    private static void testLowerKeyNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testLowerEntryNull", true, map.lowerKey(0) == null);
    }

    private static void testLowerKeyNPE() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        try {
            map.lowerKey(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableMapTest.testLowerKeyNPE", true, e.getMessage() == null);
        }
    }

    private static void testFloorEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        SimpleEntry<Integer, String> testEntry = new SimpleEntry<>(4, "second");
        Assert.assertEquals("NavigableMapTest.testFloorEntry", testEntry, map.floorEntry(4));
    }

    private static void testFloorEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testFloorEntryNull", true, map.floorEntry(0) == null);

    }

    private static void testFloorEntryNPE() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        try {
            map.floorEntry(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableMapTest.testFloorEntryNPE", true, e.getMessage() == null);
        }
    }

    private static void testFloorKey() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        Assert.assertEquals("NavigableMapTest.testFloorKey", (Integer) 4, map.floorKey(4));
    }

    private static void testFloorKeyNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testFloorKeyNull", true, map.floorKey(0) == null);
    }

    private static void testFloorKeyNPE() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        try {
            map.floorKey(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableMapTest.testFloorKeyNPE", true, e.getMessage() == null);
        }
    }

    private static void testCeilingEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        SimpleEntry<Integer, String> testEntry = new SimpleEntry<>(4, "second");
        Assert.assertEquals("NavigableMapTest.testCeilingEntry", testEntry, map.ceilingEntry(4));
    }

    private static void testCeilingEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testCeilingEntryNull", true, map.ceilingEntry(4) == null);
    }

    private static void testCeilingEntryNPE() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        try {
            map.ceilingEntry(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableMapTest.testCeilingEntryNPE", true, e.getMessage() == null);
        }
    }

    private static void testCeilingKey() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        Assert.assertEquals("NavigableMapTest.testCeilingEntry", (Integer) 4, map.ceilingKey(4));
    }

    private static void testCeilingKeyNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testCeilingKeyNull", true, map.ceilingKey(2) == null);

    }

    private static void testCeilingKeyNPE() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        try {
            map.ceilingKey(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableMapTest.testCeilingKeyNPE", true, e.getMessage() == null);
        }
    }

    private static void testHigherEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        SimpleEntry<Integer, String> testEntry = new SimpleEntry<>(6, "third");
        Assert.assertEquals("NavigableMapTest.testHigherEntry", testEntry, map.higherEntry(4));
    }

    private static void testHigherEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testHigherEntryNull", true, map.higherEntry(1) == null);
    }

    private static void testHigherEntryNPE() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        try {
            map.higherEntry(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableMapTest.testHigherEntryNPE", true, e.getMessage() == null);
        }
    }

    private static void testHigherKey() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        Assert.assertEquals("NavigableMapTest.testHigherKey", (Integer) 6, map.higherKey(4));
    }

    private static void testHigherKeyNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testHigherEntryNull", true, map.higherEntry(1) == null);
    }

    private static void testHigherKeyNPE() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        try {
            map.higherKey(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableMapTest.testHigherKeyNPE", true, e.getMessage() == null);
        }
    }

    private static void testFirstEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        SimpleEntry<Integer, String> testEntry = new SimpleEntry<>(1, "first");
        Assert.assertEquals("NavigableMapTest.testFirstEntry", testEntry, map.firstEntry());
    }

    private static void testFirstEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        Assert.assertEquals("NavigableMapTest.testFirstEntryNull", true, map.firstEntry() == null);
    }

    private static void testLastEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        SimpleEntry<Integer, String> testEntry = new SimpleEntry<>(6, "third");
        Assert.assertEquals("NavigableMapTest.testLastEntry", testEntry, map.lastEntry());
    }

    private static void testLastEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        Assert.assertEquals("NavigableMapTest.testLastEntryNull", true, map.lastEntry() == null);
    }

    private static void testPollFirstEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        SimpleEntry<Integer, String> testEntry = new SimpleEntry<>(1, "first");
        NavigableMap<Integer, String> testMap = new TreeMap<>();
        testMap.put(4, "second");
        testMap.put(6, "third");
        boolean isEqual = testEntry.equals(map.pollFirstEntry()) && map.equals(testMap);
        Assert.assertEquals("NavigableMapTest.testFirstEntry", true, isEqual);
    }

    private static void testPollFirstEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        Assert.assertEquals("NavigableMapTest.testPollFirstEntryNull", true, map.pollFirstEntry() == null);
    }

    private static void testPollLastEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        SimpleEntry<Integer, String> testEntry = new SimpleEntry<>(6, "third");
        NavigableMap<Integer, String> testMap = new TreeMap<>();
        testMap.put(1, "first");
        testMap.put(4, "second");
        boolean isEqual = testEntry.equals(map.pollLastEntry()) && map.equals(testMap);
        Assert.assertEquals("NavigableMapTest.testFirstEntry", true, isEqual);
    }

    private static void testPollLastEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        Assert.assertEquals("NavigableMapTest.testPollLastEntryNull", true, map.pollLastEntry() == null);
    }

    private static void testDescendingMap() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        Map<Integer, String> testMap = new TreeMap<>();
        testMap.put(6, "third");
        testMap.put(4, "second");
        testMap.put(1, "first");
        Assert.assertEquals("NavigableMapTest.testDescendingMap", testMap, map.descendingMap());
    }

    private static void testNavigableKeySet() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        Set<Integer> testSet = new TreeSet<>(asList(1, 4, 6));
        Assert.assertEquals("NavigableMapTest.testNavigableKeySet", testSet, map.navigableKeySet());
    }

    private static void testDescendingKeySet() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(4, "second");
        map.put(6, "third");
        Set<Integer> testSet = new TreeSet<>(asList(6, 4, 1));
        Assert.assertEquals("NavigableMapTest.testDescendingKeySet", testSet, map.navigableKeySet());
    }

}
