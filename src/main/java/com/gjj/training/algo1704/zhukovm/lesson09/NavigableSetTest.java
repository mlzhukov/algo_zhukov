package com.gjj.training.algo1704.zhukovm.lesson09;

import com.gjj.training.algo1704.zhukovm.util.Assert;

import java.util.NavigableSet;
import java.util.TreeSet;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.Arrays.asList;

public class NavigableSetTest {

    public static void main(String[] args) {
        testLower();
        testLowerNull();
        testLowerNPE();
        testFloor();
        testFloorNull();
        testFloorNPE();
        testCeiling();
        testCeilingNull();
        testCeilingNPE();
        testHigher();
        testHigherNull();
        testHigherNPE();
        testPollFirst();
        testPollFirstNull();
        testPollLast();
        testPollLastNull();
        testDescendingSet();
    }

    private static void testLower() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 9, 10));
        Assert.assertEquals("NavigableSetTest.testLower", (Integer) 9, set.lower(10));
    }

    private static void testLowerNull() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 9, 10));
        Assert.assertEquals("NavigableSetTest.testLowerNull", true, set.lower(7) == null);
    }

    private static void testLowerNPE() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 9, 10));
        try {
            set.lower(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableSetTest.testLowerNPE", true, e.getMessage() == null);
        }
    }

    private static void testFloor() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 9, 10));
        Assert.assertEquals("NavigableSetTest.testFloor", (Integer) 10, set.floor(10));
    }

    private static void testFloorNull() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 9, 10));
        Assert.assertEquals("NavigableSetTest.testFloorNull", true, set.floor(7) == null);
    }

    private static void testFloorNPE() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 9, 10));
        try {
            set.floor(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableSetTest.testFloorNPE", true, e.getMessage() == null);
        }
    }

    private static void testCeiling() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        Assert.assertEquals("NavigableSetTest.testCeiling", (Integer) 10, set.ceiling(9));
    }

    private static void testCeilingNull() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        Assert.assertEquals("NavigableSetTest.testCeilingNull", true, set.ceiling(13) == null);
    }

    private static void testCeilingNPE() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        try {
            set.ceiling(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableSetTest.testCeilingNPE", true, e.getMessage() == null);
        }
    }

    private static void testHigher() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        Assert.assertEquals("NavigableSetTest.testHigher", (Integer) 12, set.higher(10));
    }

    private static void testHigherNull() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        Assert.assertEquals("NavigableSetTest.testHigherNull", true, set.higher(12) == null);
    }

    private static void testHigherNPE() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        try {
            set.higher(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("NavigableSetTest.testHigherNPE", true, e.getMessage() == null);
        }
    }

    private static void testPollFirst() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        NavigableSet<Integer> testSet = new TreeSet<>(asList(10, 12));
        boolean isEqual = set.pollFirst() == 8 && set.equals(testSet);
        Assert.assertEquals("NavigableSetTest.testPollFirst", true, isEqual);
    }

    private static void testPollFirstNull() {
        NavigableSet<Integer> set = new TreeSet<>();
        Assert.assertEquals("NavigableSetTest.testPollFirstNull", true, set.pollFirst() == null);
    }

    private static void testPollLast() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        NavigableSet<Integer> testSet = new TreeSet<>(asList(8, 10));
        boolean isEqual = set.pollLast() == 12 && set.equals(testSet);
        Assert.assertEquals("NavigableSetTest.testPollLast", true, isEqual);
    }

    private static void testPollLastNull() {
        NavigableSet<Integer> set = new TreeSet<>();
        Assert.assertEquals("NavigableSetTest.testPollLastNull", true, set.pollLast() == null);
    }

    private static void testDescendingSet() {
        NavigableSet<Integer> set = new TreeSet<>(asList(8, 10, 12));
        NavigableSet<Integer> testSet = new TreeSet<>(asList(12, 10, 8));
        Assert.assertEquals("NavigableSetTest.testDescendingSet", testSet, set.descendingSet());
    }

}
