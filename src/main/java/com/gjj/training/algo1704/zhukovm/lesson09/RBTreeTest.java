package com.gjj.training.algo1704.zhukovm.lesson09;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class RBTreeTest {

    public static void main(String[] args) {
        testAddCase1();
        testAddCase2();
        testAddCase3();
        testAddCase5();
        testAddCase4();
        testRemoveRoot();
        testRemoveRotate1();
        testRemoveRotate2();
        testRemoveRotate3();
        testRemoveRotate4();
        testRemoveRotate5();
        testRemoveRotate6();
        testRemoveCaseReplacement();
    }

    private static void testAddCase1() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(7);
        boolean isEqual = !root.color && rbtree.root().equals(root);
        assertEquals("RBTreeTest.testAddCase1", true, isEqual);
    }

    private static void testAddCase2() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(7);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(5);
        boolean isEqual = !root.color && first.color && rbtree.parent(first).equals(root);
        assertEquals("RBTreeTest.testAddCase2", true, isEqual);
    }

    private static void testAddCase3() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(7);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(5);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(8);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(3);
        String expectedTree = "(7(5(3), 8))";
        boolean isEqual = !root.color && !first.color && !second.color && third.color && rbtree.toString().equals(expectedTree);
        assertEquals("RBTreeTest.testAddCase3", true, isEqual);
    }

    private static void testAddCase5() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(7);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(5);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(8);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(3);
        RedBlackTree.NodeRB<Integer> fourth = (RedBlackTree.NodeRB<Integer>) rbtree.add(2);
        String expectedTree = "(7(3(2, 5), 8))";
        boolean isEqual = !root.color && first.color && !second.color && !third.color && fourth.color && rbtree.toString().equals(expectedTree);
        assertEquals("RBTreeTest.testAddCase5", true, isEqual);
    }

    private static void testAddCase4() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(7);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(5);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(8);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(3);
        RedBlackTree.NodeRB<Integer> fourth = (RedBlackTree.NodeRB<Integer>) rbtree.add(4);
        RedBlackTree.NodeRB<Integer> kek = (RedBlackTree.NodeRB) rbtree.root();
        String expectedTree = "(7(4(3, 5), 8))";
        boolean isEqual = !root.color && first.color && !second.color && third.color && !fourth.color && rbtree.toString().equals(expectedTree);
        assertEquals("RBTreeTest.testAddCase4", true, isEqual);
    }

    private static void testRemoveRoot() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(7);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(5);
        rbtree.remove(7);
        boolean isEqual = !first.color && first.equals(rbtree.root());
        assertEquals("RBTreeTest.testRemoveRoot", true, isEqual);
    }

    private static void testRemoveRotate1() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(40);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(30);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(50);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(25);
        RedBlackTree.NodeRB<Integer> fourth = (RedBlackTree.NodeRB<Integer>) rbtree.add(45);
        RedBlackTree.NodeRB<Integer> fifth = (RedBlackTree.NodeRB<Integer>) rbtree.add(55);
        RedBlackTree.NodeRB<Integer> six = (RedBlackTree.NodeRB<Integer>) rbtree.add(60);
        rbtree.remove(25);
        rbtree.remove(30);
        String expectedTree = "(50(40(45), 55(60)))";
        boolean isEqual = !root.color && !second.color && fourth.color && !fifth.color && six.color
                && expectedTree.equals(rbtree.toString());
        assertEquals("RBTreeTest.testRemoveRotate1", true, isEqual);
    }

    private static void testRemoveRotate2() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(30);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(20);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(40);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(35);
        rbtree.remove(20);
        String expectedTree = "(35(30, 40))";
        boolean isEqual = third.equals(rbtree.root()) && !root.color && !second.color && !third.color && expectedTree.equals(rbtree.toString());
        assertEquals("RBTreeTest.testRemoveRotate2", true, isEqual);
    }

    private static void testRemoveRotate3() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(30);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(20);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(40);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(50);
        rbtree.remove(20);
        String expectedTree = "(40(30, 50))";
        boolean isEqual = second.equals(rbtree.root()) && !root.color && !second.color && !third.color && expectedTree.equals(rbtree.toString());
        assertEquals("RBTreeTest.testRemoveRotate3", true, isEqual);
    }

    private static void testRemoveRotate4() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(40);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(30);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(50);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(25);
        RedBlackTree.NodeRB<Integer> fourth = (RedBlackTree.NodeRB<Integer>) rbtree.add(35);
        RedBlackTree.NodeRB<Integer> fifth = (RedBlackTree.NodeRB<Integer>) rbtree.add(55);
        RedBlackTree.NodeRB<Integer> six = (RedBlackTree.NodeRB<Integer>) rbtree.add(20);
        rbtree.remove(55);
        rbtree.remove(50);
        String expectedTree = "(30(25(20), 40(35)))";
        boolean isEqual = !root.color && !first.color && !third.color && fourth.color && six.color
                && expectedTree.equals(rbtree.toString());
        assertEquals("RBTreeTest.testRemoveRotate4", true, isEqual);
    }

    private static void testRemoveRotate5() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(30);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(20);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(40);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(25);
        rbtree.remove(40);
        String expectedTree = "(25(20, 30))";
        boolean isEqual = third.equals(rbtree.root()) && !root.color && !second.color && !third.color
                && expectedTree.equals(rbtree.toString());
        assertEquals("RBTreeTest.testRemoveRotate5", true, isEqual);
    }

    private static void testRemoveRotate6() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(30);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(20);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(40);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(10);
        rbtree.remove(40);
        String expectedTree = "(20(10, 30))";
        boolean isEqual = first.equals(rbtree.root()) && !root.color && !second.color && !third.color
                && expectedTree.equals(rbtree.toString());
        assertEquals("RBTreeTest.testRemoveRotate6", true, isEqual);
    }

    private static void testRemoveCaseReplacement() {
        RedBlackTree<Integer> rbtree = new RedBlackTree<>();
        RedBlackTree.NodeRB<Integer> root = (RedBlackTree.NodeRB<Integer>) rbtree.add(40);
        RedBlackTree.NodeRB<Integer> first = (RedBlackTree.NodeRB<Integer>) rbtree.add(20);
        RedBlackTree.NodeRB<Integer> second = (RedBlackTree.NodeRB<Integer>) rbtree.add(50);
        RedBlackTree.NodeRB<Integer> third = (RedBlackTree.NodeRB<Integer>) rbtree.add(35);
        rbtree.remove(20);
        String expectedTree = "(40(35, 50))";
        boolean isEqual = !root.color && !second.color && !third.color && expectedTree.equals(rbtree.toString());
        assertEquals("RBTreeTest.testRemoveCaseReplacement", true, isEqual);
    }
}
