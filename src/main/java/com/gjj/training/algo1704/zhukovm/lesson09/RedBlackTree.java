package com.gjj.training.algo1704.zhukovm.lesson09;

import com.gjj.training.algo1704.zhukovm.lesson07.Node;
import com.gjj.training.algo1704.zhukovm.lesson07.binary.LinkedBinaryTree;
import com.gjj.training.algo1704.zhukovm.lesson08.balanced.BalanceableTree;


/**
 * @author Vital Severyn
 * @since 05.08.15
 */
public class RedBlackTree<E> extends BalanceableTree<E> {

    @Override
    protected NodeRB<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        } else if (n instanceof NodeRB) {
            return (NodeRB<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private boolean isBlack(Node<E> n) {
        if (n == null) {
            return true;
        }
        NodeRB<E> node = validate(n);
        return !node.color;
    }

    private boolean isRed(Node<E> n) {
        if (n == null) {
            return false;
        }
        NodeRB<E> node = validate(n);
        return node.color;
    }

    private void makeBlack(Node<E> n) {
        if (n == null) {
            return;
        }
        NodeRB<E> node = validate(n);
        node.color = false;
    }

    private void makeRed(Node<E> n) {
        NodeRB<E> node = validate(n);
        node.color = true;
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root == null) {
            root = new NodeRB<>(e);
            size++;
            return root;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeRB<E> node = validate(n);
        if (node.left == null) {
            NodeImpl<E> newNode = new NodeRB<>(e);
            node.left = newNode;
            size++;
            return newNode;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeRB<E> node = validate(n);
        if (node.right == null) {
            NodeRB<E> newNode = new NodeRB<>(e);
            node.right = newNode;
            size++;
            return newNode;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> add(E e) {
        Node<E> n = super.add(e);
        NodeRB<E> node = validate(n);
        afterElementAdded(node);
        return n;
    }

    @Override
    public Node<E> remove(E e) {
        NodeRB<E> n = validate(treeSearch(root, e));
        NodeRB<E> left = validate(left(n));
        NodeRB<E> right = validate(right(n));
        NodeRB<E> parent = validate(parent(n));
        NodeRB<E> replacement = (left != null ? left : right);
        if (replacement != null) {
            NodeRB<E> parentReplacement = validate(parent(replacement));
            parentReplacement = parent;
            if (parent == null) {
                root = replacement;
            } else if (n == parent.left) {
                parent.left = replacement;
            } else {
                parent.right = replacement;
            }
            if (isBlack(n)) {
                afterElementRemoved(replacement);
            }
        } else if (parent == null) {
            root = null;
        } else {
            if (isBlack(n))
                afterElementRemoved(n);
            if (parent(n) != null) {
                if (n == parent.left) {
                    parent.left = null;
                } else {
                    parent.right = null;
                }
            }
        }
        return null;
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        insertCase1(n);
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        NodeRB<E> parent = validate(parent(n));
        while (n != root && isBlack(n)) {
            if (n == parent.left) {
                NodeRB<E> sibling = validate(parent.right);
                if (isRed(sibling)) {
                    makeBlack(sibling);
                    makeRed(parent);
                    rotate(parent.right); //rotate1
                    sibling = validate(parent.right);
                }
                if (isBlack(sibling.left) && isBlack(sibling.right)) {
                    makeRed(sibling);
                    n = parent;
                } else {
                    if (isBlack(sibling.right)) {
                        makeBlack(sibling.left);
                        makeRed(sibling);
                        rotate(sibling.left); //rotate2
                        sibling = validate(parent.right);
                    }
                    sibling.color = parent.color;
                    makeBlack(parent);
                    makeBlack(sibling.right);
                    rotate(parent.right); //rotate3
                    n = root;
                }
            } else { //another side
                NodeRB<E> sib = validate(parent.left);
                if (isRed(sib)) {
                    makeBlack(sib);
                    makeRed(parent);
                    rotate(parent.left); //rotate4
                    sib = validate(parent.left);
                }
                if (isBlack(sib.right) && isBlack(sib.left)) {
                    makeRed(sib);
                    n = parent;
                } else {
                    if (isBlack(sib.left)) {
                        makeBlack(sib.right);
                        makeRed(sib);
                        rotate(sib.right); //rotate5
                        sib = validate(parent.left);
                    }
                    sib.color = parent.color;
                    makeBlack(parent);
                    makeBlack(sib.left);
                    rotate(parent.left); //rotate6
                    n = root;
                }
            }
        }
        makeBlack(n);
    }

    private void insertCase1(Node<E> n) {
        Node<E> parent = parent(n);
        if (parent == null) {
            makeBlack(n);
        } else {
            insertCase2(n);
        }
    }

    private void insertCase2(Node<E> n) {
        Node<E> parent = parent(n);
        if (isBlack(parent)) {
            return;
        } else {
            insertCase3(n);
        }
    }

    private void insertCase3(Node<E> n) {
        Node<E> parent = parent(n);
        Node<E> grandParent = parent(parent);
        Node<E> uncle = sibling(parent);
        if (uncle != null && isRed(uncle)) {
            makeBlack(parent);
            makeBlack(uncle);
            makeRed(grandParent);
            insertCase1(grandParent);
        } else {
            insertCase4(n);
        }
    }

    private void insertCase4(Node<E> n) {
        NodeRB<E> parent = validate(parent(n));
        NodeRB<E> grandParent = validate(parent(parent));
        NodeRB<E> node = validate(n);
        if (n == parent.right && parent == grandParent.left) {
            rotate(n);
            node = validate(node.left);
        } else if (n == parent.left && parent == grandParent.right) {
            rotate(n);
            node = validate(node.right);
        }
        insertCase5(node);
    }

    private void insertCase5(Node<E> n) {
        NodeRB<E> parent = (NodeRB<E>) parent(n);
        NodeRB<E> grandParent = (NodeRB<E>) parent(parent);
        makeBlack(parent);
        makeRed(grandParent);
        rotate(parent);
    }

    static class NodeRB<E> extends LinkedBinaryTree.NodeImpl<E> implements Node<E> {
        protected boolean color; //true is for red, false is for black

        public NodeRB(E value) {
            super(value);
            color = true;
        }

        public NodeRB(E value, NodeRB<E> left, NodeRB<E> right) {
            super(value, left, right);
            color = true;
        }

        public NodeRB(NodeImpl<E> n, boolean color) {
            super(n.getElement(), n.left, n.right);
            this.color = color;
        }

        public NodeRB(E value, NodeRB<E> left, NodeRB<E> right, boolean color) {
            super(value, left, right);
            this.color = color;
        }
    }

}
