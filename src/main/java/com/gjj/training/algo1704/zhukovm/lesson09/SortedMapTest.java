package com.gjj.training.algo1704.zhukovm.lesson09;

import com.gjj.training.algo1704.zhukovm.util.Assert;

import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.Arrays.asList;

public class SortedMapTest {

    public static void main(String[] args) {
        testComparator();
        testSubMap();
        testSubMapNPE();
        testSubMapIllegalArg();
        testHeadMap();
        testHeadMapNPE();
        testTailMap();
        testTailMapNPE();
        testFirstKey();
        testFirstKeyNoSuchElem();
        testLastKey();
        testLastKeyNoSuchElem();
        testKeySet();
        testValues();
    }

    private static void testComparator() {
        SortedMap<Integer, String> map = new TreeMap<>();
        Assert.assertEquals("SortedMapTest.testComparator", true, map.comparator() == null);
    }

    private static void testSubMap() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(6, "third");
        map.put(4, "second");
        SortedMap<Integer, String> testMap = new TreeMap<>();
        testMap.put(6, "third");
        testMap.put(4, "second");
        Assert.assertEquals("SortedMapTest.testSubMap", testMap, map.subMap(4, 8));
    }

    private static void testSubMapNPE() {
        SortedMap<Integer, String> map = new TreeMap<>();
        try {
            map.subMap(null, null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedMapTest.testSubMapNPE", true, e.getMessage() == null);
        }
    }

    private static void testSubMapIllegalArg() {
        SortedMap<Integer, String> map = new TreeMap<>();
        try {
            map.subMap(10, 8);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedMapTest.testSubMapIllegalArg", "fromKey > toKey", e.getMessage());
        }
    }

    private static void testHeadMap() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(6, "third");
        map.put(4, "second");
        SortedMap<Integer, String> testMap = new TreeMap<>();
        testMap.put(1, "first");
        testMap.put(4, "second");
        Assert.assertEquals("SortedMapTest.testHeadMap", testMap, map.headMap(6));
    }

    private static void testHeadMapNPE() {
        SortedMap<Integer, String> map = new TreeMap<>();
        try {
            map.headMap(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedMapTest.testHeadMapNPE", true, e.getMessage() == null);
        }
    }

    private static void testTailMap() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(6, "third");
        map.put(4, "second");
        SortedMap<Integer, String> testMap = new TreeMap<>();
        testMap.put(6, "third");
        testMap.put(4, "second");
        Assert.assertEquals("SortedMapTest.testTailMap", testMap, map.tailMap(4));
    }

    private static void testTailMapNPE() {
        SortedMap<Integer, String> map = new TreeMap<>();
        try {
            map.tailMap(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedMapTest.testTailMapNPE", true, e.getMessage() == null);
        }
    }

    private static void testFirstKey() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(6, "third");
        map.put(4, "second");
        Assert.assertEquals("SortedMapTest.testFirstKey", (Integer) 1, map.firstKey());
    }

    private static void testFirstKeyNoSuchElem() {
        SortedMap<Integer, String> map = new TreeMap<>();
        try {
            map.firstKey();
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedMapTest.testFirstKeyNoSuchElem", true, e.getMessage() == null);
        }
    }

    private static void testLastKey() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(6, "third");
        map.put(4, "second");
        Assert.assertEquals("SortedMapTest.testLastKey", (Integer) 6, map.lastKey());
    }

    private static void testLastKeyNoSuchElem() {
        SortedMap<Integer, String> map = new TreeMap<>();
        try {
            map.lastKey();
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedMapTest.testLastKeyNoSuchElem", true, e.getMessage() == null);
        }
    }

    private static void testKeySet() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(6, "third");
        map.put(4, "second");
        Set<Integer> testSet = new TreeSet<>(asList(1, 4, 6));
        Assert.assertEquals("SortedMapTest.testKeySet", testSet, map.keySet());
    }

    private static void testValues() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "first");
        map.put(6, "third");
        map.put(4, "second");
        String[] testArray = {"first", "second", "third"};
        Assert.assertEquals("SortedMapTest.testValues", testArray, map.values().toArray());
    }

}
