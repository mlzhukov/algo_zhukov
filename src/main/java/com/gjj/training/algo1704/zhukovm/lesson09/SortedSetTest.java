package com.gjj.training.algo1704.zhukovm.lesson09;

import com.gjj.training.algo1704.zhukovm.util.Assert;

import java.util.SortedSet;
import java.util.TreeSet;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static java.util.Arrays.asList;

public class SortedSetTest {

    public static void main(String[] args) {
        testComparator();
        testSubSet();
        testSubSetNPE();
        testSubSetIllegalArg();
        testHeadSet();
        testHeadSetNPE();
        testTailSet();
        testTailSetNPE();
        testFirst();
        testFirstNoSuchElem();
        testLast();
        testLastNoSuchElem();
    }

    private static void testComparator() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        Assert.assertEquals("SortedSetTest.testComparator", true, set.comparator() == null);
    }

    private static void testSubSet() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        SortedSet<Integer> testSet = new TreeSet<>(asList(8, 9));
        Assert.assertEquals("SortedSetTest.testSubSet", testSet, set.subSet(8, 10));
    }

    private static void testSubSetNPE() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        try {
            set.subSet(null, 10);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedSetTest.testSubSetNPE", true, e.getMessage() == null);
        }
    }

    private static void testSubSetIllegalArg() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        try {
            set.subSet(12, 8);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedSetTest.testSubSetIllegalArg", "fromKey > toKey", e.getMessage());
        }
    }

    private static void testHeadSet() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        SortedSet<Integer> testSet = new TreeSet<>(asList(8, 9));
        Assert.assertEquals("SortedSetTest.testHeadSet", testSet, set.headSet(10));
    }

    private static void testHeadSetNPE() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        try {
            set.headSet(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedSetTest.testHeadSetNPE", true, e.getMessage() == null);
        }
    }

    private static void testTailSet() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        SortedSet<Integer> testSet = new TreeSet<>(asList(9, 10));
        Assert.assertEquals("SortedSetTest.testTailSet", testSet, set.tailSet(9));
    }

    private static void testTailSetNPE() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        try {
            set.tailSet(null);
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedSetTest.testTailSetNPE", true, e.getMessage() == null);
        }
    }

    private static void testFirst() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        Assert.assertEquals("SortedSetTest.testFirst", (Integer) 8, set.first());
    }

    private static void testFirstNoSuchElem() {
        SortedSet<Integer> set = new TreeSet<>();
        try {
            set.first();
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedSetTest.testFirstNoSuchElem", true, e.getMessage() == null);
        }
    }

    private static void testLast() {
        SortedSet<Integer> set = new TreeSet<>(asList(10, 8, 9));
        Assert.assertEquals("SortedSetTest.testLast", (Integer) 10, set.last());
    }

    private static void testLastNoSuchElem() {
        SortedSet<Integer> set = new TreeSet<>();
        try {
            set.last();
            Assert.fail("AssertionError");
        } catch (Exception | Error e) {
            Assert.assertEquals("SortedSetTest.testLastNoSuchElem", true, e.getMessage() == null);
        }
    }

}
