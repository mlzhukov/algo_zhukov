package com.gjj.training.algo1704.zhukovm.lesson10;

import java.util.ArrayList;
import java.util.Arrays;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;
import static java.util.Arrays.*;

/**
 * unfortunately recognized that i've done A LOT of redundant work too late
 */
public class ArraysTest {

    public static void main(String[] args) {
        testSortInt();
        testSortIntIndex();
        testSortIntIndexIllegalArg();
        testSortIntIndexOutOfBounds();
        testSortLong();
        testSortLongIndex();
        testSortLongIndexIllegalArg();
        testSortLongIndexOutOfBounds();
        testSortShort();
        testSortShortIndex();
        testSortShortIndexIllegalArg();
        testSortShortIndexOutOfBounds();
        testSortChar();
        testSortCharIndex();
        testSortCharIndexIllegalArg();
        testSortCharIndexOutOfBounds();
        testSortByte();
        testSortByteIndex();
        testSortByteIndexIllegalArg();
        testSortByteIndexOutOfBounds();
        testSortFloat();
        testSortFloatIndex();
        testSortFloatIndexIllegalArg();
        testSortFloatIndexOutOfBounds();
        testSortDouble();
        testSortDoubleIndex();
        testSortDoubleIndexIllegalArg();
        testSortDoubleIndexOutOfBounds();
        testParallelSortByte();
        testParallelSortByteIndex();
        testParallelSortByteIndexIllegalArg();
        testParallelSortByteIndexOutOfBounds();
        testParallelSortChar();
        testParallelSortCharIndex();
        testParallelSortCharIndexIllegalArg();
        testParallelSortCharIndexOutOfBounds();
        testParallelSortShort();
        testParallelSortShortIndex();
        testParallelSortShortIndexIllegalArg();
        testParallelSortShortIndexOutOfBounds();
        testParallelSortInt();
        testParallelSortIntIndex();
        testParallelSortIntIndexIllegalArg();
        testParallelSortIntIndexOutOfBounds();
        testParallelSortLong();
        testParallelSortLongIndex();
        testParallelSortLongIndexIllegalArg();
        testParallelSortLongIndexOutOfBounds();
        testParallelSortFloat();
        testParallelSortFloatIndex();
        testParallelSortFloatIndexIllegalArg();
        testParallelSortFloatIndexOutOfBounds();
        testParallelSortDouble();
        testParallelSortDoubleIndex();
        testParallelSortDoubleIndexIllegalArg();
        testParallelSortDoubleIndexOutOfBounds();
        testBSearchLong();
        testBSearchLongIndex();
        testBSearchLongIndexIllegalArg();
        testBSearchLongIndexOutOfBounds();
        testBSearchInt();
        testBSearchIntIndex();
        testBSearchIntIndexOutOfBounds();
        testBSearchIntIndexIllegalArg();
        testBSearchShort();
        testBSearchShortIndex();
        testBSearchShortIllegalArg();
        testBSearchShortIndexOutOfBounds();
        testBSearchChar();
        testBSearchCharIndex();
        testBSearchCharIllegalArg();
        testBSearchCharIndexOutOfBounds();
        testBSearchByte();
        testBSearchByteIndex();
        testBSearchByteIllegalArg();
        testBSearchByteIndexOutOfBounds();
        testBSearchDouble();
        testBSearchDoubleIndex();
        testBSearchDoubleIllegalArg();
        testBSearchDoubleOutOfBounds();
        testBSearchFloat();
        testBSearchFloatIndex();
        testBSearchFloatIndexIllegalArg();
        testBSearchFloatIndexOutOfBounds();
        testBSearchObj();
        testBSearchObjIndex();
        testBSearchObjIndexIllegalArg();
        testBSearchObjIndexOutOfBounds();
        testEqualsLong();
        testNotEqualsLong();
        testEqualsInt();
        testNotEqualsInt();
        testEqualsShort();
        testNotEqualsShort();
        testEqualsChar();
        testNotEqualsChar();
        testEqualsByte();
        testNotEqualsByte();
        testEqualsBoolean();
        testNotEqualsBoolean();
        testEqualsDouble();
        testNotEqualsDouble();
        testEqualsFloat();
        testNotEqualsFloat();
        testEqualsObj();
        testNotEqualsObj();
        testFillLong();
        testFillLongIndex();
        testFillLongIndexIllegalArg();
        testFillLongIndexOutOfBounds();
        testCopyOfInt();
        testCopyOfIntNegSize();
        testCopyOfIntNPE();
        testCopyOfRange();
        testCopyOfRangeIllegalArg();
        testCopyOfRangeNPE();
        testCopyOfRangeOutOfBounds();
        testAsList();
        testHashCode();
        testToString();
    }

    private static void testSortInt() {
        int[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array);
        int[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testSortInt", expectedArray, array);
    }

    private static void testSortIntIndex() {
        int[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array, 0, 4);
        int[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testSortIntIndex", expectedArray, array);
    }

    private static void testSortIntIndexIllegalArg() {
        int[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortIntIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testSortIntIndexOutOfBounds() {
        int[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortIntIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testSortLong() {
        long[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array);
        long[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testSortLong", expectedArray, array);
    }

    private static void testSortLongIndex() {
        long[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array, 0, 4);
        long[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testSortLongIndex", expectedArray, array);
    }


    private static void testSortLongIndexIllegalArg() {
        long[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortLongIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testSortLongIndexOutOfBounds() {
        long[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortLongIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testSortShort() {
        short[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array);
        short[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testSortShort", expectedArray, array);
    }

    private static void testSortShortIndex() {
        short[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array, 0, 4);
        short[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testSortShortIndex", expectedArray, array);
    }

    private static void testSortShortIndexIllegalArg() {
        short[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortShortIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testSortShortIndexOutOfBounds() {
        short[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortShortIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testSortChar() {
        char[] array = {'5', '6', '1', '4', '3', '2', '7', '8', '9'};
        sort(array);
        char[] expectedArray = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
        assertEquals("ArraysTest.testSortChar", expectedArray, array);
    }

    private static void testSortCharIndex() {
        char[] array = {'5', '6', '1', '4', '3', '2', '7', '8', '9'};
        sort(array, 0, 4);
        char[] expectedArray = {'1', '4', '5', '6', '3', '2', '7', '8', '9'};
        assertEquals("ArraysTest.testSortCharIndex", expectedArray, array);
    }


    private static void testSortCharIndexIllegalArg() {
        char[] array = {'5', '6', '1', '4', '3', '2', '7', '8', '9'};
        try {
            sort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortCharIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testSortCharIndexOutOfBounds() {
        char[] array = {'5', '6', '1', '4', '3', '2', '7', '8', '9'};
        try {
            sort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortCharIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testSortByte() {
        byte[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array);
        byte[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testSortByte", expectedArray, array);
    }

    private static void testSortByteIndex() {
        byte[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array, 0, 4);
        byte[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testSortByteIndex", expectedArray, array);
    }

    private static void testSortByteIndexIllegalArg() {
        byte[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortByteIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testSortByteIndexOutOfBounds() {
        byte[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortByteIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testSortFloat() {
        float[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array);
        float[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testSortFloat", expectedArray, array);
    }

    private static void testSortFloatIndex() {
        float[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array, 0, 4);
        float[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testSortFloatIndex", expectedArray, array);
    }


    private static void testSortFloatIndexIllegalArg() {
        float[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortFloatIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testSortFloatIndexOutOfBounds() {
        float[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortFloatIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testSortDouble() {
        double[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array);
        double[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testSortDouble", expectedArray, array);
    }

    private static void testSortDoubleIndex() {
        double[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        sort(array, 0, 4);
        double[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testSortDoubleIndex", expectedArray, array);
    }

    private static void testSortDoubleIndexIllegalArg() {
        double[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortDoubleIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testSortDoubleIndexOutOfBounds() {
        double[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            sort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortDoubleIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testParallelSortByte() {
        byte[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array);
        byte[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testParallelSortByte", expectedArray, array);
    }

    private static void testParallelSortByteIndex() {
        byte[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array, 0, 4);
        byte[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testParallelSortByteIndex", expectedArray, array);
    }


    private static void testParallelSortByteIndexIllegalArg() {
        byte[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortByteIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testParallelSortByteIndexOutOfBounds() {
        byte[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortByteIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testParallelSortChar() {
        char[] array = {'5', '6', '1', '4', '3', '2', '7', '8', '9'};
        parallelSort(array);
        char[] expectedArray = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
        assertEquals("ArraysTest.testParallelSortChar", expectedArray, array);
    }

    private static void testParallelSortCharIndex() {
        char[] array = {'5', '6', '1', '4', '3', '2', '7', '8', '9'};
        parallelSort(array, 0, 4);
        char[] expectedArray = {'1', '4', '5', '6', '3', '2', '7', '8', '9'};
        assertEquals("ArraysTest.testParallelSortCharIndex", expectedArray, array);
    }

    private static void testParallelSortCharIndexIllegalArg() {
        char[] array = {'5', '6', '1', '4', '3', '2', '7', '8', '9'};
        try {
            parallelSort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortCharIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testParallelSortCharIndexOutOfBounds() {
        char[] array = {'5', '6', '1', '4', '3', '2', '7', '8', '9'};
        try {
            parallelSort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortCharIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testParallelSortShort() {
        short[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array);
        short[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testParallelSortShort", expectedArray, array);
    }

    private static void testParallelSortShortIndex() {
        short[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array, 0, 4);
        short[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testParallelSortShortIndex", expectedArray, array);
    }


    private static void testParallelSortShortIndexIllegalArg() {
        short[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortShortIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testParallelSortShortIndexOutOfBounds() {
        short[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortParallelShortIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testParallelSortInt() {
        int[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array);
        int[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testParallelSortInt", expectedArray, array);
    }

    private static void testParallelSortIntIndex() {
        int[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array, 0, 4);
        int[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testParallelSortIntIndex", expectedArray, array);
    }

    private static void testParallelSortIntIndexIllegalArg() {
        int[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortIntIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testParallelSortIntIndexOutOfBounds() {
        int[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testSortIntIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testParallelSortLong() {
        long[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array);
        long[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testParallelSortLong", expectedArray, array);
    }

    private static void testParallelSortLongIndex() {
        long[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array, 0, 4);
        long[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testParallelSortLongIndex", expectedArray, array);
    }


    private static void testParallelSortLongIndexIllegalArg() {
        long[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortLongIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testParallelSortLongIndexOutOfBounds() {
        long[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortLongIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testParallelSortFloat() {
        float[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array);
        float[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testParallelSortFloat", expectedArray, array);
    }

    private static void testParallelSortFloatIndex() {
        float[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array, 0, 4);
        float[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testParallelSortFloatIndex", expectedArray, array);
    }

    private static void testParallelSortFloatIndexIllegalArg() {
        float[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortFloatIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testParallelSortFloatIndexOutOfBounds() {
        float[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortFloatIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testParallelSortDouble() {
        double[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array);
        double[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("ArraysTest.testParallelSortDouble", expectedArray, array);
    }

    private static void testParallelSortDoubleIndex() {
        double[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        parallelSort(array, 0, 4);
        double[] expectedArray = {1, 4, 5, 6, 3, 2, 7, 8, 10, 9};
        assertEquals("ArraysTest.testParallelSortDoubleIndex", expectedArray, array);
    }


    private static void testParallelSortDoubleIndexIllegalArg() {
        double[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, 4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortDoubleIndexIllegalArg", "fromIndex(4) > toIndex(0)", e.getMessage());
        }
    }

    private static void testParallelSortDoubleIndexOutOfBounds() {
        double[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        try {
            parallelSort(array, -4, 0);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testParallelSortDoubleIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testBSearchLong() {
        long[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 7);
        assertEquals("ArraysTest.testBSearchLong", 6, index);
    }

    private static void testBSearchLongIndex() {
        long[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 5, 10, 7);
        assertEquals("ArraysTest.testBSearchLongIndex", 6, index);
    }

    private static void testBSearchLongIndexIllegalArg() {
        long[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, 10, 5, 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchLongIndexIllegalArg", "fromIndex(10) > toIndex(5)", e.getMessage());
        }
    }

    private static void testBSearchLongIndexOutOfBounds() {
        long[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, -4, 5, 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchLongIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testBSearchInt() {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 7);
        assertEquals("ArraysTest.testBSearchInt", 6, index);
    }

    private static void testBSearchIntIndex() {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 5, 10, 7);
        assertEquals("ArraysTest.testBSearchIntIndex", 6, index);
    }

    private static void testBSearchIntIndexIllegalArg() {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, 10, 5, 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchIntIndexIllegalArg", "fromIndex(10) > toIndex(5)", e.getMessage());
        }
    }

    private static void testBSearchIntIndexOutOfBounds() {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, -4, 5, 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchIntIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testBSearchShort() {
        short[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, (short) 7);
        assertEquals("ArraysTest.testBSearchShort", 6, index);
    }

    private static void testBSearchShortIndex() {
        short[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 5, 10, (short) 7);
        assertEquals("ArraysTest.testBSearchShortIndex", 6, index);
    }

    private static void testBSearchShortIllegalArg() {
        short[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, 10, 5, (short) 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchShortIndexIllegalArg", "fromIndex(10) > toIndex(5)", e.getMessage());
        }
    }

    private static void testBSearchShortIndexOutOfBounds() {
        short[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, -4, 5, (short) 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchIntIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testBSearchChar() {
        char[] array = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
        int index = binarySearch(array, '7');
        assertEquals("ArraysTest.testBSearchChar", 6, index);
    }

    private static void testBSearchCharIndex() {
        char[] array = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
        int index = binarySearch(array, 5, 9, '7');
        assertEquals("ArraysTest.testBSearchCharIndex", 6, index);
    }

    private static void testBSearchCharIllegalArg() {
        char[] array = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
        try {
            binarySearch(array, 9, 5, '7');
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchCharIndexIllegalArg", "fromIndex(9) > toIndex(5)", e.getMessage());
        }
    }

    private static void testBSearchCharIndexOutOfBounds() {
        char[] array = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
        try {
            binarySearch(array, -4, 5, '7');
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchCharIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testBSearchByte() {
        byte[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, (byte) 7);
        assertEquals("ArraysTest.testBSearchByte", 6, index);
    }

    private static void testBSearchByteIndex() {
        byte[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 5, 9, (byte) 7);
        assertEquals("ArraysTest.testBSearchByteIndex", 6, index);
    }

    private static void testBSearchByteIllegalArg() {
        byte[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, 9, 5, (byte) 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchByteIndexIllegalArg", "fromIndex(9) > toIndex(5)", e.getMessage());
        }
    }

    private static void testBSearchByteIndexOutOfBounds() {
        byte[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, -4, 5, (byte) 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchByteIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testBSearchDouble() {
        double[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, (double) 7);
        assertEquals("ArraysTest.testBSearchDouble", 6, index);
    }

    private static void testBSearchDoubleIndex() {
        double[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 5, 9, (double) 7);
        assertEquals("ArraysTest.testBSearchDoubleIndex", 6, index);
    }

    private static void testBSearchDoubleIllegalArg() {
        double[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, 9, 5, (double) 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchDoubleIllegalArg", "fromIndex(9) > toIndex(5)", e.getMessage());
        }
    }

    private static void testBSearchDoubleOutOfBounds() {
        double[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, -4, 5, (double) 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchDoubleOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testBSearchFloat() {
        float[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, (float) 7);
        assertEquals("ArraysTest.testBSearchFloat", 6, index);
    }

    private static void testBSearchFloatIndex() {
        float[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 5, 9, (float) 7);
        assertEquals("ArraysTest.testBSearchFloatIndex", 6, index);
    }

    private static void testBSearchFloatIndexIllegalArg() {
        float[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, 9, 5, (float) 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchFloatIndexIllegalArg", "fromIndex(9) > toIndex(5)", e.getMessage());
        }
    }

    private static void testBSearchFloatIndexOutOfBounds() {
        float[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, -4, 5, (float) 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchFloatIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testBSearchObj() {
        Integer[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 7);
        assertEquals("ArraysTest.testBSearchObj", 6, index);
    }

    private static void testBSearchObjIndex() {
        Integer[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int index = binarySearch(array, 5, 9, 7);
        assertEquals("ArraysTest.testBSearchObjIndex", 6, index);
    }

    private static void testBSearchObjIndexIllegalArg() {
        Integer[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, 9, 5, 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchObjIndexIllegalArg", "fromIndex(9) > toIndex(5)", e.getMessage());
        }
    }

    private static void testBSearchObjIndexOutOfBounds() {
        Integer[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            binarySearch(array, -4, 5, 7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testBSearchObjIndexOutOfBounds", "Array index out of range: -4", e.getMessage());
        }
    }

    private static void testEqualsLong() {
        long[] array1 = {1, 2, 3};
        long[] array2 = {1, 2, 3};
        assertEquals("ArraysTest.testEqualsLong", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsLong() {
        long[] array1 = {1, 2, 3};
        long[] array2 = {1, 2};
        assertEquals("ArraysTest.testNotEqualsLong", false, Arrays.equals(array1, array2));
    }

    private static void testEqualsInt() {
        int[] array1 = {1, 2, 3};
        int[] array2 = {1, 2, 3};
        assertEquals("ArraysTest.testEqualsLong", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsInt() {
        int[] array1 = {1, 2, 3};
        int[] array2 = {1, 2};
        assertEquals("ArraysTest.testNotEqualsInt", false, Arrays.equals(array1, array2));
    }

    private static void testEqualsShort() {
        short[] array1 = {1, 2, 3};
        short[] array2 = {1, 2, 3};
        assertEquals("ArraysTest.testEqualsShort", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsShort() {
        short[] array1 = {1, 2, 3};
        short[] array2 = {1, 2};
        assertEquals("ArraysTest.testNotEqualsShort", false, Arrays.equals(array1, array2));
    }

    private static void testEqualsChar() {
        char[] array1 = {'1', '2', '3'};
        char[] array2 = {'1', '2', '3'};
        assertEquals("ArraysTest.testEqualsChar", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsChar() {
        char[] array1 = {'1', '2', '3'};
        char[] array2 = {'1', '2'};
        assertEquals("ArraysTest.testNotEqualsChar", false, Arrays.equals(array1, array2));
    }

    private static void testEqualsByte() {
        byte[] array1 = {1, 2, 3};
        byte[] array2 = {1, 2, 3};
        assertEquals("ArraysTest.testEqualsByte", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsByte() {
        byte[] array1 = {1, 2, 3};
        byte[] array2 = {1, 2};
        assertEquals("ArraysTest.testNotEqualsByte", false, Arrays.equals(array1, array2));
    }

    private static void testEqualsBoolean() {
        boolean[] array1 = {true, false};
        boolean[] array2 = {true, false};
        assertEquals("ArraysTest.testEqualsBoolean", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsBoolean() {
        boolean[] array1 = {true, false};
        boolean[] array2 = {true};
        assertEquals("ArraysTest.testNotEqualsBoolean", false, Arrays.equals(array1, array2));
    }

    private static void testEqualsDouble() {
        double[] array1 = {1, 2, 3};
        double[] array2 = {1, 2, 3};
        assertEquals("ArraysTest.testEqualsDouble", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsDouble() {
        double[] array1 = {1, 2, 3};
        double[] array2 = {1, 2};
        assertEquals("ArraysTest.testNotEqualsDouble", false, Arrays.equals(array1, array2));
    }

    private static void testEqualsFloat() {
        float[] array1 = {1, 2, 3};
        float[] array2 = {1, 2, 3};
        assertEquals("ArraysTest.testEqualsFloat", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsFloat() {
        float[] array1 = {1, 2, 3};
        float[] array2 = {1, 2};
        assertEquals("ArraysTest.testNotEqualsFloat", false, Arrays.equals(array1, array2));
    }

    private static void testEqualsObj() {
        Integer[] array1 = {1, 2, 3};
        Integer[] array2 = {1, 2, 3};
        assertEquals("ArraysTest.testEqualsFloat", true, Arrays.equals(array1, array2));
    }

    private static void testNotEqualsObj() {
        Integer[] array1 = {1, 2, 3};
        Integer[] array2 = {1, 2};
        assertEquals("ArraysTest.testNotEqualsObj", false, Arrays.equals(array1, array2));
    }

    private static void testFillLong() {
        long[] array = new long[5];
        fill(array, 1);
        long[] expectedArray = {1, 1, 1, 1, 1};
        assertEquals("ArraysTest.testFillLong", expectedArray, array);
    }

    private static void testFillLongIndex() {
        long[] array = new long[5];
        fill(array, 2, 5, 1);
        long[] expectedArray = {0, 0, 1, 1, 1};
        assertEquals("ArraysTest.testFillLongIndex", expectedArray, array);
    }

    private static void testFillLongIndexOutOfBounds() {
        long[] array = new long[5];
        try {
            fill(array, -2, 5, 1);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testFillLongIndexOutOfBounds", "Array index out of range: -2", e.getMessage());
        }
    }

    private static void testFillLongIndexIllegalArg() {
        long[] array = new long[5];
        try {
            fill(array, 5, 2, 1);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testFillLongIndexIllegalArg", "fromIndex(5) > toIndex(2)", e.getMessage());
        }
    }

    private static void testCopyOfInt() {
        int[] array = {1, 2, 3, 4, 5};
        int[] expectedArray = {1, 2, 3, 4, 5, 0, 0};
        assertEquals("ArraysTest.testCopyOfInt", expectedArray, copyOf(array, 7));
    }

    private static void testCopyOfIntNPE() {
        int[] array = null;
        try {
            copyOf(array, 4);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testCopyOfIntNPE", true, e.getMessage() == null);
        }
    }

    private static void testCopyOfIntNegSize() {
        int[] array = new int[5];
        try {
            copyOf(array, -7);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testCopyOfIntNegSize", true, e.getMessage() == null);
        }
    }

    private static void testCopyOfRange() {
        int[] array = {1, 2, 3, 4, 5};
        int[] expectedArray = {2, 3};
        assertEquals("ArraysTest.testCopyOfRange", expectedArray, copyOfRange(array, 1, 3));
    }

    private static void testCopyOfRangeOutOfBounds() {
        int[] array = {1, 2, 3, 4, 5};
        try {
            copyOfRange(array, -2, 2);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testCopyOfRangeOutOfBounds", true, e.getMessage() == null);
        }
    }

    private static void testCopyOfRangeIllegalArg() {
        int[] array = {1, 2, 3, 4, 5};
        try {
            copyOfRange(array, 5, 2);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testCopyOfRangeIllegalArg", "5 > 2", e.getMessage());
        }
    }

    private static void testCopyOfRangeNPE() {
        int[] array = null;
        try {
            copyOfRange(array, 2, 4);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("ArraysTest.testCopyOfIntNPE", true, e.getMessage() == null);
        }
    }

    private static void testAsList() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        assertEquals("ArraysTest.testAsList", list, asList(1, 2));
    }

    private static void testHashCode() {
        int[] array = null;
        assertEquals("ArraysTest.testHashCode", 0, Arrays.hashCode(array));
    }

    private static void testToString() {
        int[] array = {1};
        assertEquals("ArraysTest.testToString", "[1]", Arrays.toString(array));
    }

}
