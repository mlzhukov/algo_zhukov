package com.gjj.training.algo1704.zhukovm.lesson10;

import java.util.*;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;
import static com.gjj.training.algo1704.zhukovm.util.Assert.fail;
import static java.util.Arrays.asList;
import static java.util.Collections.*;

public class CollectionsTest {

    public static void main(String[] args) {
        testAddAll();
        testBSearch();
        testAsLifoQueue();
        testCheckedСollection();
        testCopy();
        testCopyOutOfBounds();
        testDisjointTrue();
        testDisjointFalse();
        testDisjointNPE();
        testEmptyList();
        testFill();
        testFrequency();
        testFrequencyNPE();
        testIndexOfSublist();
        testLastIndexOfSubList();
        testMax();
        testMaxNoSuchElem();
        testMin();
        testMinNoSuchElem();
        testNCopies();
        testNCopiesIllegalArg();
        testReplaceAll();
        testReverse();
        testRotate();
        testSort();
        testSwap();
        testUnmodifiableCollection();
    }

    private static void testAddAll() {
        Collection<Integer> col = new ArrayList<>();
        Collection<Integer> expectedCol = new ArrayList<>(asList(1, 2));
        col.addAll(asList(1, 2));
        assertEquals("CollectionsTest.testAddAll", expectedCol, col);
    }

    private static void testBSearch() {
        List<Integer> list = new ArrayList<>(asList(6, 7, 8));
        assertEquals("CollectionsTest.testBSearch", 1, binarySearch(list, 7));
    }

    private static void testAsLifoQueue() {
        Deque<Integer> deque = new ArrayDeque<>(asList(1, 2, 3));
        Queue<Integer> lifo = asLifoQueue(deque);
        lifo.poll();
        Integer[] expectedArray = {2, 3};
        assertEquals("CollectionsTest.testAsLifoQueue", expectedArray, lifo.toArray());
    }

    private static void testCheckedСollection() {
        Collection raw = new ArrayList(asList('1'));
        Collection checked = checkedCollection(raw, Character.class);
        try {
            checked.add(1);
            fail("AssertionError");
        } catch (Exception | Error e) {
            String expected = "Attempt to insert class java.lang.Integer element into collection with element type class java.lang.Character";
            assertEquals("CollectionsTest.testCheckedСollection", expected, e.getMessage());
        }
    }

    private static void testCopy() {
        List<Integer> list1 = new ArrayList<>(asList(1, 2, 3));
        List<Integer> list2 = new ArrayList<>(asList(6, 7, 8));
        copy(list2, list1);
        assertEquals("CollectionsTest.testCopy", list1, list2);
    }

    private static void testCopyOutOfBounds() {
        List<Integer> list1 = new ArrayList<>(asList(1, 2, 3));
        List<Integer> list2 = new ArrayList<>(asList(6, 7));
        try {
            copy(list2, list1);
            fail("AssertionError");
        } catch (Exception | Error e) {
            String expected = "Source does not fit in dest";
            assertEquals("CollectionsTest.testCopyOutOfBounds", expected, e.getMessage());
        }
    }

    private static void testDisjointFalse() {
        Collection<Integer> col = new ArrayList<>(asList(1, 2, 3));
        Collection<Integer> col2 = new ArrayList<>(asList(2, 3));
        assertEquals("CollectionsTest.testDisjointFalse", false, disjoint(col, col2));
    }

    private static void testDisjointTrue() {
        Collection<Integer> col = new ArrayList<>(asList(1, 2, 3));
        Collection<Integer> col2 = new ArrayList<>(asList(5, 7));
        assertEquals("CollectionsTest.testDisjointTrue", true, disjoint(col, col2));
    }

    private static void testDisjointNPE() {
        Collection<Integer> col = null;
        Collection<Integer> col2 = new ArrayList<>(asList(5, 7));
        try {
            disjoint(col, col2);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("CollectionsTest.testDisjointNPE", true, e.getMessage() == null);
        }
    }

    private static void testEmptyList() {
        List list = emptyList();
        Object[] expectedArray = new Object[0];
        assertEquals("CollectionsTest.testEmptyList", expectedArray, list.toArray());
    }

    private static void testFill() {
        List<Integer> list = new ArrayList<>(asList(1, 2, 3));
        fill(list, 0);
        Integer[] expectedArray = {0, 0, 0};
        assertEquals("CollectionsTest.testFill", expectedArray, list.toArray());
    }

    private static void testFrequency() {
        List<Integer> list = new ArrayList<>(asList(1, 2, 3));
        assertEquals("CollectionsTest.testFrequency", 1, frequency(list, 2));
    }

    private static void testFrequencyNPE() {
        List<Integer> list = null;
        try {
            frequency(list, 4);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("CollectionsTest.testFrequencyNPE", true, e.getMessage() == null);
        }
    }

    private static void testIndexOfSublist() {
        List<Integer> list = new ArrayList<>(asList(7, 2, 3, 1, 2, 3, 1, 2, 3));
        List<Integer> subList = new ArrayList<>(asList(1, 2, 3));
        assertEquals("CollectionsTest.testIndexOfSublist", 3, indexOfSubList(list, subList));
    }

    private static void testLastIndexOfSubList() {
        List<Integer> list = new ArrayList<>(asList(7, 2, 3, 1, 2, 3, 1, 2, 3));
        List<Integer> subList = new ArrayList<>(asList(1, 2, 3));
        assertEquals("CollectionsTest.testLastIndexOfSubList", 6, lastIndexOfSubList(list, subList));
    }

    private static void testMax() {
        Collection<Integer> col = new ArrayList<>(asList(1, 2, 3));
        assertEquals("CollectionsTest.testMax", (Integer) 3, max(col));
    }

    private static void testMaxNoSuchElem() {
        Collection<Integer> col = new ArrayList<>();
        try {
            max(col);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("CollectionsTest.testMaxNoSuchElem", true, e.getMessage() == null);
        }
    }

    private static void testMin() {
        Collection<Integer> col = new ArrayList<>(asList(1, 2, 3));
        assertEquals("CollectionsTest.testMin", (Integer) 1, min(col));
    }

    private static void testMinNoSuchElem() {
        Collection<Integer> col = new ArrayList<>();
        try {
            min(col);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("CollectionsTest.testMinNoSuchElem", true, e.getMessage() == null);
        }
    }

    private static void testNCopies() {
        Integer[] expectedArray = {1, 1, 1};
        assertEquals("CollectionsTest.testNCopies", expectedArray, nCopies(3, 1).toArray());
    }

    private static void testNCopiesIllegalArg() {
        try {
            nCopies(-1, 1);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("CollectionsTest.testNCopiesIllegalArg", "List length = -1", e.getMessage());
        }
    }

    private static void testReplaceAll() {
        List<Integer> list = new ArrayList<>(asList(1, 1, 2, 3));
        List<Integer> expectedList = new ArrayList<>(asList(2, 2, 2, 3));
        replaceAll(list, 1, 2);
        assertEquals("CollectionsTest.testReplaceAll", expectedList, list);
    }

    private static void testReverse() {
        List<Integer> list = new ArrayList<>(asList(1, 1, 2, 3));
        List<Integer> expectedList = new ArrayList<>(asList(3, 2, 1, 1));
        reverse(list);
        assertEquals("CollectionsTest.testReverse", expectedList, list);
    }

    private static void testRotate() {
        List<Integer> list = new ArrayList<>(asList(1, 1, 2, 3));
        List<Integer> expectedList = new ArrayList<>(asList(1, 2, 3, 1));
        rotate(list, -1);
        assertEquals("CollectionsTest.testRotate", expectedList, list);
    }

    private static void testSort() {
        List<Integer> list = new ArrayList<>(asList(3, 2, 1, 1));
        List<Integer> expectedList = new ArrayList<>(asList(1, 1, 2, 3));
        sort(list);
        assertEquals("CollectionsTest.testSort", expectedList, list);
    }

    private static void testSwap() {
        List<Integer> list = new ArrayList<>(asList(3, 2, 1, 1));
        List<Integer> expectedList = new ArrayList<>(asList(2, 3, 1, 1));
        swap(list, 0, 1);
        assertEquals("CollectionsTest.testSwap", expectedList, list);
    }

    private static void testUnmodifiableCollection() {
        Collection<Integer> col = new ArrayList<>(asList(1, 2));
        Collection<Integer> unmod = unmodifiableCollection(col);
        try {
            unmod.add(4);
            fail("AssertionError");
        } catch (Exception | Error e) {
            assertEquals("CollectionsTest.testUnmodifiableCollection", true, e.getMessage() == null);
        }
    }

}
