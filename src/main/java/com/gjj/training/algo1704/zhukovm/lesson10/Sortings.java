package com.gjj.training.algo1704.zhukovm.lesson10;

public class Sortings<E> {

    public static <E extends Comparable<E>> void bubbleSort(E[] array) {
        boolean flag;
        do {
            flag = false;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i].compareTo(array[i + 1]) > 0) {
                    swapElements(array, i, i + 1);
                    flag = true;
                }
            }
        } while (flag);
    }

    public static <E extends Comparable<E>> void insertionSort(E[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j > 0 && array[j - 1].compareTo(array[j]) > 0; j--) {
                swapElements(array, j, j - 1);
            }
        }
    }

    public static <E extends Comparable<E>> void quickSort(E[] array) {
        quickSortImpl(array, 0, array.length - 1);
    }

    private static <E extends Comparable<E>> void quickSortImpl(E[] array, int lowerIndex, int higherIndex) {
        int i = lowerIndex;
        int j = higherIndex;
        int mid = lowerIndex + (higherIndex - lowerIndex) / 2;
        E pivot = array[mid];
        while (i <= j) {
            while (array[i].compareTo(pivot) < 0) {
                i++;
            }
            while (array[j].compareTo(pivot) > 0) {
                j--;
            }
            if (i <= j) {
                swapElements(array, i, j);
                i++;
                j--;
            }
            if (lowerIndex < j) {
                quickSortImpl(array, lowerIndex, j);
            }
            if (higherIndex > i) {
                quickSortImpl(array, i, higherIndex);
            }
        }
    }

    public static <E extends Comparable<E>> void mergeSort(E[] array) {
        E[] tempArray = (E[]) new Comparable[array.length];
        mergeSortImpl(array, tempArray, 0, array.length - 1);
    }

    private static <E extends Comparable<E>> void mergeSortImpl(E[] array, E[] tempArray, int lowerIndex, int higherIndex) {
        if (lowerIndex < higherIndex) {
            int mid = lowerIndex + (higherIndex - lowerIndex) / 2;
            mergeSortImpl(array, tempArray, lowerIndex, mid);
            mergeSortImpl(array, tempArray, mid + 1, higherIndex);
            System.arraycopy(array, lowerIndex, tempArray, lowerIndex, higherIndex - lowerIndex + 1);
            int i = lowerIndex;
            int j = mid + 1;
            int k = lowerIndex;
            while (i <= mid && j <= higherIndex) {
                if (tempArray[i].compareTo(tempArray[j]) <= 0) {
                    array[k] = tempArray[i];
                    i++;
                } else {
                    array[k] = tempArray[j];
                    j++;
                }
                k++;
            }
            while (i <= mid) {
                array[k] = tempArray[i];
                k++;
                i++;
            }
        }
    }

    private static <E extends Comparable<E>> void swapElements(E[] array, int firstIndex, int secondIndex) {
        E tmp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = tmp;
    }

}
