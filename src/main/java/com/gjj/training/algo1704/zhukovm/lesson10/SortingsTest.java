package com.gjj.training.algo1704.zhukovm.lesson10;

import static com.gjj.training.algo1704.zhukovm.util.Assert.assertEquals;

public class SortingsTest {

    public static void main(String[] args) {
        testBubbleSort();
        testBubbleSortDuplicates();
        testBubbleSortOneElem();
        testBubbleSortSorted();
        testInsertionSort();
        testInsertionSortOneElem();
        testInsertionSortDuplicates();
        testQuickSort();
        testQuickSortOneElem();
        testQuickSortDuplicates();
        testMergeSort();
        testMergeSortOneElem();
        testMergeSortDuplicates();
    }

    private static void testBubbleSort() {
        Integer[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        Sortings.bubbleSort(array);
        Integer[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("SortingsTest.testBubbleSort", expectedArray, array);
    }

    private static void testBubbleSortOneElem() {
        Integer[] array = {1};
        Sortings.bubbleSort(array);
        Integer[] expectedArray = {1};
        assertEquals("SortingsTest.testBubbleSortOneElem", expectedArray, array);
    }

    private static void testBubbleSortDuplicates() {
        Integer[] array = {6, 6, 6, 4, 4, 3, 3, 3, 5, 5};
        Sortings.bubbleSort(array);
        Integer[] expectedArray = {3, 3, 3, 4, 4, 5, 5, 6, 6, 6};
        assertEquals("SortingsTest.testBubbleSortDuplicates", expectedArray, array);
    }

    private static void testBubbleSortSorted() {
        Integer[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Sortings.bubbleSort(array);
        Integer[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("SortingsTest.testBubbleSortSorted", expectedArray, array);
    }

    private static void testInsertionSort() {
        Integer[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        Sortings.insertionSort(array);
        Integer[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("SortingsTest.testInsertionSort", expectedArray, array);
    }

    private static void testInsertionSortOneElem() {
        Integer[] array = {1};
        Sortings.insertionSort(array);
        Integer[] expectedArray = {1};
        assertEquals("SortingsTest.testInsertionSortOneElem", expectedArray, array);
    }

    private static void testInsertionSortDuplicates() {
        Integer[] array = {6, 6, 6, 4, 4, 3, 3, 3, 5, 5};
        Sortings.insertionSort(array);
        Integer[] expectedArray = {3, 3, 3, 4, 4, 5, 5, 6, 6, 6};
        assertEquals("SortingsTest.testInsertionSortDuplicates", expectedArray, array);
    }

    private static void testQuickSort() {
        Integer[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        Sortings.quickSort(array);
        Integer[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("SortingsTest.testQuickSort", expectedArray, array);
    }

    private static void testQuickSortOneElem() {
        Integer[] array = {1};
        Sortings.quickSort(array);
        Integer[] expectedArray = {1};
        assertEquals("SortingsTest.testQuickSortOneElem", expectedArray, array);
    }

    private static void testQuickSortDuplicates() {
        Integer[] array = {6, 6, 6, 4, 4, 3, 3, 3, 5, 5};
        Sortings.quickSort(array);
        Integer[] expectedArray = {3, 3, 3, 4, 4, 5, 5, 6, 6, 6};
        assertEquals("SortingsTest.testQuickSortDuplicates", expectedArray, array);
    }

    private static void testMergeSort() {
        Integer[] array = {5, 6, 1, 4, 3, 2, 7, 8, 10, 9};
        Sortings.mergeSort(array);
        Integer[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals("SortingsTest.testMergeSort", expectedArray, array);
    }

    private static void testMergeSortOneElem() {
        Integer[] array = {1};
        Sortings.mergeSort(array);
        Integer[] expectedArray = {1};
        assertEquals("SortingsTest.testMergeSortOneElem", expectedArray, array);
    }

    private static void testMergeSortDuplicates() {
        Integer[] array = {6, 6, 6, 4, 4, 3, 3, 3, 5, 5};
        Sortings.mergeSort(array);
        Integer[] expectedArray = {3, 3, 3, 4, 4, 5, 5, 6, 6, 6};
        assertEquals("SortingsTest.testMergeSortDuplicates", expectedArray, array);
    }

}
