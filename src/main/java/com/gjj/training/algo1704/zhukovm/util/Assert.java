package com.gjj.training.algo1704.zhukovm.util;

import java.util.Arrays;
import java.util.List;

public final class Assert {

    //boolean : boolean
    public static void assertEquals(final String testName, final boolean expected, final boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    //int : int
    public static void assertEquals(final String testName, final int expected, final int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    //String : String
    public static void assertEquals(final String testName, final String expected, final String actual) {
        if (actual.equals(expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \"" + expected + "\", actual \"" + actual + "\"");
        }
    }

    //double[] : double[]
    public static void assertEquals(final String testName, final double[] expected, final double[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nexpected "
                    + Arrays.toString(expected) + ",\nactual " + Arrays.toString(actual));
        }
    }

    //double : double
    public static void assertEquals(final String testName, final double expected, final double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    //int[] : int[]
    public static void assertEquals(final String testName, final int[] expected, final int[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nexpected "
                    + Arrays.toString(expected) + ",\nactual " + Arrays.toString(actual));
        }
    }

    //long[] : long[]
    public static void assertEquals(final String testName, final long[] expected, final long[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nexpected "
                    + Arrays.toString(expected) + ",\nactual " + Arrays.toString(actual));
        }
    }

    //short[] : short[]
    public static void assertEquals(final String testName, final short[] expected, final short[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nexpected "
                    + Arrays.toString(expected) + ",\nactual " + Arrays.toString(actual));
        }
    }

    //char[] : char[]
    public static void assertEquals(final String testName, final char[] expected, final char[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nexpected "
                    + Arrays.toString(expected) + ",\nactual " + Arrays.toString(actual));
        }
    }

    //byte[] : byte[]
    public static void assertEquals(final String testName, final byte[] expected, final byte[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nexpected "
                    + Arrays.toString(expected) + ",\nactual " + Arrays.toString(actual));
        }
    }

    //float[] : float[]
    public static void assertEquals(final String testName, final float[] expected, final float[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nexpected "
                    + Arrays.toString(expected) + ",\nactual " + Arrays.toString(actual));
        }
    }

    //char : char
    public static void assertEquals(final String testName, final char expected, final char actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    //int[][] : int[][]
    public static void assertEquals(final String testName, final int[][] expected, final int[][] actual) {
        if (Arrays.deepEquals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nexpected "
                    + Arrays.deepToString(expected) + ",\nactual " + Arrays.deepToString(actual));
        }
    }

    public static void assertEquals(String testName, List<?> expected, List<?> actual) {
        if (actual.equals(expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed");
        }
    }

    public static void assertEquals(String testName, Object expected, Object actual) {
        if (actual.equals(expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \"" + expected.toString() + "\", actual \"" + actual.toString() + "\"");
        }
    }

    public static void assertEquals(String testName, Object[] expected, Object[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \"" + Arrays.toString(expected) + "\", actual \"" + Arrays.toString(actual) + "\"");
        }
    }

    public static void fail(String exceptionMessage) {
        throw new AssertionError(exceptionMessage);
    }

}
