package com.gjj.training.algo1704.zhukovm.util;

public class StopWatch {

    private long startTime;

    public long start() {
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public long getElapsedTime() {
        return System.currentTimeMillis() - startTime;
    }

}
